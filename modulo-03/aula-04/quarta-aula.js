 class Turma {
    constructor(ano){
        this.ano = ano;
    }

    apresentarAno(){
        console.log(`Essa turma é do ano de: ${this.ano}`);
    }

    static info(){
        console.log(`Testando informações`)
    }

    get anoTurma() {
        console.log(this.ano);
    }

    set local1(localizacao){
        this.localTurma = localizacao;
    }
}
/*
const vemser = new Turma("2019/02");

vemser.apresentarAno();
vemser.anoTurma;
vemser.local = "DBC";
console.log(vemser.localTurma); 



class Vemser extends Turma {
    constructor(ano, local, qtdAlunos){
        super(ano)
        this.local = local;
        this.qtdAlunos = qtdAlunos;
    }

    descricao(){
        console.log(`Turma do Vem ser ano ${this.ano} realizado na ${this.local}, quantidade de alunos: ${this.qtdAlunos} alunos. `)
    }
}

const vemser = new Vemser("2019/02", "DBC", 17);
vemser.descricao(); 


let defer = new Promise((resolved, reject) => {
    setTimeout(() => {
        if(true){
            resolved('Foi resolvido');
        }else{
            reject('Erro');
        }
    }, 3000);
})

defer
    .then((data) =>{
        console.log(data);
        return "Novo Resultado";
    })
    .then((data) =>{
        console.log(data);
    })
    .catch((erro) => console.log(erro));
    */

   // let pokemon = fetch(`https://pokeapi.co/api/v2/pokemon`);
   // let pokemon1 = fetch(`https://pokeapi.co/api/v2/pokemon/1`);


    //pending
    //resolved
    //rejected
    //pokemon
            //.then(data => data.json() )
            //.then(data => {console.log(data.results) });

    const valor1 = new Promise((resolved, rejected) => {
        resolved({ valorAtual : '1'})
    })        

    const valor2 = new Promise((resolved, rejected) => {
        setTimeout(() =>{
            resolved({ valorAtual : '2'})
        }, 4000);
    })   

    /* Promise
        .all([valor1, valor2])
        .then(resposta => {
            console.log(resposta);
        }) */

    Promise
        .race([valor1, valor2])
        .then(resposta => {
            console.log(resposta);
        })    
