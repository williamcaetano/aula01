class PokeApi {// eslint-disable-line no-unused-vars
  buscarTodos() {
    const pokemon = fetch( 'https://pokeapi.co/api/v2/pokemon' )
    return pokemon.then( data => data.json() )
  }

  buscarNomeEspecifico( id ) {
    const pokemon = fetch( `https://pokeapi.co/api/v2/pokemon/${ id }` )
    return pokemon.then( data => data.json() )
  }
}
