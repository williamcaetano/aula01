class Pokemon {// eslint-disable-line no-unused-vars
  constructor( obj ) {
    this.nome = obj.name
    this.idPokemon = obj.id
    this.altura = obj.height
    this.peso = obj.weight
    this.img = obj.sprites.front_default
    this.tipo = obj.types.map( item => ` ${ item.type.name }` ).toString()
    this.stats = obj.stats.map( item => ` ${ item.stat.name }(${ item.base_stat }%)` ).toString()
  }

  get conversaoPeso() {
    return `${ this.peso / 10 }  Kg`
  }

  get conversaoAltura() {
    return `${ this.altura * 10 } cm`
  }
}
