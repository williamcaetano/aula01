/* eslint-disable no-alert */
const pokeApi = new PokeApi();
// const pokemonEspecifico = pokeApi.buscarEspecifico();

// pokemonEspecifico.then(pokemon => {
// const poke = new Pokemon ( pokemon );
// renderizacaoPokemon( poke );
// })
const nome = document.querySelector( '.nome' );
const height = document.querySelector( '.height' );
const imagem = document.querySelector( '.imagem' );
const idPoke = document.querySelector( '.id' );
const pesoPoke = document.querySelector( '.weight' );
const tipoPoke = document.querySelector( '.type' );
const estatisticasPoke = document.querySelector( '.stats' );


function renderizacaoPokemon( pokemon ) {
// let dadosPokemon = document.getElementById('dadosPokemon');
  nome.innerHTML = `Nome: ${ pokemon.nome }`;
  height.innerHTML = `Altura: ${ pokemon.conversaoAltura }`;
  imagem.innerHTML = `<img src="${ pokemon.img }"></img>`;
  idPoke.innerHTML = `ID: ${ pokemon.idPokemon } `;
  pesoPoke.innerHTML = `Peso: ${ pokemon.conversaoPeso } `;
  tipoPoke.innerHTML = `Tipo: ${ pokemon.tipo }`;
  estatisticasPoke.innerHTML = `Estatisticas: ${ pokemon.stats }`;
}


const id = document.getElementById( 'numero' )


/* const teste = document.cookie.indexOf( id )
console.log( teste ) */

/* async function buscar() {
  conts pokemonEspecifico = await pokeApi.buscarNomeEspecifico( id.value )
  const poke = new Pokemon( pokemonEspecifico )
  renderizacaoPokemon( poke )
}
buscar() */

function buscarPokemon() {
  const cookies = document.cookie
  const pokemonEspecifico = pokeApi.buscarNomeEspecifico( id.value );

  pokemonEspecifico
    .then( pokemon => {
      const poke = new Pokemon( pokemon )
      if ( cookies.indexOf( id.value ) === -1 ) {
        renderizacaoPokemon( poke );
        document.cookie += ` ${ id.value } `
      } else alert( 'Mesma id digitada, recarregue a página' )
    } )
    .catch( () => {
      alert( 'Digite um id válido' )
    } )
}
id.addEventListener( 'blur', () => {
  buscarPokemon()
} )
// EXERCICIO 3, 5
const array = []
function sortear() {
  while ( array.length < 802 ) {
    const aleatorio = Math.floor( Math.random() * 802 + 1 )
    if ( array.indexOf( aleatorio ) === -1 ) {
      array.push( aleatorio )
    }
  }
}
sortear()
let i = 0

function sortearDeNovo() {
  i = 0
  sortear()
}

function pokemonAleatorio() {
  const pokemonAleatorio1 = pokeApi.buscarNomeEspecifico( array[i += 1] );
  if ( i === 801 ) {
    sortearDeNovo()
  }
  pokemonAleatorio1.then( pokemon2 => {
    const poke = new Pokemon( pokemon2 );
    renderizacaoPokemon( poke );
  } )
}

const sorte = document.getElementById( 'sorte' )
sorte.addEventListener( 'click', () => {
  pokemonAleatorio()
} )
