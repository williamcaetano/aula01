
// Exercício 1 
let circulo = {
    raio:3,
    tipoCalculo : "A"
}

let calcularCirculo = function({raio , tipoCalculo:tipo}){
    return Math.ceil(tipo == "A" ? Math.PI* Math.pow(raio,2) : 2 * Math.PI *raio);
}

console.log(calcularCirculo(circulo));
//console.log(calcularCirculo(3, "B"));

console.log("\n");

// Exercício 2 

/*function nãoBissexto(ano){
    return (ano%400 === 0) || (ano %4 === 0 && ano %100 !== 0) ? false : true;
}
*/

let naoBissexto = ano => (ano%400 === 0) || (ano %4 === 0 && ano %100 !== 0) ? false : true;

/*const testes = {
    diaAula : "Segundo",
    local : "DBC",
    naoBissexto(ano){
        return (ano%400 === 0) || (ano %4 === 0 && ano %100 !== 0) ? false : true;
    }
}*/

console.log(naoBissexto(1992));
console.log(naoBissexto(2016));
console.log(naoBissexto(2000));
console.log(naoBissexto(2100));
console.log("\n");

// Exercício 3

function somarPares(numeros){
    let resultado = 0;
    for (let i = 0; i < numeros.length; i++) {
        if (i % 2 == 0){
            resultado += numeros[i];
        }
    }
    return resultado;
}

//console.log(somarPares([1, 56, 4.34, 6, -2]));// 3.34

/*function adicionar(op1){
    return function(op2){
        return op1+op2;
    }
}
*/

let adicionar = op1 => op2 => op1 + op2

console.log(adicionar(3)(4)); //7
console.log(adicionar(5642)(8749)); // 14391
console.log("\n");

/* const is_divisivel = (divisor, numero) => !(numero % divisor);
const divisor = 2;
console.log(is_divisivel(divisor, 20));
console.log(is_divisivel(divisor, 11));
console.log(is_divisivel(divisor, 12)); */

const divisivelPor = divisor => numero => !(numero % divisor);

const is_divisivel = divisivelPor(2);
const is_divisivel3 = divisivelPor(3);

/*console.log(is_divisivel(20));
console.log(is_divisivel(11));
console.log(is_divisivel(12));
console.log(is_divisivel3(20));
console.log(is_divisivel3(11));
console.log(is_divisivel3(12));*/




function imprimirGBP(numero){
    let qtdCasasMilhares = 3;
    let separadorMilhar = ',';
    let separadorDecimal = '.';

    let stringBuffer = [];
    let parteDecimal = arredondar(Math.abs(numero)%1)
    let parteInteira = Math.trunc(numero)
    let parteInteiraString = Math.abs(parteInteira).toString()
    let parteInteiraTamanho = parteInteiraString.length

    let c = 1

    while (parteInteiraString.length > 0){
        if ( c % qtdCasasMilhares == 0){
            stringBuffer.push(`${separadorMilhar}${parteInteiraString.slice( parteInteiraTamanho - c)}`)
            parteInteiraString = parteInteiraString.slice(0 , parteInteiraTamanho - c)
        } else if(parteInteiraTamanho < qtdCasasMilhares){
            stringBuffer.push(parteInteiraString)
            parteInteiraString = ''
        }
        c++
    }
    stringBuffer.push(parteInteiraString)

    let decimalString = parteDecimal.toString().replace('0.', '').padStart(2, '0')
    return `${parteInteira >= 0 ? '£' : '-£'}${stringBuffer.reverse().join('')}${separadorDecimal}${decimalString}`
}

console.log(imprimirGBP(0)) // “£ 0.00”
console.log(imprimirGBP(3498.99)) // “£ 3,498.99”
console.log(imprimirGBP(-3498.99)) // “-£ 3,498.99”
console.log(imprimirGBP(2313477.0135)) // “£ 2,313,477.02”
console.log("\n");

function imprimirFR(numero){
    let qtdCasasMilhares = 3;
    let separadorMilhar = '.';
    let separadorDecimal = ',';
    let simboloEuro = ' €';

    let stringBuffer = [];
    let parteDecimal = arredondar(Math.abs(numero)%1)
    let parteInteira = Math.trunc(numero)
    let parteInteiraString = Math.abs(parteInteira).toString()
    let parteInteiraTamanho = parteInteiraString.length

    let c = 1

    while (parteInteiraString.length > 0){
        if ( c % qtdCasasMilhares == 0){
            stringBuffer.push(`${separadorMilhar}${parteInteiraString.slice( parteInteiraTamanho - c)}`)
            parteInteiraString = parteInteiraString.slice(0 , parteInteiraTamanho - c)
        } else if(parteInteiraTamanho < qtdCasasMilhares){
            stringBuffer.push(parteInteiraString)
            parteInteiraString = ''
        }
        c++
    }
    stringBuffer.push(parteInteiraString)

    let decimalString = parteDecimal.toString().replace('0.', '').padStart(2, '0')
    return `${parteInteira >= 0 ? '' : '-'}${stringBuffer.reverse().join('')}${separadorDecimal}${decimalString}${simboloEuro}`
}

console.log(imprimirFR(0)) // “0,00 €”
console.log(imprimirFR(3498.99)) // “3.498,99 €”
console.log(imprimirFR(-3498.99)) // “-3.498,99 €”
console.log(imprimirFR(2313477.0135)) // “2.313.477,02 €”

function imprimirBRL(numero){
    return imprimirMoeda(
    {    
        numero,
        separadorMilhar : '.',
        separadorDecimal: ',',
        //prefixo = 'R$'
        colocarMoeda : numeroFormatado => `R$ ${ numeroFormatado}`,
        colocarNegativo : numeroFormatado => `- ${ numeroFormatado}`,
    }    
    )
}


let moedas = (function() {
    //Tudo que é privado
    imprimirMoeda(params){
        function arredondar(numero, precisao = 2) {
            const fator = Math.pow( 10, precisao)
            return Math.ceil(numero * fator) / fator
        }

    const {
        numero, 
        separadorMilhar, 
        separadorDecimal, 
        colocarMoeda, 
        colocarNegativo
    } = params

    let qtdCasasMilhares = 3;
    let stringBuffer = [];
    let parteDecimal = arredondar(Math.abs(numero)%1)
    let parteInteira = Math.trunc(numero)
    let parteInteiraString = Math.abs(parteInteira).toString()
    let parteInteiraTamanho = parteInteiraString.length

    let c = 1

    while (parteInteiraString.length > 0){
        if ( c % qtdCasasMilhares == 0){
            stringBuffer.push(`${separadorMilhar}${parteInteiraString.slice( parteInteiraTamanho - c)}`)
            parteInteiraString = parteInteiraString.slice(0 , parteInteiraTamanho - c)
        } else if(parteInteiraTamanho < qtdCasasMilhares){
            stringBuffer.push(parteInteiraString)
            parteInteiraString = ''
        }
        c++
    }
    stringBuffer.push(parteInteiraString)

    let decimalString = parteDecimal.toString().replace('0.', '').padStart(2, '0')
    //let prefixoMoeda = `${ parteInteira >=0 ? '' : '-'}${prefixo}`
    const numeroFormatado = `${ stringBuffer.reverse().join('')}${separadorDecimal}${decimalString}`
    //return `${prefixoMoeda}${stringBuffer.reverse().join('')}${separadorDecimal}${decimalString}`
    return parteInteira >=0 ? colocaMoeda(numeroFormatado) : colocaNegativo(colocaMoeda(numeroFormatado))
}
})()
    

console.log(imprimirBRL(0)) // “R$ 0,00”
console.log(imprimirBRL(3498.99)) // “R$ 3.498,99”
console.log(imprimirBRL(-3498.99)) // “-R$ 3.498,99”
console.log(imprimirBRL(2313477.0135)) // “R$ 2.313.477,02”
console.log("\n");