import React, {Component} from 'react';
import * as axios from 'axios';
import Cliente from '../../Componentes/Cliente'
import Header from '../../Componentes/Header'
import Footer from '../../Componentes/Footer'
import './clientes.css'


export default class Clientes extends Component {
    constructor(props){
        super(props)
        this.state = {
            ListaClientes : [],
            ListaAlterada : []
        }        
    }
    componentDidMount() {
        const header = {
            headers: {Authorization : localStorage.getItem('Authorization')}
        }
        axios.get('http://localhost:1337/clientes', header).then(resp=>
            { const item = resp.data.clientes;
                this.setState({
                    ListaClientes : item,
                    ListaAlterada : item
                })
            })
    }

    buscarPorFiltro( evt ){
        const pesquisa = evt.target.value
        const resultado = this.state.ListaAlterada.filter(item => item.nome.toLowerCase().includes(pesquisa.toLowerCase()))
        this.setState({
            ListaClientes: resultado
        })
    }
    
    render() {
        const {ListaClientes} = this.state
        return (
            <React.Fragment>
                <Header></Header>
                <h1 className="titulo">Clientes</h1>
                <div className="box-input">
                    <label className="label">Pesquisar: </label>
                    <input type="text" className="input" placeholder="Pesquisar por NOME..." onBlur={this.buscarPorFiltro.bind( this )}></input>
                </div>
                <Cliente ListaClientes={ListaClientes}></Cliente>
                <Footer></Footer>
            </React.Fragment>
        )   
    }
}