import React, {Component} from 'react';
import * as axios from 'axios';
import Header from '../../Componentes/Header'
import Footer from '../../Componentes/Footer'
import './DetalheAgencias.css'

export default class DetalheAgencias extends Component {
    componentDidMount() {
        const { id } = this.props.match.params
        const header = {
            headers: {Authorization : localStorage.getItem('Authorization')}
        }
        axios.get(`http://localhost:1337/agencia/${id -1}`, header).then(resp=>
            { const item = resp.data.agencias;
                this.setState({
                    DetalheAgencias :  item
                })
            })
    }

    renderizar(){
        return this.state == null ? false : true
    }

    render() {
        return (
            <React.Fragment>
                <Header></Header>
                <h1 className="titulo"> Detalhamento Agencia </h1>
                {this.renderizar() ? 
                    <div className="container-detalhe-agencia">
                        <div className="item1">
                            <p className="paragrafo">
                                Nome: {this.state.DetalheAgencias.nome}<br/> Endereço: {this.state.DetalheAgencias.endereco.logradouro} nº:{this.state.DetalheAgencias.endereco.numero}<br/>
                                Bairro: {this.state.DetalheAgencias.endereco.bairro} {this.state.DetalheAgencias.endereco.cidade}/{this.state.DetalheAgencias.endereco.uf}.
                            </p>
                        </div>
                    </div> 
                : ''
                }
                <Footer></Footer>
            </React.Fragment>        
        )   
    }
}