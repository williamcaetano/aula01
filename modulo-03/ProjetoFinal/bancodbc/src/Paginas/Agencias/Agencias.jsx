import React, {Component} from 'react';
import * as axios from 'axios';
import Agencia from '../../Componentes/Agencia'
import Header from '../../Componentes/Header'
import Footer from '../../Componentes/Footer'
import './agencias.css'

export default class Agencias extends Component {
    constructor(props){
        super(props)
        this.state = {
            ListaAgencias : [],
            ListaAlterada: []
        }        
    }
    componentDidMount() {
        const header = {
            headers: {Authorization : localStorage.getItem('Authorization')}
        }
        axios.get('http://localhost:1337/agencias', header).then(resp=>
            { const item = resp.data.agencias;
                this.setState({
                    ListaAgencias : item,
                    ListaAlterada : item
                })
            })
    }

    buscarPorFiltro( evt ){
        const pesquisa = evt.target.value
        const resultado = this.state.ListaAlterada.filter(item => item.nome.toLowerCase().includes(pesquisa.toLowerCase()))
        this.setState({
            ListaAgencias: resultado
        })
    }
    
    render() {
        const {ListaAgencias} = this.state
        return (
            <React.Fragment>
                <Header></Header>
                <h1 className="titulo">AGENCIAS</h1>
                <div className="box-input">
                    <label className="label">Pesquisar: </label>
                    <input type="text" className="input" placeholder="Pesquisar por NOME..." onBlur={this.buscarPorFiltro.bind( this )}></input>
                    <button type="button" className="button-filtro">Agencias Digitais</button>
                </div>
                <Agencia ListaAgencias={ListaAgencias}></Agencia>
                <Footer></Footer>
            </React.Fragment>
        )   
    }
}
