import React, {Component} from 'react';
import * as axios from 'axios';
import Header from '../../Componentes/Header'
import Footer from '../../Componentes/Footer'
import './DetalheClientes.css'

export default class DetalheClientes extends Component {
    componentDidMount() {
        const { id } = this.props.match.params
        const header = {
            headers: {Authorization : localStorage.getItem('Authorization')}
        }
        axios.get(`http://localhost:1337/cliente/${id -1}`, header).then(resp=>
            { const item = resp.data.cliente;
                console.log(item)
                this.setState({
                    DetalheClientes :  item
                })
            })
    }

    renderizar(){
        return this.state == null ? false : true
    }

    render() {
        return (
            <React.Fragment>
                <Header></Header>
                <h1 className="titulo"> Detalhamento Cliente </h1>
                {this.renderizar() ? 
                    <div className="container-detalhe-cliente">
                        <div className="item2">
                            <p className="paragrafo">
                                Nome: {this.state.DetalheClientes.nome}<br/>
                                CPF: {this.state.DetalheClientes.cpf}<br/> Agencia: {this.state.DetalheClientes.agencia.nome}<br/>
                                Endereço: {this.state.DetalheClientes.agencia.endereco.logradouro}<br/> 
                                nº: {this.state.DetalheClientes.agencia.endereco.numero} bairro: {this.state.DetalheClientes.agencia.endereco.bairro} {this.state.DetalheClientes.agencia.endereco.cidade}
                                /{this.state.DetalheClientes.agencia.endereco.uf}.
                            </p>
                        </div>
                    </div> 
                : ''}
                <Footer></Footer>   
            </React.Fragment>        
        )   
    }
}