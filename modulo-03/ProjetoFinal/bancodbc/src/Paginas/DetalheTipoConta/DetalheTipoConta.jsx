import React, {Component} from 'react';
import * as axios from 'axios';
import Header from '../../Componentes/Header'
import Footer from '../../Componentes/Footer'
import './DetalheTipoConta.css'

export default class DetalheTipoConta extends Component {
    componentDidMount() {
        const { id } = this.props.match.params
        const header = {
            headers: {Authorization : localStorage.getItem('Authorization')}
        }
        axios.get(`http://localhost:1337/tiposConta/${id-1}`, header).then(resp=>
            { const item = resp.data.tipos;
                this.setState({
                    DetalheConta :  item
                })
            })
    }

    renderizar(){
        return this.state == null ? false : true
    }

    render() {
        return (
            <React.Fragment>
                <Header></Header>
                <h1 className="titulo"> Detalhamento Tipo de Conta </h1>
                {this.renderizar() ? 
                    <div className="container-detalhe-tipo-conta">
                        <div className="item4">
                            <p className="paragrafo">
                                Nome: {this.state.DetalheConta.nome}<br/> ID: {this.state.DetalheConta.id}
                            </p>
                        </div> 
                    </div>
                : ''
                }
                <Footer></Footer>
            </React.Fragment>        
        )   
    }
}