import React, {Component} from 'react';
import * as axios from 'axios';
import './login.css'

export default class Login extends Component {

    constructor(props){
        super(props)
        this.state={
            email : '',
            senha : ''
        }
        this.trocaValoresState = this.trocaValoresState.bind(this)
    }

    trocaValoresState(evt){
        const  {name, value} = evt.target;
        this.setState({
            [name] : value,      
        })
    }

    logar(evt){
        evt.preventDefault();
        const { email, senha } = this.state
        if(email  && senha)
        {
            //executa regra de login
            axios.post('http://localhost:1337/login', {
                email:this.state.email,
                senha : this.state.senha
            }).then(
                resp=>{
                    localStorage.setItem( 'Authorization' , resp.data.token ) 
                    this.props.history.push('/')     
                }  ) 
        }}
        

    render() {
        return (
        <div>
            <React.Fragment>
                <header className="headerLogin">
                    <span>BANCO DBC</span>
                </header>
                <div className="container">
                    <div className="containerLogin">
                        <h1>Faça seu Login:</h1>
                        <input className="inputLogin" type='email' name = 'email' id='nome' placeholder='Digite o seu email...' onChange = { this.trocaValoresState } />
                        <br />
                        <input className="inputLogin" type='password' name = 'senha' id='senha' placeholder='Digite sua senha...' onChange = { this.trocaValoresState } />
                        <br />
                        <button className="buttonLogin" type='button' onClick={this.logar.bind(this)}> Logar </button>
                    </div>
                </div>
            </React.Fragment>
        </div>
        
        )   
        
    }
}