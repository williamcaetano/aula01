import React, {Component} from 'react';
import * as axios from 'axios';
import ContaCliente from '../../Componentes/ContaCliente'
import Header from '../../Componentes/Header'
import Footer from '../../Componentes/Footer'
import './contaClientes.css'

export default class Agencias extends Component {
    constructor(props){
        super(props)
        this.state = {
            ListaContasClientes: [],
            ListaAlterada: []
        }        
    }
    componentDidMount() {
        const header = {
            headers: {Authorization : localStorage.getItem('Authorization')}
        }
        axios.get('http://localhost:1337/conta/clientes', header).then(resp=>
            { const item = resp.data.cliente_x_conta;
                this.setState({
                    ListaContasClientes: item,
                    ListaAlterada : item
                })
            })
    }

    buscarPorFiltro( evt ){
        const pesquisa = evt.target.value
        const resultado = this.state.ListaAlterada.filter(item => item.tipo.nome.toLowerCase().includes(pesquisa.toLowerCase()))
        console.log(resultado)
        this.setState({
            ListaContasClientes: resultado
        })
    }

    render() {
        return (
            <React.Fragment>
                <Header></Header>
                <h1 className="titulo">Conta Clientes</h1>
                <div className="box-input">
                    <label className="label">Pesquisar: </label>
                    <input type="text" className="input" placeholder="Pesquisar por TIPO DE CONTA..." onBlur={this.buscarPorFiltro.bind( this )}></input>
                </div>
                <ContaCliente ListaContasClientes={this.state.ListaContasClientes}></ContaCliente>
                <Footer></Footer>
            </React.Fragment>
        )   
    }
}
