import React, {Component} from 'react';
import * as axios from 'axios';
import Header from '../../Componentes/Header'
import Foooter from '../../Componentes/Footer'
import './DetalheContaClientes.css'

export default class DetalheContaClientes extends Component {
    componentDidMount() {
        const { id } = this.props.match.params
        const header = {
            headers: {Authorization : localStorage.getItem('Authorization')}
        }
        axios.get(`http://localhost:1337/conta/cliente/${id -1}`, header).then(resp=>
            { const item = resp.data.conta;
                console.log(item)
                this.setState({
                    DetalheConta :  item
                })
            })
    }

    renderizar(){
        return this.state == null ? false : true
    }

    render() {
        return (
            <React.Fragment>
                <Header></Header>
                <h1 className="titulo"> Detalhamento Conta de Cliente </h1>
                {this.renderizar() ? 
                    <div className="container-detalhe-conta-cliente">
                        <div className="item3">
                            <p className="paragrafo">
                                Cliente: {this.state.DetalheConta.cliente.nome} CPF: {this.state.DetalheConta.cliente.cpf}<br/>
                                Tipo de Conta: {this.state.DetalheConta.tipo.nome}<br/> ID: {this.state.DetalheConta.tipo.id}
                            </p>
                        </div> 
                    </div>
                : ''}   
                <Foooter></Foooter>    
            </React.Fragment>        
        )   
    }
}