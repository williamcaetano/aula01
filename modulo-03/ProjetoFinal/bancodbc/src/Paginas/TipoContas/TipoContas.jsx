import React, {Component} from 'react';
import * as axios from 'axios';
import Conta from '../../Componentes/Conta'
import Header from '../../Componentes/Header'
import Footer from '../../Componentes/Footer'
import './tipoContas.css'

export default class Agencias extends Component {
    constructor(props){
        super(props)
        this.state = {
            ListaContas : [],
            ListaAlterada : []
        }        
    }
    componentDidMount() {
        const header = {
            headers: {Authorization : localStorage.getItem('Authorization')}
        }
        axios.get('http://localhost:1337/tipoContas', header).then(resp=>
            { const item = resp.data.tipos;
                this.setState({
                    ListaContas : item,
                    ListaAlterada : item
                })
            })
    }

    buscarPorFiltro( evt ){
        const pesquisa = evt.target.value
        const resultado = this.state.ListaAlterada.filter(item => item.nome.toLowerCase().includes(pesquisa.toLowerCase()))
        this.setState({
            ListaContas: resultado
        })
    }

    render() {
        const {ListaContas} = this.state
        return (
            <React.Fragment>
                <Header></Header>
                <h1 className="titulo">Tipo Contas</h1>
                <div className="box-input">
                    <label className="label">Pesquisar: </label>
                    <input type="text" className="input" placeholder="Pesquisar por TIPO DE CONTA..." onBlur={this.buscarPorFiltro.bind( this )}></input>
                </div>
                <Conta ListaContas={ListaContas}></Conta>
                <Footer></Footer>
            </React.Fragment>
        )   
    }
}