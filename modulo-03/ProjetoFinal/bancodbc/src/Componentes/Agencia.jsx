import React from 'react';
import {Link} from 'react-router-dom';
import './Agencia.css'

const Agencia = props => { 
    return (
        <React.Fragment>
            { props.ListaAgencias.map((item) => { 
                return(
                    <React.Fragment>
                        <div className="container-agencia">
                            <div className="box-item">
                                <Link className="link" to={`/agencia/${item.id}`}>
                                    <span>Nome: {item.nome}</span><br/>
                                    <span>Codigo: {item.codigo} </span><br/>
                                </Link><br/>
                                <input className="checkbox" type="checkbox" value={ item.id }/><span className="span-agencia">Tornar Digital</span>
                            </div>
                        </div>
                    </React.Fragment> 
                )
            }) }
        </React.Fragment>
    )
} 
export default Agencia;
