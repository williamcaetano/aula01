import React, {Component} from 'react';
import { Link } from 'react-router-dom'
import './Header.css'

export default class Header extends Component{
    logout(){
        localStorage.removeItem('Authorization')
    }

    render(){
        return (
            <React.Fragment>
                <header className="header">
                    <span className="logo">BANCO DBC</span>
                    <Link className="Link" to="/"> Agencias </Link>
                    <Link className="Link" to="/clientes"> Clientes </Link>
                    <Link className="Link" to="/tipoContas"> Tipos de Conta</Link>
                    <Link className="Link" to="/conta/clientes"> Contas de Clientes</Link>
                    <button className="button" onClick={this.logout.bind(this)}>Logout</button>
                </header>
            </React.Fragment>
        )
    }
}