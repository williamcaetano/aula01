import React from 'react';
import {Link} from 'react-router-dom';
import './Conta.css'

const Conta = props => { 
    return (
    <React.Fragment>
        { props.ListaContas.map((item) => { 
            return(
                <React.Fragment>
                    <div className="container-tipo-conta">
                        <div className="box-item4">
                            <Link className="link4" to={ `/tiposConta/${item.id}` }>
                                <span>
                                    Nome: {item.nome}
                                </span>
                            </Link>
                        </div>
                    </div>
                </React.Fragment> 
            )
        }) }
    </React.Fragment>
    )
} 
export default Conta;
