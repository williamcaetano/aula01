import React from 'react';
import {Link} from 'react-router-dom';
import './Cliente.css'

const Agencia = props => { 
    return (
    <React.Fragment>
        { props.ListaClientes.map((item) => { 
            return(
                <React.Fragment>
                    <div className="container-cliente">
                        <div className="box-item1">
                            <Link className="link1" to={`/cliente/${item.id}`}>
                                <span>Nome: {item.nome}</span><br/>
                                <span>CPF: {item.cpf} </span><br/>
                            </Link><br/>
                        </div>
                    </div>
                </React.Fragment> 
            )
        }) }
    </React.Fragment>
    )
} 
export default Agencia;