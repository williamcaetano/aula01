import React from 'react';
import {Link} from 'react-router-dom';
import './ContaCliente.css'

const ContaCliente = props => { 
    return (
    <React.Fragment>
        { props.ListaContasClientes.map((item) => { 
            return(
                <React.Fragment>
                    <div className="container-conta-cliente">
                        <div className="box-item3">
                            <Link className="link3" to={`/conta/cliente/${item.id}`}>
                                <span>Cliente:  {item.cliente.nome}</span><br/>
                                <span>Tipo de Conta:  {item.tipo.nome}</span><br/>
                            </Link>
                        </div>
                    </div>
                </React.Fragment> 
            )
        }) }
    </React.Fragment>
    )
} 
export default ContaCliente;
