import React from 'react';
import { Link } from 'react-router-dom'
import './Footer.css'

const Footer = props =>{
    return (
        <React.Fragment>
            <footer className="footerPadrao">
                <Link className="LinkFooter" to="/"> Agencias </Link>
                <Link className="LinkFooter" to="/clientes"> Clientes </Link>
                <Link className="LinkFooter" to="/tipoContas"> Tipos de Conta</Link>
                <Link className="LinkFooter" to="/conta/clientes"> Contas de Clientes</Link>
                <p className="paragrafoFooter">
                    Banco DBC. Todos os direitos reservados&copy;<br/><br/>
                    DBC Company: <br />
                    Av. Andaraí, 531 – Bairro Passo D’Areia<br />
                    Porto Alegre / RS – CEP: 91350-110<br />
                    Telefone: +55 (51) 3330.7777<br />
                </p>
            </footer>
        </React.Fragment>
    )
}

export default Footer;