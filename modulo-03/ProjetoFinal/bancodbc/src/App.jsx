import React, {Component} from 'react'
import './App.css'
import Login from './Paginas/Login/Login'
import Agencias from './Paginas/Agencias/Agencias'
import Clientes from './Paginas/Clientes/Clientes'
import TipoContas from './Paginas/TipoContas/TipoContas'
import ContaClientes from './Paginas/ContaClientes/ContaClientes'
import DetalheAgencias from './Paginas/DetalheAgencias/DetalheAgencias'
import DetalheClientes from './Paginas/DetalheClientes/DetalheClientes'
import DetalheTipoConta from './Paginas/DetalheTipoConta/DetalheTipoConta'
import DetalheContaClientes from './Paginas/DetalheContaClientes/DetalheContaClientes'
import { BrowserRouter as Router, Route } from 'react-router-dom'
import { PrivateRoute } from './Componentes/PrivateRoute'

export default class App extends Component{
  render() {
    return (
      <div className="App">
        <Router>
          <React.Fragment>
            <Route path="/login" component={ Login }/>
            <PrivateRoute path="/" exact component={ Agencias }/>
            <PrivateRoute path="/clientes" exact component={ Clientes }/>
            <PrivateRoute path="/tipoContas" exact component={ TipoContas }/>
            <PrivateRoute path="/conta/clientes" exact component={ ContaClientes }/>
            <PrivateRoute path="/agencia/:id" exact component={ DetalheAgencias }/>
            <PrivateRoute path="/cliente/:id" exact component={ DetalheClientes }/>
            <PrivateRoute path="/tiposConta/:id" exact component={ DetalheTipoConta }/>
            <PrivateRoute path="/conta/cliente/:id" exact component={ DetalheContaClientes }/>
          </React.Fragment>
        </Router>
      </div>
    );
  }
}
