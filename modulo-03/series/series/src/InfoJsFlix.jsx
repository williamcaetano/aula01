import React, { Component } from 'react';
import { Link } from 'react-router-dom'
//import logo from './logo.svg';
import './reactvemser/src/App2.css';
import series from './models/ListaSeries';

/* Array.prototype.hashtagSecreta = function(){
  let hash = []
  let hashtag = []
  let aux = []
  let hr = 0
  for (let index = 0; index < series.length; index++) {
    for(let j = 0; j < series[index].elenco.length; j++){
      if(series[index].elenco[j].indexOf('.') !== -1){
        hash.push(series[index].elenco[j])
      }
    }
    if(hash.length < series[index].elenco.length){
      hash = []
    }
  }
  for(let i = 0; i<hash.length; i++){
    hashtag.push(hash[i].split(''))
  }
  for (let i = 0; i < hashtag.length; i++) {
      hr = hashtag[i].indexOf('.')
      aux.push(hashtag[i][hr-1])
  }
  console.log(`#${aux.join('')}`)
} */

class InfoJsFlix extends Component {
    constructor(props) {
      super( props )
      this.series = series
      this.state = {
        series : ''
      }
    }

    encontrarSeriesinvalidas() {
      let state = this.state;
      let invalidas = []
      
      for(let index = 0; index < series.length; index++) {
          const isNulo = Object.values(series[index]).some(s => (s === null));
          if(series[index].anoEstreia > 2019 || isNulo){
              invalidas.push(series[index].titulo)
          }
      }
      state.series = invalidas.toString();
      return this.setState(state);
    }

    filtrarPorAno() {
      let state = this.state;
      let ano = document.getElementById('pesquisar');
      let filtroPorAno = []
      
      for(let index = 0; index < series.length; index++) {
          if(series.filter((s) => s.anoEstreia >= ano)){
              filtroPorAno.push(series[index].titulo)
          }
      }
      console.log(filtroPorAno)
      state.series = filtroPorAno.toString(); 
      return this.setState(state);
    } 

    procurarPorNome() { 
      let state = this.state;
      let nome = document.getElementById('pesquisar');
      for(let index = 0; index < series.length; index++) {
        for (let j = 0; j < series[index].elenco.length; j++) {
          if(series[index].elenco[j].match(nome.value)){
            console.log(true)
            state.series = `Ator encontrado`
            return this.setState(state);
          }
        }
      }
      state.series = 'Não há atores com esse nome ';
      return this.setState(state);
    }

    mediaEpisodios() {
      let state = this.state;
      let soma = 0
      for(let index = 0; index < series.length; index++){
        soma = soma + series[index].numeroEpisodios
      }
      let media = soma/series.length
      state.series = media;
      return this.setState(state);
    }

    totalSalarios() {
      let state = this.state;
      let indice = document.getElementById('pesquisar');
      if(series[indice.value] !== null){
        let diretores = series[indice.value].diretor.length;
        let operarios = series[indice.value].elenco.length;
        let total = (diretores * 100000)+(operarios*40000);
        state.series = total;
        return this.setState(state);
      }
      state.series = 'Série não encontrada';
      return this.setState(state);
    }

    queroGenero() {
      let state = this.state;
      let genero = document.getElementById('pesquisar');
      let generos = [];
      for(let index = 0; index < series.length; index++) {
        for (let j = 0; j < series[index].genero.length; j++) {
          if(series[index].genero[j].match(genero.value)){
            generos.push(series[index].titulo)
          }
        }
      }
      state.series = generos.join('/');
      return this.setState(state);  
    }

    queroTitulo() {
      let state = this.state
      let titulo = document.getElementById('pesquisar');
      let titulos = []
      for(let index = 0; index < series.length; index++) {
        if(series[index].titulo.indexOf( titulo.value ) !== -1){
          titulos.push(series[index].titulo)
        }
      }
      state.series = titulos.join('/');
      return this.setState(state);  
    }
    
    logout(){
      localStorage.removeItem('Authorization');
    }

    render(){
      return (
        <div className="App">
          <div className="App-header">
            <header className="header">
              <Link className="Link" to="/InfoJsFlix">JSFlix</Link>
              <Link className="Link" to="/blackMirror">Mirror</Link>
              <Link className="Link" to="/ListaDeAvaliacoes">Lista Avaliações</Link>
              <button className="button" type="button" onClick={this.logout.bind(this)}>Deslogar</button>
              <img src="" alt=""></img>
            </header>
            <input id="pesquisar" placeholder="Pesquisar" type="text"/>
            <div>
              <button className="button2" onClick={this.encontrarSeriesinvalidas.bind(this)}>Séries Invalidas</button>
              <button className="button2" onClick={this.filtrarPorAno.bind(this)}>Filtrar por Ano</button>
              <button className="button2" onClick={this.procurarPorNome.bind(this)}>Procurar Ator</button>
              <button className="button2" onClick={this.mediaEpisodios.bind(this)}>Média dos Episódios</button>
              <button className="button2" onClick={this.totalSalarios.bind(this)}>Total Salários</button>
              <button className="button2" onClick={this.queroGenero.bind(this)}>Procurar por Gênero</button>
              <button className="button2" onClick={this.queroTitulo.bind(this)}>Procurar por Título</button>
            </div>
            <p>{this.state.series}</p>
          </div>
        </div>
    );
  }
}
export default InfoJsFlix;