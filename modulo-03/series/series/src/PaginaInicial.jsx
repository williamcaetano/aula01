import React, { Component } from 'react';
import { BrowserRouter as Router, Route } from 'react-router-dom'
import './reactvemser/src/App2.css'
import App from './reactvemser/src/App'
import InfoJsFlix from './InfoJsFlix'
import Login from './Login'
import ListaDeAvaliacoes from './reactvemser/src/components/ListaDeAvaliacoes'
import {PrivateRoute} from './Components/PrivateRoute'

export default class PaginaInicial extends Component{
  render(){
    return(
      <div className="App2">
        <Router>
          <React.Fragment>  
            <PrivateRoute path="/" component={ InfoJsFlix }/>
            <Route path="/blackMirror" component={ App }/>
            <Route path="/Login" component={ Login }/>
            <Route path="/ListaDeAvaliacoes" component={ ListaDeAvaliacoes }/>
          </React.Fragment>
        </Router>
      </div>
    )
  }
}