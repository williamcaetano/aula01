import React , {Component} from 'react'
import * as axios from 'axios'
import './reactvemser/src/App2.css'

export default class Login extends Component{
    constructor( props ){
        super(props)
        this.state = {
            email: '',
            password: ''
        }
        this.trocaValoresState = this.trocaValoresState.bind( this )
    }

    trocaValoresState( evt ){
        const {name, value} = evt.target;
        this.setState({
            [name]: value
        })
    }

    logar( evt ){
        evt.preventDefault();

        const {email, password} = this.state
        if ( email && password ){
            //Executo a regra de login
            axios.post('http://localhost:1337/login', {
                email: this.state.email,
                password : this.state.password
            }).then( resp => {
                localStorage.setItem('Authorization', resp.data.token);
                this.props.history.push("/");
                console.log(resp) 
            })
        }

    }
    render(){
        return (
            <React.Fragment>
                <h2>  Logar:</h2>
                <input type="text" name="email" id="email" placeholder="Digite o email" onChange={this.trocaValoresState} ></input>
                <input type="text"  name="password" id="password" placeholder="Digite o password" onChange={this.trocaValoresState}></input>
                <button type="button" onClick={ this.logar.bind( this ) }>LOGAR</button>
            </React.Fragment>
        )
    }
}