import React, {Component} from 'react';
import PropTypes from 'prop-types';
import '../App2.css'

export default class ListaDeAvaliacoes extends Component{
    render() {
        const { infoEpisodio } = this.props
        return (
            <div>
                <React.Fragment>
                    {infoEpisodio.map((item) =>{
                    return( 
                        <h2>{item}</h2>
                        );
                        })}
                </React.Fragment>
            </div>
        )
    }
} 

ListaDeAvaliacoes.propTypes = {
    infoEpisodio: PropTypes.array
}
