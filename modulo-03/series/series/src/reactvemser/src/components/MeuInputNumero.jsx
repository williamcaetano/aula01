import React from 'react'


const MeuInputNumero = props => {
    let episodio = props.episodio;
    let verificaNota = props.verificaNota.bind(this);

    return (
        <React.Fragment>
        {
         episodio.assistido && props.exibirNaTela && (
          <div>
            <span>{props.mensagem}</span>
            <input  className={props.obrigatorio ? "Input": ""} type="number" placeholder={props.placeholder} onBlur={verificaNota} ></input>
            <span className="vermelho span2">{props.obrigatorio ? '*obrigatório': ''}</span>
          </div>
        )
        }
        </React.Fragment>
    )
}

export default MeuInputNumero