import React from 'react'

const EpisodioPadrao = props => {
    const { episodio } = props
    return(
        <React.Fragment>
          <h1>{ episodio.nome }</h1>
          <img src={ episodio.thumbUrl } alt={ episodio.nome }></img><br/>
          <h2>Temp/Ep: { episodio.temporadaEpisodio}, Duração: { episodio.duracaoEmMin }</h2>
          <h2>Ja assisti? { episodio.assistido ? 'Sim' : 'Não'}, {episodio.qtdVezesAssistido} vez(es)</h2>
          <h2>{ episodio.nota || 'Sem nota' }</h2>
          <button className="button" onClick={ props.sortearNoComp  /*.bind( this )*/}>Próximo</button>
          <button className="button" onClick={ props.marcarNoComp }>Já assisti</button><br></br>
        </React.Fragment>
    )
}

export default EpisodioPadrao