import React from 'react'

const MensagemFlash = props => {
    let exibirMensagem = props.exibirMensagem;
    let exibirAlerta = props.exibirAlerta;
    let visivel = props.visivel;

    return (
        <React.Fragment>
            <h3 className={`${props.cor} ${visivel ? 'fade-in':'fade-out'}`}>{exibirMensagem ? 'Nota registrada com sucesso': ''}</h3>
            <h3 className={`vermelho ${visivel ? 'fade-in':'fade-out'}`}>{exibirAlerta ? 'Informar uma nota válida (entre 1 e 5)': ''}</h3>
        </React.Fragment>
    )
}

/* export default class MensagemFlash extends React.Component{
    constructor(props){
        super(props)
    }
    render(){
        const {deveExibirMensagem, mensagem, cor} = this.props

        return deveExibirMensagem ? (
            <span>
        )
    }
}
 */
export default MensagemFlash