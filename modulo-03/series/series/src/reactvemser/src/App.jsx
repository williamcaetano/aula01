import React, { Component } from 'react';
import { BrowserRouter as Router, Route } from 'react-router-dom'
import * as axios from 'axios'
//import Popup from "reactjs-popup";
//import logo from './logo.svg';
import './App2.css';
import './Fade.css'
import './models/Episodio'
//import ListaTime from './exemplos/listaTimes'
//import Filho from './exemplos/Filhos'
//import Familia from './exemplos/Familia'
import ListaEpisodios from './models/ListaEpisodios'
import EpisodioPadrao from './components/EpisodioPadrao'
//import Episodio from './models/Episodio'
import { Link } from 'react-router-dom'
//import PaginaInicial from '../../PaginaInicial'
//import InfoJsFlix from '../../InfoJsFlix'
import MensagemFlash from './components/MensagemFlash'
import MeuInputNumero from './components/MeuInputNumero'
import ListaDeAvaliacoes from './components/ListaDeAvaliacoes'


//import CompA, { CompB } from './ExemploComponenteBasico'

// console.log(ListaTime)

class App extends Component {
  constructor( props ) {
    super( props )
    this.listaEpisodios = new ListaEpisodios();
    this.infoEpisodio = []
    //this.sortear = this.sortear.bind( this )
    this.state = {
      episodio: this.listaEpisodios.episodiosAleatorios,
      exibirMensagem: false,
      exibirAlerta: false,
      tempo : 4,
      placeholder: "Nota de 1 a 5",
      cor: "verde",
      exibirNaTela: true,
      mensagemInput: "Qual sua nota para o episódio?",
      obrigatorio: true,
      visivel: false
    }
  }

  compronentDidMount(){
    axios.get(`link aqui da API`).then( response => console.log(response) )
  }

  sortear = () => {
    const episodio = this.listaEpisodios.episodiosAleatorios
    this.setState({
      episodio
    })
  }

  marcarComoAssistido = () => {
    const { episodio } = this.state
    this.listaEpisodios.marcarComoAssistido( episodio )
    this.setState({
      episodio
    })
  }

  verificaNota( evt ) {
    const { episodio } = this.state
    let nota = evt.target.value
    if(nota >= 1 && nota <=5) {
      episodio.avaliar( evt.target.value )
      this.infoEpisodio.push(`Episodio: ${episodio.nome}, Nota: ${nota} `)
      this.setState({
        episodio,
        exibirMensagem: true,
        visivel: true
      })
      setTimeout( () => {
        this.setState({
          exibirMensagem: false,
          visivel: false
        })
      }, this.state.tempo*1000)
    }
    else{
      this.setState({
        episodio,
        exibirAlerta: true,
        visivel: true
      })
      setTimeout( () => {
        this.setState({
          exibirAlerta: false,
          visivel: false
        })
      }, this.state.tempo*1000)
    }
  }

  render(){
    const { episodio, notaEpisodio, tituloEpisodio } = this.state;
    return (
      <div className="App">
      <div className="App-Header2">
        <EpisodioPadrao episodio={ episodio } sortearNoComp={ this.sortear } marcarNoComp={ this.marcarComoAssistido}/>
        <MensagemFlash cor={this.state.cor} exibirMensagem={this.state.exibirMensagem} exibirAlerta={this.state.exibirAlerta} visivel={this.state.visivel}></MensagemFlash>
        <MeuInputNumero mensagem={this.state.mensagemInput}  placeholder={this.state.placeholder} 
                        episodio={this.state.episodio} verificaNota={this.verificaNota.bind( this )} 
                        exibirNaTela={this.state.exibirNaTela} obrigatorio={this.state.obrigatorio}>
        </MeuInputNumero>
      </div>
      </div>
    );
  }
}

export default App;
