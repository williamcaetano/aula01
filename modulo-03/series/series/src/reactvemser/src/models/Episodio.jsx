export default class Episodio {
    constructor ( nome, duracao, temporada, ordemEpisodio, thumbUrl, qtdVezesAssistido ){
        this.nome = nome
        this.duracao = duracao
        this.temporada = temporada
        this.ordemEpisodio = ordemEpisodio
        this.thumbUrl = thumbUrl
        this.qtdVezesAssistido = qtdVezesAssistido || 0
        //this.nota = nota || 'Não avaliado'
    }

    get duracaoEmMin() {
        return `${ this.duracao }min`
    }
    
    get temporadaEpisodio() {
        return `${ this.temporada.toString().padStart(2, '0')}/${ this.ordemEpisodio.toString().padStart(2, '0') }`
    }

    avaliar( nota ) {
        this.nota = `Nota: ${parseInt( nota )}`
        return nota
    }

}

