

//let inputTeste = document.getElementById('campoTeste')
//inputTeste.addEventListener('blur', cardapioIFood(true,true));

function multiplicar(...valores) {
    let array = [] 
    let i = 0
    array = valores
    let j = array.length-1
    while(i<j){
        array[j] = array[i]*array[j]
        j--
    }
    array.splice(0,1)
    return array
}

console.log(multiplicar(1,2,5,7)) // [2,5,7]
console.log(multiplicar(5, 3, 4, 5)) // [15, 20, 25]
console.log(multiplicar(7, 4, 8, 12, 14)) // [28, 56, 84, 98]

String.prototype.correr = function(upper = false){
    let texto = `${this} estou correndo`;
    return upper ? texto.toUpperCase : texto;
}

//console.log("ola".correr());