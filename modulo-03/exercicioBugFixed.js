function cardapioIFood( veggie = true, comLactose = false ) {
    let cardapio = [
      'enroladinho de salsicha',
      'cuca de uva'
    ]
    if (comLactose ) {
      cardapio.push( 'pastel de queijo' )
    }

    cardapio = [...cardapio,'pastel de carne', 'empada de legumes marabijosa']
    /*cardapio = cardapio.concat()
    console.log(cardapio)
    console.log("\n")
    */

    if ( veggie ) {
      cardapio.splice('enroladinho de salsicha', 1)
      cardapio.splice('pastel de carne', 1)
    }
    
    let resultado = cardapio
                          ///.filter(alimento => alimento === 'cuca de uva')
                            .map(alimento => alimento.toUpperCase());
    return resultado;
  }
  
  console.log(cardapioIFood(true,true)) // esperado: [ 'CUCA DE UVA', 'PASTEL DE QUEIJO', 'EMPADA DE LEGUMES MARABIJOSA' ]

  function criarSanduiche(pao, recheio, queijo) {
      console.log(`Seu sanduiche tem o pão ${pao} com recheio de ${recheio} e queijo ${queijo}`)  
  }

  const ingredientes = ['3 queijos', 'Frango', 'cheddar']
  //criarSanduiche(...ingredientes);

  function receberValoresIndefinidos(...valores) {
    let array = []
    let arrayN = []
    let i = 0
      valores.map(valor => array.push(valor));
      while(i<array.length){
        arrayN = array[i]*array[i+1];
        i++;
      }
      return arrayN
  }


  console.log(receberValoresIndefinidos(1,3,4));

  
  //console.log([..."William"]);
  //console.log(..."William");