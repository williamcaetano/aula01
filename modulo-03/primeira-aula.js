
//console.log("Cheguei");

//console.log(teste1);

var teste = "123";
let teste1 = "1233";
const teste2 = 1222;

var teste = "111";
//teste1 = "222";
//teste2 = "aqui";
//console.log(teste2);

//console.log(teste1);

{
    let teste1 = "Aqui mudou";
    //console.log(teste1);
}

//console.log(teste1);

const pessoa = {
    nome: "William",
    idade: 22,
    endereco: {
        logradouro: "rua da esquerda",
        numero: 123
    }
}

Object.freeze(pessoa);

pessoa.nome = "William C";

 //console.log(pessoa);

function somar(valor1, valor2){
    console.log(valor1 + valor2);
}

//somar("Aqui", 3);

function ondeMoro(cidade){
    console.log("Eu moro em "+ cidade + " E sou muito feliz");
    console.log(`Eu moro em ${cidade} e sou muito feliz`);
}

//ondeMoro("Gravataí");

function fruteira(){
    let texto = "Banana"
                + "\n"
                +"Ameixa"
                + "\n"
                +"Goiaba"
                + "\n"
                +"Pessego"
                + "\n";
    let newTexto = `
    Banana
    Ameixa
    Goiaba
    Pessego`;
    
    //console.log(newTexto);
}

fruteira();
console.log("\n");

function quemSou(pessoa){
    console.log(`Meu nome é ${pessoa.nome} e tenho ${pessoa.idade} anos e moro na ${pessoa.endereco.logradouro} numero ${pessoa.endereco.numero}.`);
}

//quemSou(pessoa);

let funcaoSomaVar = function(a, b, c = 0){
    return a+b+c;
}

let add = funcaoSomaVar
let resultado = add(3,2);
//console.log(resultado);

const { nome:n, idade:i } = pessoa
const { endereco:{logradouro, numero} } = pessoa
//console.log(n, i);
//console.log(logradouro, numero);

const array = [1,3,4,8];
// se for let adicionar com array[] = valor;

const [n1, ,n2, ,n3 = 9] = array
console.log(n1, n2, n3);

function testarPessoa({ nome, idade }) {
    console.log(nome, idade);
}

testarPessoa(pessoa);

let a1 = 42;
let b1 = 15;

console.log(a1, b1);

[a1, b1] = [b1, a1];

console.log(a1, b1);