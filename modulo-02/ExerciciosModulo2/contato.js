let nome = document.getElementById('nome'), email = document.getElementById('email')

nome.addEventListener('blur', () =>{
    if(nome.value.length < 10){
        alert("Deve conter no mínimo 10 caracteres")
    }
})

email.addEventListener('blur', () =>{
    if( !email.value.includes('@')){
        alert("Deve conter um @")
    }
})

function validar(frm) {
    let inputs = document.getElementsByClassName('required');
    let valid = true;
    for(let i=0; i < inputs.length; i++){
        if (!inputs[i].value){ 
            valid = false; 
        }
     }
     if (!valid){
         alert('Por favor preencha todos os campos.');
       return false;
     } 
     else { 
         return true; 
    }
}
