

import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class ElfoNoturnoTest
{
   @Test
   
   public void atirarFlechaDiminuirFlechaAumentarXPDiminuirVidaDwarfDiminuirVidaElfoNoturno(){
       ElfoNoturno novoElfo = new ElfoNoturno("Legolas");
       Dwarf dwarf = new Dwarf("João");
        
       novoElfo.atirarFlecha(dwarf);
       
       assertEquals(3, novoElfo.getExp());
       assertEquals(3, novoElfo.getQtdFlecha());
       assertEquals(100.0, 110.0, dwarf.getVida());
       assertEquals(85.0, novoElfo.getVida(), 1e-9);
   }
   
   @Test
   
   public void elfoNoturnoParaDeAtirarFlechaQuandoZeraAVida(){
       ElfoNoturno novoElfo = new ElfoNoturno("Legolas");
       Dwarf dwarf = new Dwarf("João");
       
       novoElfo.incrementarFlecha(6);
       dwarf.equiparEscudo();
       for(int i=0; i<10;i++){
       novoElfo.atirarFlecha(dwarf);
      }
       
       assertEquals(21, novoElfo.getExp());
       assertEquals(3, novoElfo.getQtdFlecha());
       assertEquals(75.0, 110.0, dwarf.getVida());
       assertEquals(0, novoElfo.getVida(), 1e-9);
       assertEquals(Status.MORTO, novoElfo.getStatus());
   }
}
