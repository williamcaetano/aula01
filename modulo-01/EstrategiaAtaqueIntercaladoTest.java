

import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import java.util.*;

public class EstrategiaAtaqueIntercaladoTest
{
    @Test
    public void exercitoIntercalado50Por50(){
        EstrategiaAtaqueIntercalado estrategia = new EstrategiaAtaqueIntercalado();
        Elfo night1 = new ElfoNoturno("Noturno1");
        Elfo night2 = new ElfoNoturno("Noturno2");
        Elfo night3 = new ElfoNoturno("Noturno3");
        Elfo green1 = new ElfoVerde("Verde1");
        Elfo green2 = new ElfoVerde("Verde2");
        Elfo green3 = new ElfoVerde("Verde3");
        
        ArrayList<Elfo> elfosEnviadosPar = new ArrayList<>(
            Arrays.asList(night1, night2, green1, green2));
        
        ArrayList<Elfo> elfosEnviadosImpar = new ArrayList<>(
            Arrays.asList(night1, night2, green1, green2, night3));
           
        
        ArrayList<Elfo> elfosEsperadosPar = new ArrayList<>(
            Arrays.asList(green1, night2, green2, night1));//par
            
        ArrayList<Elfo> elfosEsperadosImpar = new ArrayList<>(
            Arrays.asList(green1, night2, green2, night1));//Impar
            
        ArrayList<Elfo> elfosResultadoPar = estrategia.EstrategiaAtaqueIntercalado(elfosEnviadosPar);
        ArrayList<Elfo> elfosResultadoImpar = estrategia.EstrategiaAtaqueIntercalado(elfosEnviadosImpar);
        
        assertEquals(elfosResultadoPar,elfosEsperadosPar);
        assertEquals(elfosResultadoImpar,elfosEsperadosImpar);
    }
}
