
public class TradutorParaIngles implements Tradutor
{
    public String traduzir(String texto){
        switch(texto){
            case "Sim":
                return "Yes";
            case "Obrigado":
            case "Obrigada":
                return "Thanks";    
            default : 
                return null;
        }
    }
}
