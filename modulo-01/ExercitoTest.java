
import java.util.*;
import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class ExercitoTest
{
    @Test
    
    public void adicionarElfosDeDescricoesCorretas(){
      Exercito novoExercito = new Exercito();
      ElfoDaLuz elfo1 = new ElfoDaLuz("Legolas");
      ElfoDaLuz elfo2 = new ElfoDaLuz("Jeremias");
      
      ElfoVerde elfo3 = new ElfoVerde("João");
      ElfoVerde elfo4 = new ElfoVerde("Irmão do João");
      
      ElfoNoturno elfo5 = new ElfoNoturno("Salomão");
      ElfoNoturno elfo6 = new ElfoNoturno("Pedro");
        
      novoExercito.alistar(elfo1);
      novoExercito.alistar(elfo2);
      novoExercito.alistar(elfo3);
      novoExercito.alistar(elfo4);
      novoExercito.alistar(elfo5);
      novoExercito.alistar(elfo6);
      
      assertEquals(4, novoExercito.buscar(Status.RECEM_CRIADO).size());
    }
    
    @Test
    
     public void verificarElfosComMesmoStatus(){
      Exercito novoExercito = new Exercito();
      Dwarf dwarf = new Dwarf("José");
      ElfoVerde elfo1 = new ElfoVerde("João");
      ElfoVerde elfo2 = new ElfoVerde("Irmão do João");
      ElfoNoturno elfo3 = new ElfoNoturno("Salomão");
      ElfoNoturno elfo4 = new ElfoNoturno("Pedro");
        
      novoExercito.alistar(elfo1);
      novoExercito.alistar(elfo2);
      novoExercito.alistar(elfo4);
      elfo3.atirarFlecha(dwarf);
      novoExercito.alistar(elfo3);
      
      assertEquals(3, novoExercito.buscar(Status.RECEM_CRIADO).size());
      assertEquals(1, novoExercito.buscar(Status.SOFREU_DANO).size());
    }
    
}
