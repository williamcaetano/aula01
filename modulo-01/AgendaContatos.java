import java.util.*;
public class AgendaContatos
{
    HashMap<String, String> contatos = new LinkedHashMap<>();
    
    public void adicionarContato(String nome, String telefone){
        this.contatos.put(nome, telefone);
    }
    
    public String obterTelefone(String nome){
        return this.contatos.get(nome);
    }
    
    public String obterContato(String telefone){
        for(HashMap.Entry<String,String> par : contatos.entrySet()){
            if(par.getValue().equals(telefone))
                return par.getKey();
        }
        return null;
    }
    
    /*
    public String obterContato(String telefone){
        Set<String> key = contatos.keySet();
        
        for(String nome : key){
            if (contatos.get(nome).equals(telefone))
                return nome;
        }
        return null;
    }
    */
    public String csv(){
        StringBuilder builder = new StringBuilder();
        String separator = System.lineSeparator();
        
         for(HashMap.Entry<String,String> par : contatos.entrySet()){
            String chave = par.getKey();
            String valor = par.getValue();
            String contato = String.format("%s,%s%s", chave,valor,separator);
            builder.append(contato);
        }
        return builder.toString();
    }
}
