

public class EstatisticasInventario
{
    private Inventario inventario;
    
    public EstatisticasInventario(Inventario inventario){
        this.inventario = inventario;
    }
    
    public double calcularMedia(){
        
        if(inventario.getItem().isEmpty())
            return Double.NaN;
             
        double media = 0, soma = 0;
        
        //ou um foreach (Item item: this.inventario.getItem())
        
        for(int i =0; i<inventario.getItem().size();i++){ 
            soma+= inventario.getItem().get(i).getQuantidade();
        }
        
        media = soma/inventario.getItem().size();
        return media;
    }
    
    public float calcularMediana(){
        if(inventario.getItem().isEmpty())
            return Float.NaN;
        this.inventario.ordenarItens(); 
        float medianaImpar = 0, qtdItens = inventario.mochila.size(), medianaPar = 0, media = 0; 
        
        if(qtdItens %2 == 1){
            medianaImpar = qtdItens/2;
            
            return inventario.getItem().get((int)medianaImpar).getQuantidade();
        }
        else{
            medianaPar = qtdItens/2;
            media = (float) ((inventario.getItem().get((int)medianaPar).getQuantidade())+
                            (inventario.getItem().get((int)(medianaPar-1)).getQuantidade()))/2;
            return media; 
        }
    }
    
    public int qtdItensAcimaDaMedia(){
        int cont = 0;
        double media = this.calcularMedia();
        //ou um foreach (Item item: this.inventario.getItem())
        
        for(int i =0; i<inventario.getItem().size();i++){ 
            if(inventario.getItem().get(i).getQuantidade() > media){
                cont++;
            }
        }
        
        return cont;
    }
    
    public Inventario getInventario(){
        return this.inventario;
    }
}
