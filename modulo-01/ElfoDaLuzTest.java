

import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;


public class ElfoDaLuzTest
{
    @Test
   
   public void atacarComEspadaIncrementarXpAumentarOuDiminuirVida(){
       ElfoDaLuz novoElfo = new ElfoDaLuz("Legolas");
       Dwarf dwarf = new Dwarf("João");
       //dwarf.equiparEscudo();
       
       for(int i = 0; i<9;i++){
        novoElfo.atacarComEspada(dwarf);
       }
       
       assertEquals(9, novoElfo.getExp());
       assertEquals(20.0, dwarf.getVida(), 1e-9);
       assertEquals(35.0, novoElfo.getVida(), 1e-9);
   }
}
