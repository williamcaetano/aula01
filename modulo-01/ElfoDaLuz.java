import java.util.*;

public class ElfoDaLuz extends Elfo
{   
    private final ArrayList<String> DESCRICOES_OBRIGATORIAS= new ArrayList<>(Arrays.asList(
    
    "Espada de Galvorn"));
    private int numAtaque = 0;
    private final double QTD_VIDA_GANHA = 10;
    
    public ElfoDaLuz(String nome){
        super(nome);
        super.ganharItem(new ItemSempreExistente(1,DESCRICOES_OBRIGATORIAS.get(0)));
        this.qtdExpPorAtaque = 1;
        this.qtdDano = 21;
    }
    
    public Item getEspada(){
        return this.getInventario().buscar(DESCRICOES_OBRIGATORIAS.get(0));
    }
    
    public void perderItem(Item item){
        boolean possoPerder = !DESCRICOES_OBRIGATORIAS.contains(item.getDescricao());
        if(possoPerder)
            super.perderItem(item);
    }
    
    public void ganharVida(){
        this.vida+=QTD_VIDA_GANHA;
    }
    
    public boolean devePerderVida(){
        return this.numAtaque % 2 == 1;
    }
    
     public void atacarComEspada(Dwarf dwarf){
        if(podeSofrerDano() && getEspada().getQuantidade()>0){
            this.incrementarExp();
            dwarf.decrementarVida();
            this.numAtaque++;
            if(devePerderVida()){
                this.vida -= this.vida >= calcularDano() ? calcularDano() : this.vida;
                this.status = Status.SOFREU_DANO;
            }    
            else
                ganharVida();
        }
        
        if(this.vida == 0){
            this.status = Status.MORTO; 
        }
    }
    
    public double calcularDano(){
        return this.qtdDano;
    }
}
