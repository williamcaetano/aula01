
public class ElfoNoturno extends Elfo
{   
    public ElfoNoturno(String nome){
        super(nome);
        this.qtdExpPorAtaque = 3;
        this.qtdDano = 15;
    }
    
    public double calcularDano(){
        return this.qtdDano;
    }
    
    @Override
     public void atirarFlecha(Dwarf dwarf){
        int qntAtual = this.getFlecha().getQuantidade();
        if(podeSofrerDano()){
            if(podeAtirarFlecha()){
                inventario.obter(0).setQuantidade(qntAtual - 1);
                this.incrementarExp();
                dwarf.decrementarVida();
                this.vida-=this.vida>=calcularDano() ? calcularDano() : this.vida;
                this.status = Status.SOFREU_DANO;
            }
        }
        
        if(this.vida == 0){
            this.status = Status.MORTO; 
        }
    }
    
    @Override
    
    public boolean podeSofrerDano(){
        return this.vida > 0;
    }
}
