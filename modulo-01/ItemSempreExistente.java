
public class ItemSempreExistente extends Item
{
    public ItemSempreExistente(int quantidade, String descricao){
        super(quantidade,descricao);
    }
    
    public void setQuantidade(int qtd){
        boolean podeAlterar = qtd >0;
        this.quantidade = podeAlterar ? qtd : 1;
    }
}
