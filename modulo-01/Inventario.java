import java.util.*;

public class Inventario
{
    ArrayList<Item> mochila = new ArrayList<>();
    
    public void adicionaItem(Item item){
        mochila.add(item);
    }
    
    public Item obter(int pos){
        if (pos >= mochila.size()){
            return null;
        }
        
        return mochila.get(pos);
    }
    
    public void remover(int pos){
        mochila.remove(pos);
    }
    
    public String getDescricaoStringBuilder() { 
        StringBuilder descricoes = new StringBuilder(); 
        
        for(int i = 0; i < this.mochila.size() ; i++){ 
            Item item = this.mochila.get(i);
            if (item != null){
                descricoes.append(item.getDescricao());
                descricoes.append(",");
            }
        }
        
        return (descricoes.length() > 0 ? 
               descricoes.substring(0, (descricoes.length()-1)) 
                        :
               descricoes.toString());
    }
    
    public String obterDescricoes(){
        String s = mochila.get(0).getDescricao();
        int i = 1;
        while (i<mochila.size()){
            s+= ", "+mochila.get(i).getDescricao();
            i++;
        }
        
        return s;
    }
    
    public Item itemComMaiorQuantidade(){
        int indice = 0, maiorQuantidade = 0;
        
        for(int i = 0; i< this.mochila.size();i++){
            Item item = this.mochila.get(0);
            
            if (mochila.get(i).getQuantidade() > maiorQuantidade){
                
                maiorQuantidade = item.getQuantidade();
                indice = i;
            }
        }
        
        return this.mochila.size() > 0 ? this.mochila.get(indice) : null;
    }
    
    public ArrayList<Item> getItem(){ 
        return this.mochila;
    }
    
    public Item buscar(String nome){
        
        //  ou (Item itemAtual : this.mochila)
        
        for(int i = 0; i<mochila.size();i++){
            if(mochila.get(i).getDescricao().equals(nome)){
                return mochila.get(i);
            }
        }
        
        return null;
    }
    
    public ArrayList<Item> inverter(){
        ArrayList<Item> listaInvertida = new ArrayList<Item>();
        
        for(int i = mochila.size()-1 ; i>=0 ;i--){
            listaInvertida.add(this.mochila.get(i));
        }
        
        return listaInvertida;
    }
    
    public String ordenarItensTeste(){
        //Ordenação do tipo bubble.
        
        int k = mochila.size()-1;
        
        while (k>0){
            int last = 0;
            for(int j = 0; j < k; j++){
                if(mochila.get(j).getQuantidade() > mochila.get(j+1).getQuantidade()){
                    Item aux = mochila.get(j);
                    mochila.set(j, mochila.get(j+1));
                    mochila.set((j+1), aux);
                    last = j;
                }
            }
            k = last;
        }
        
        String s = " "+mochila.get(0).getQuantidade();
        int i = 1;
        while (i<mochila.size()){
            s+= ", "+mochila.get(i).getQuantidade();
            i++;
        }
        
        return s;
    }
    
    public void ordenarItens(){
        //Ordenação do tipo bubble.
        
        int i = mochila.size()-1;
        
        while (i>0){
            int last = 0;
            for(int j = 0; j < i; j++){
                if(mochila.get(j).getQuantidade() > mochila.get(j+1).getQuantidade()){
                    Item aux = mochila.get(j);
                    mochila.set(j, mochila.get(j+1));
                    mochila.set((j+1), aux);
                    last = j;
                }
            }
            i = last;
        }
        
    }
    
    public void ordenarItensDesc(){
        //Ordenação do tipo bubble.
        
        int i = mochila.size()-1;
        
        while (i>0){
            int last = 0;
            for(int j = 0; j < i; j++){
                if(mochila.get(j).getQuantidade() < mochila.get(j+1).getQuantidade()){
                    Item aux = mochila.get(j);
                    mochila.set(j, mochila.get(j+1));
                    mochila.set((j+1), aux);
                    last = j;
                }
            }
            i = last;
        }
        
    }
    
    public String ordenarItensDescTeste(){
        //Ordenação do tipo bubble.
        
        int k = mochila.size()-1;
        
        while (k>0){
            int last = 0;
            for(int j = 0; j < k; j++){
                if(mochila.get(j).getQuantidade() < mochila.get(j+1).getQuantidade()){
                    Item aux = mochila.get(j);
                    mochila.set(j, mochila.get(j+1));
                    mochila.set((j+1), aux);
                    last = j;
                }
            }
            k = last;
        }
        
        String s = " "+mochila.get(0).getQuantidade();
        int i = 1;
        while (i<mochila.size()){
            s+= ", "+mochila.get(i).getQuantidade();
            i++;
        }
        
        return s;
    }
    
    public String ordenarItensTeste(TipoOrdenacao tipo){
        if(tipo == TipoOrdenacao.ASC){
            return this.ordenarItensTeste();
        }
        else
            return this.ordenarItensDescTeste();
    }
    
    public void ordenarItens(TipoOrdenacao tipo){
        if(tipo == TipoOrdenacao.ASC){
            this.ordenarItens();
        }
        else
            this.ordenarItensDesc();
    }
 }
