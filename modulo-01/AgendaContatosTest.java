

import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class AgendaContatosTest
{
    @Test
    
    public void encontraNumeroPeloNome(){
        AgendaContatos agenda = new AgendaContatos();
        
        agenda.contatos.put("Maria","987654");
        agenda.contatos.put("João","123456");
        agenda.contatos.put("José","123987");
        
        assertEquals("987654", agenda.obterTelefone("Maria"));
    }
    
    @Test
    
    public void encontraNumeroPeloTelefone(){
        AgendaContatos agenda = new AgendaContatos();
        
        agenda.contatos.put("Maria","987654");
        agenda.contatos.put("João","123456");
        agenda.contatos.put("José","654789");
        
        assertEquals("Maria", agenda.obterContato("987654"));
        assertEquals("João", agenda.obterContato("123456"));
        assertNull(agenda.obterContato("654754"));
    }
    
    public void adicionarContatoEPesquisar(){
        AgendaContatos agenda = new AgendaContatos();
        
        agenda.adicionarContato("Maria","987654");
        agenda.adicionarContato("João","123456");
        agenda.adicionarContato("José","654789");
        
        String separador = System.lineSeparator();
        String esperado = String.format("Maria,987654%sJoão,123456%sJosé,654789%s",separador, separador, separador);
        
        assertEquals(esperado, agenda.csv());
    }
}
