
import java.util.*;
import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

/**
 * A classe de teste InventarioTest.
 *
 * @author  (seu nome)
 * @version (um número de versão ou data)
 */
public class InventarioTest
{
    
    
    @Test
    public void adicionarDoisItens(){
        Inventario inventario = new Inventario();
        Item espada = new Item(1,"espada");
        Item escudo = new Item(2,"escudo");
        
        inventario.adicionaItem(espada);
        inventario.adicionaItem(escudo);
        
        assertEquals(espada, inventario.getItem().get(0));
        assertEquals(escudo, inventario.getItem().get(1));
    }
   
     @Test
    public void ObterItemAdicionado(){
        Inventario inventario = new Inventario();
        Item espada = new Item(1,"espada");
        
        inventario.adicionaItem(espada);
        
        assertEquals(espada, inventario.getItem().get(0));
    }
    
    @Test
    public void renoverItemAntesDeAdicionarProximo(){
        Inventario inventario = new Inventario();
        
        Item espada = new Item(1,"espada");
        Item escudo = new Item(2,"escudo");
        
        inventario.adicionaItem(espada);
        inventario.remover(0);
        inventario.adicionaItem(escudo);
        assertEquals(escudo, inventario.getItem().get(0));
        assertEquals(1, inventario.getItem().size());
    }
    
     @Test
    public void getDescricoesVariosItens(){
        Inventario inventario = new Inventario();
        
        Item espada = new Item(1,"espada");
        Item escudo = new Item(2,"escudo");
        Item armadura = new Item(1,"armadura");
        
        inventario.adicionaItem(espada);
        inventario.adicionaItem(armadura);
        inventario.adicionaItem(escudo);
        
        assertEquals("espada,armadura,escudo", inventario.getDescricaoStringBuilder());
       
    }
    
     @Test
    public void getDescricoesNenhumItem() {
        Inventario inventario = new Inventario();
        
        assertEquals("", inventario.getDescricaoStringBuilder());
    }
    
    @Test
    
    public void getItemMaiorQuantidadeComVarios(){
        Inventario inventario = new Inventario();
        
        Item espada = new Item(1,"espada");
        Item escudo = new Item(2,"escudo");
        Item armadura = new Item(1,"armadura");
        
        inventario.adicionaItem(espada);
        inventario.adicionaItem(armadura);
        inventario.adicionaItem(escudo);
        
        assertEquals(escudo, inventario.itemComMaiorQuantidade());
    }
    
    @Test
    
    public void getItemMaiorQuantidadeComInventarioVazio(){
        Inventario inventario = new Inventario();
        
        assertNull(inventario.itemComMaiorQuantidade());
    }
    
    @Test
    
    public void getItemMaiorQuantidadeComItensMesmaQuantidade(){
        Inventario inventario = new Inventario();
        
        Item espada = new Item(1,"espada");
        Item escudo = new Item(1,"escudo");
        Item armadura = new Item(1,"armadura");
        
        inventario.adicionaItem(espada);
        inventario.adicionaItem(armadura);
        inventario.adicionaItem(escudo);
        
        assertEquals(espada, inventario.itemComMaiorQuantidade());
    }
    
    @Test
    
    public void getItemComInventarioVazio(){
        Inventario inventario = new Inventario();
        
        assertNull(inventario.buscar("capa"));
    }
    
    @Test
    
    public void acharNomeDeItem(){
        Inventario inventario = new Inventario();
        
        Item espada = new Item(1,"espada");
        Item escudo = new Item(1,"escudo");
        Item armadura = new Item(1,"armadura");
        
        inventario.adicionaItem(espada);
        inventario.adicionaItem(armadura);
        inventario.adicionaItem(escudo);
        
        assertEquals(espada, inventario.buscar("espada"));
    }
    
    /*
    
    @Test
    
    public void exibirListaInvertida(){
        Inventario inventario = new Inventario();
        ArrayList<Item> inverso = new ArrayList<Item>();
        
        Item espada = new Item(1,"espada");
        Item escudo = new Item(1,"escudo");
        Item armadura = new Item(1,"armadura");
        
        inventario.adicionaItem(espada);
        inventario.adicionaItem(armadura);
        inventario.adicionaItem(escudo);
        
        inverso.add(escudo);
        inverso.add(armadura);
        inverso.add(espada);
        
        
        assertEquals(inverso, inventario.inverter());
    }
    */
    
   
   
    public void inverterComDoisItens(){
        Inventario inventario = new Inventario();
        
        Item espada = new Item(1,"espada");
        Item escudo = new Item(1,"escudo");
        
        inventario.adicionaItem(espada);
        inventario.adicionaItem(escudo);
        
        assertEquals(escudo, inventario.inverter().get(0));
        assertEquals(2, inventario.inverter().size());
    }
   
   
    @Test
    
    public void testarMetodoOrdenacao(){
        Inventario inventario = new Inventario();
        
        Item bracelete = new Item(8,"bracelete");
        Item espada = new Item(3,"espada");
        Item escudo = new Item(2,"escudo");
        Item armadura = new Item(1,"armadura");
        Item flecha = new Item(4,"flecha");
        
        inventario.adicionaItem(bracelete);
        inventario.adicionaItem(espada);
        inventario.adicionaItem(armadura);
        inventario.adicionaItem(escudo);
        inventario.adicionaItem(flecha);
        
        assertEquals(" 1, 2, 3, 4, 8", inventario.ordenarItensTeste());
    }
    
    @Test
    
        public void testarMetodoOrdenacaoDesc(){
        Inventario inventario = new Inventario();
        
        Item bracelete = new Item(8,"bracelete");
        Item espada = new Item(3,"espada");
        Item escudo = new Item(2,"escudo");
        Item armadura = new Item(1,"armadura");
        Item flecha = new Item(4,"flecha");
        
        inventario.adicionaItem(bracelete);
        inventario.adicionaItem(espada);
        inventario.adicionaItem(armadura);
        inventario.adicionaItem(escudo);
        inventario.adicionaItem(flecha);
        
        assertEquals(" 8, 4, 3, 2, 1", inventario.ordenarItensDescTeste());
    }
    
    @Test
    
    public void testarMetodoOrdenacaoTipoOrdenacaoAsc(){
        Inventario inventario = new Inventario();
        
        Item bracelete = new Item(8,"bracelete");
        Item espada = new Item(3,"espada");
        Item escudo = new Item(2,"escudo");
        Item armadura = new Item(1,"armadura");
        Item flecha = new Item(4,"flecha");
        
        inventario.adicionaItem(bracelete);
        inventario.adicionaItem(espada);
        inventario.adicionaItem(armadura);
        inventario.adicionaItem(escudo);
        inventario.adicionaItem(flecha);
        
        assertEquals(" 1, 2, 3, 4, 8", inventario.ordenarItensTeste(TipoOrdenacao.ASC));
    }
    
    @Test
    
    public void testarMetodoOrdenacaoTipoOrdenacaoDesc(){
        Inventario inventario = new Inventario();
        
        Item bracelete = new Item(8,"bracelete");
        Item espada = new Item(3,"espada");
        Item escudo = new Item(2,"escudo");
        Item armadura = new Item(1,"armadura");
        Item flecha = new Item(4,"flecha");
        
        inventario.adicionaItem(bracelete);
        inventario.adicionaItem(espada);
        inventario.adicionaItem(armadura);
        inventario.adicionaItem(escudo);
        inventario.adicionaItem(flecha);
        
        assertEquals(" 8, 4, 3, 2, 1", inventario.ordenarItensTeste(TipoOrdenacao.DESC));
    }
}
