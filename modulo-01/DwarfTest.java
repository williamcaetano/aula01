

import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

/**
 * A classe de teste DwarfTest.
 *
 * @author  (seu nome)
 * @version (um número de versão ou data)
 */
public class DwarfTest {   
    private final double DELTA = 1e-9;
    
    @Test
    
    public void dwarfNasceCom110DeVida(){
        Dwarf dwarf = new Dwarf("Anão");
        
       
        assertEquals(110, dwarf.getVida(), DELTA);
    }
    
    @Test
    
    public void dwarfPerde10DeVida(){
        Dwarf dwarf = new Dwarf("Anão");
        dwarf.decrementarVida();
       
        assertEquals(100, dwarf.getVida(), DELTA);
    }
    
    @Test
    
    public void dwarfPerdeTodaVida11Ataques(){
       
       Dwarf dwarf = new Dwarf("Anão");
        
       for (int i = 0; i<12 ; i++){
           dwarf.decrementarVida();
       }
       
       assertEquals(0, dwarf.getVida(), DELTA);
    }
    
    @Test
    
     public void dwarfNasceComStatus(){
       Dwarf dwarf = new Dwarf("dwarf");
       
       assertEquals(Status.RECEM_CRIADO, dwarf.getStatus());
    }
    
     
    @Test
    
    public void dwarfPerdeVidaEDeveMorrer(){
       Dwarf dwarf = new Dwarf("Anão");
       
       for (int i = 0; i<12 ; i++){
           dwarf.decrementarVida();
       }
       
       assertEquals(Status.MORTO, dwarf.getStatus());
       assertEquals(0.0, dwarf.getVida(), DELTA);
    }
    
    @Test
    
    public void dwarfPerdeMetadeDaVidaComEscudoEquipado(){
       Dwarf dwarf = new Dwarf("Anão");
       dwarf.equiparEscudo();
       
       for (int i = 0; i<11 ; i++){
           dwarf.decrementarVida();
       }
       
       assertEquals(55, dwarf.getVida(), DELTA);
    }
    
    @Test
    
    public void dwarfPerdeTodaVidaComEscudoEquipado(){
       Dwarf dwarf = new Dwarf("Anão");
       dwarf.equiparEscudo();
       
       for (int i = 0; i<22 ; i++){
           dwarf.decrementarVida();
       }
       
       assertEquals(0, dwarf.getVida(), DELTA);
    }
    
    @Test
    
    public void dwarfNasceComEscudoNoInventario(){
       Personagem dwarf = new Dwarf("Anão");
       
       assertEquals("Escudo", dwarf.getInventario().getItem().get(0).getDescricao());
    }
}
