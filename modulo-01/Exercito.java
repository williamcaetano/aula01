import java.util.*;
import java.util.Collections;
import java.lang.*;
 public class Exercito{
    private final ArrayList<Class> TIPOS_PERMITIDOS = new ArrayList<>(
        Arrays.asList(
            ElfoVerde.class,
            ElfoNoturno.class
        )
    );    
    
    private ArrayList<Elfo> exercitoElfos = new ArrayList<>();
    private HashMap<Status, ArrayList<Elfo>> porStatus = new HashMap<>();
    
    /*
    public ArrayList<Elfo> getOrdemDeAtaque(ArrayList<Elfo> atacantes){
        ArrayList<Elfo> exercitoVivos = new ArrayList<>();
        ArrayList<Elfo> exercitoAjustado = new ArrayList<>();
        exercitoVivos = buscarStatusDiferente(Status.MORTO,atacantes);
        for(int i=0; i<exercitoVivos.size();i++){
            if(exercitoVivos.get(i) instanceof ElfoVerde){
                exercitoAjustado.add(exercitoVivos.get(i));
            }
        }
        for(int j=0; j<exercitoVivos.size();j++){
            if(exercitoVivos.get(j) instanceof ElfoNoturno){
                exercitoAjustado.add(exercitoVivos.get(j));
            }
        }
        return exercitoAjustado;
    }
    
    public ArrayList<Elfo> retornaMetadeIntercalado(ArrayList<Elfo> exercitoVerdesENoturnos){
        ArrayList<Elfo> exercitoVerdesENoturnosMeioAMeio = new ArrayList<>();
        int tamanho = exercitoVerdesENoturnos.size();
        int i = 0; int j = exercitoVerdesENoturnos.size()-1;
        int k = 0;
        while(k<(tamanho/2) && i<j){
           exercitoVerdesENoturnosMeioAMeio.add(exercitoVerdesENoturnos.get(i++));
           exercitoVerdesENoturnosMeioAMeio.add(exercitoVerdesENoturnos.get(j--));   
        }
        return exercitoVerdesENoturnosMeioAMeio;
    }
    
    public ArrayList<Elfo> retornaMetadeIntercalado2(ArrayList<Elfo> exercitoVerdesENoturnos){
        ArrayList<Elfo> exercitoVerdesENoturnosMeioAMeio = new ArrayList<>();
        int tamanho = exercitoVerdesENoturnos.size();
        int i = 0; int j = exercitoVerdesENoturnos.size()-1;
        int k = 0;
        while(k<(tamanho/2) && i<j){
           exercitoVerdesENoturnosMeioAMeio.add(exercitoVerdesENoturnos.get(j--));
           exercitoVerdesENoturnosMeioAMeio.add(exercitoVerdesENoturnos.get(i++));   
        }
        return exercitoVerdesENoturnosMeioAMeio;
    }
    
    public ArrayList<Elfo> ataqueIntercalado(ArrayList<Elfo> atacantes){
        ArrayList<Elfo> exercitoVivos, exercitoVerdesENoturnos = new ArrayList<>();
        ArrayList<Elfo> exercitoSorteio = new ArrayList<>();
        ArrayList<Elfo> exercitoVerdesENoturnosMeioAMeioIntercalado2 = new ArrayList<>(); 
        ArrayList<Elfo> exercitoVerdesENoturnosMeioAMeioIntercalado1 = new ArrayList<>(); 
        exercitoVivos = buscarStatusDiferente(Status.MORTO,atacantes);
        exercitoVerdesENoturnos = this.getOrdemDeAtaque(exercitoVivos);
        exercitoVerdesENoturnosMeioAMeioIntercalado1 = this.retornaMetadeIntercalado(exercitoVerdesENoturnos);
        exercitoVerdesENoturnosMeioAMeioIntercalado2 = this.retornaMetadeIntercalado2(exercitoVerdesENoturnos);
        
        int sorteado = this.sortear();
        if(sorteado ==1)
            exercitoSorteio = exercitoVerdesENoturnosMeioAMeioIntercalado1;
        else
            exercitoSorteio = exercitoVerdesENoturnosMeioAMeioIntercalado2;
            
        return exercitoSorteio;   
    }
    
    public boolean compare(Elfo elfo1, Elfo elfo2){
        return elfo1.getClass().equals(elfo2.getClass());
    }
    */
    public void alistar(Elfo elfo){
        boolean podeAlistar = TIPOS_PERMITIDOS.contains(elfo.getClass());
        if(podeAlistar){
            exercitoElfos.add(elfo);
            ArrayList<Elfo> elfoDoStatus = porStatus.get(elfo.getStatus());
            if(elfoDoStatus == null){
                elfoDoStatus = new ArrayList<>();
                porStatus.put(elfo.getStatus(), elfoDoStatus);
            }
            elfoDoStatus.add(elfo);
        }
    }
    
    public void alistarInstance(Elfo elfo){
        if(elfo instanceof ElfoVerde || elfo instanceof ElfoNoturno)
            this.exercitoElfos.add(elfo);
    }
    
    public ArrayList<Elfo> getElfos(){
        return this.exercitoElfos;
    }
   
    public ArrayList<Elfo> buscar(Status status){
        return this.porStatus.get(status);
    }
    
    public ArrayList<Elfo> buscarStatusDiferente(Status status, ArrayList<Elfo> elfo){
        ArrayList<Elfo> listaStatus = new ArrayList<>();
        for(Elfo elfoAtual : elfo){
            if(elfoAtual.getStatus() != status)
                listaStatus.add(elfoAtual);
        }
        return listaStatus;
    }
    
    public ArrayList<Elfo> buscarStatus(Status status){
        ArrayList<Elfo> listaStatus = new ArrayList<>();
        
        for(Elfo elfoAtual : this.exercitoElfos){
            if(elfoAtual.getStatus() != status)
                listaStatus.add(elfoAtual);
        }
        return listaStatus;
    }
}
