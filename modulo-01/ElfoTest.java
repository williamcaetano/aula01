

import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

/**
 * A classe de teste ElfoTest.
 *
 * @author  (seu nome)
 * @version (um número de versão ou data)
 */
public class ElfoTest
{  
   @After
   
   public void tearDown(){
       System.gc();
   }
    
   @Test
   
   public void atirarFlechaDiminuirFlechaAumentarXPDiminuirVidaDwarf(){
       Elfo novoElfo = new Elfo("Legolas");
       Dwarf dwarf = new Dwarf("João");
        
       novoElfo.atirarFlecha(dwarf);
       novoElfo.atirarFlecha(dwarf);
        
       
       assertEquals(2, novoElfo.getExp());
       assertEquals(2, novoElfo.getQtdFlecha());
       assertEquals(90.0, 110.0, dwarf.getVida());
   }
   
   @Test
   
   public void elfosNascemComStatusRecemCriado(){
       Elfo novoElfo = new Elfo("Legolas");
       
       assertEquals(Status.RECEM_CRIADO, novoElfo.getStatus());
   }
   
   @Test
   
   public void criarElfosIncrementarContador(){
      new Elfo("Legolas");
      new Elfo("Jeremias");
      
      new Elfo("João");
      new Elfo("Irmão do João");
      
      new Elfo("Salomão");
      new Elfo("Pedro");
      
      assertEquals(6, Elfo.getQtdElfos());
    }
}
