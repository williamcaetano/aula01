

import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;


public class PaginadorInventarioTest
{
    @Test
    
    public void testarPaginador(){
        Inventario inventario = new Inventario();
        PaginadorInventario paginador = new PaginadorInventario(inventario);
        
        Item espada = new Item(5,"espada");
        Item escudo = new Item(4,"escudo");
        Item flecha = new Item(7,"flecha");
        Item bracelete = new Item(6,"bracelete");
        Item armadura = new Item(9,"armadura");
        Item bonus = new Item(1,"bonus xp");
        
        inventario.adicionaItem(espada);
        inventario.adicionaItem(escudo);
        inventario.adicionaItem(flecha);
        inventario.adicionaItem(bracelete);
        inventario.adicionaItem(armadura);
        inventario.adicionaItem(bonus);
        paginador.pular(2);
        
        assertEquals(flecha, paginador.limitar(3).get(0));
        assertEquals(bracelete, paginador.limitar(3).get(1));
        assertEquals(armadura, paginador.limitar(3).get(2));
    }
}
