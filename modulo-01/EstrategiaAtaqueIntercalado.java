import java.util.*;
public class EstrategiaAtaqueIntercalado extends EstrategiaPriorizandoElfosVerdes
{
    public ArrayList<Elfo> EstrategiaAtaqueIntercalado(ArrayList<Elfo> atacantes){
        ArrayList<Elfo> exercitoOrdenado = new ArrayList<>();
        ArrayList<Elfo> exercitoIntercalado = new ArrayList<>();
        exercitoOrdenado = this.getOrdemDeAtaque(atacantes);
        int tamanhoPar = exercitoOrdenado.size()-1;
        int tamanhoImpar = exercitoOrdenado.size()-2;
        int i = 0; int tamanho = exercitoOrdenado.size();
        
        if(tamanho%2 == 0){
            while(i<tamanhoPar){
                exercitoIntercalado.add(exercitoOrdenado.get(i++));
                exercitoIntercalado.add(exercitoOrdenado.get(tamanhoPar--));
            }
        }
        else{
            while(i<tamanhoImpar){
                exercitoIntercalado.add(exercitoOrdenado.get(i++));
                exercitoIntercalado.add(exercitoOrdenado.get(tamanhoImpar--));
            }
        }
        return exercitoIntercalado;
    }
    
}
