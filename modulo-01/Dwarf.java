

public class Dwarf extends Personagem {
    private boolean escudoEquipado;
    
    
    public void equiparEscudo(){
        this.escudoEquipado = true;
    }
    
    public Dwarf(String nome){
        super(nome);
        //this.nome = nome;
        this.inventario.adicionaItem(new Item(1, "Escudo"));
        this.vida = 110.0;
        this.escudoEquipado = false;
    }
    
    public double calcularDano(){
        return this.escudoEquipado ? 5.0 : 10.0;
    }
    
    public boolean escudoEquipado(){
        return this.escudoEquipado;
    }
    
    public void decrementarVida(){  
        
        if(this.podeSofrerDano()){ 
           this.vida = this.vida >= this.calcularDano() ?
           this.vida - this.calcularDano() : 0.0;
           this.status = Status.SOFREU_DANO;
           if(this.vida == 0){
            this.status = Status.MORTO; 
           }
        }
    }
    
    public String imprimirResultado(){
        return "";
    }
} 

