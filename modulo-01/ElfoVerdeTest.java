

import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;


public class ElfoVerdeTest
{
    @Test
   
   public void atirarFlechaDiminuirFlechaDobrarXPDiminuirVidaDwarf(){
       ElfoVerde novoElfo = new ElfoVerde("Legolas");
       Dwarf dwarf = new Dwarf("João");
        
       novoElfo.atirarFlecha(dwarf);
       novoElfo.atirarFlecha(dwarf);
        
       
       assertEquals(4, novoElfo.getExp());
       assertEquals(2, novoElfo.getQtdFlecha());
       assertEquals(90.0, 110.0, dwarf.getVida());
       
   }
   
   @Test
   
   public void elfoVerdeNasceSomenteCom2Itens(){
       ElfoVerde novoElfo = new ElfoVerde("Legolas");
       Dwarf dwarf = new Dwarf("João");
       
       assertEquals(2, novoElfo.inventario.mochila.size());
   }
   
    @Test
   
   public void testeAdicionarERemoverItemBloqueado(){
       ElfoVerde novoElfo = new ElfoVerde("Legolas");
       
       novoElfo.ganharItem(new Item (3,"flechas"));
       novoElfo.perderItem(novoElfo.inventario.obter(1));
       
       assertEquals(2, novoElfo.inventario.mochila.size());
  }
  
  @Test
   
   public void elfoVerdeAdicionaItemComDescricaoValida(){
       ElfoVerde novoElfo = new ElfoVerde("Legolas");
       Item arcoDeVidro = new Item(1,"Arco de Vidro");
       
       novoElfo.ganharItem(arcoDeVidro);
       
       Inventario inventario = novoElfo.getInventario();
       
       
       assertEquals(new Item(1,"Arco"), novoElfo.inventario.obter(1));
       assertEquals(new Item(4,"Flecha"), novoElfo.inventario.obter(0));
       assertEquals(new Item(1,"Arco de Vidro"), novoElfo.inventario.obter(2));
  }
   
   @Test
   
   public void testeAdicionarERemoverItem(){
       ElfoVerde novoElfo = new ElfoVerde("Legolas");
       
       novoElfo.ganharItem(new Item (1,"Espada de aço Valiriano"));
       novoElfo.ganharItem(new Item (2,"Flecha de Vidro"));
       novoElfo.perderItem(novoElfo.inventario.obter(2));
       
       assertEquals(3, novoElfo.inventario.mochila.size());
  }
  
  @Test
   
   public void elfoVerdeAdicionaItemComDescricaoInvalida(){
       ElfoVerde novoElfo = new ElfoVerde("Legolas");
       Item arcoDeMadeira = new Item(1,"Arco de Madeira");
       
       novoElfo.ganharItem(arcoDeMadeira);
       
       Inventario inventario = novoElfo.getInventario();
       
       assertEquals(new Item(1,"Arco"), novoElfo.inventario.obter(1));
       assertEquals(new Item(4,"Flecha"), novoElfo.inventario.obter(0));
       assertNull(novoElfo.inventario.buscar("Arco de Madeira"));
  }
}
