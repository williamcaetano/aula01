
public abstract class Personagem
{
    protected String nome;
    protected double vida;
    protected Status status;
    protected Inventario inventario;
    protected int exp;
    
    {
        this.status = Status.RECEM_CRIADO;
        this.exp = 0;
        this.inventario = new Inventario();
    }
    
    protected Personagem(String nome){
        this.nome = nome;
    }
    
    protected void setNome(String nome){
        this.nome = nome;
    } 
    
    protected String getNome(){
        return this.nome;
    }
    
    protected int getExp(){
        return exp;
    }
    
    protected Status getStatus(){
        return this.status;   
    }
    
    protected double getVida(){
        return this.vida;
    }
    
    protected Inventario getInventario(){ 
        return this.inventario;
    }
    
    protected void ganharItem(Item item){
        this.inventario.adicionaItem(item);
    }
    
    protected void perderItem(Item item){
        this.inventario.mochila.remove(item); 
    }
    
    protected boolean podeSofrerDano(){
        return this.vida > 0;
    }
    
    protected abstract String imprimirResultado();
}
