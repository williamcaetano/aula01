import java.util.*;
public class PaginadorInventario
{
    private Inventario inventario;
    private int marcador;
    
    public PaginadorInventario(Inventario inventario){
        this.inventario = inventario;
    }
    
    public Inventario getInventario(){
        return this.inventario;
    }
    
    public void pular(int marcador){
        this.marcador = marcador > 0 ? marcador : 0;
    }
    
    public ArrayList<Item> limitar(int qtd){
        ArrayList<Item> subConjunto = new ArrayList<>();
        int fim = this.marcador+qtd;
        
        
        for(int i = this.marcador; i < fim && i < this.inventario.getItem().size(); i++){
            subConjunto.add(this.inventario.getItem().get(i));
        }
        
        return subConjunto;
    }
    
    /*
    public String paginador(int pular, int limitar){
        StringBuilder descricoes = new StringBuilder(); 
        
        for(int i = pular; i <(pular+limitar) ; i++){ 
            Item item = this.inventario.getItem().get(i);
            
            descricoes.append("("+item.getQuantidade()+","+item.getDescricao()+");");
        }
        
        return (descricoes.length() > 0 ? descricoes.substring(0, (descricoes.length()-1)) 
                        :
                descricoes.toString());
    }
    
    */
}
