import java.util.ArrayList;
import java.util.Arrays;

public class ElfoVerde extends Elfo{
    private final ArrayList<String> DESCRICOES_VALIDAS = new ArrayList<>(Arrays.asList(
    
    "Espada de aço Valiriano","Arco de Vidro","Flecha de Vidro"));
    
    ElfoVerde(String nome){
        super(nome);
        this.qtdExpPorAtaque = 2;
    }
    
    @Override
     public void ganharItem(Item item){
        boolean descricaoValida = DESCRICOES_VALIDAS.contains(item.getDescricao());
        if(descricaoValida)
           this.inventario.adicionaItem(item);
    }
    
    @Override
    public void perderItem(Item item){
        boolean descricaoValida = DESCRICOES_VALIDAS.contains(item.getDescricao());
        if(descricaoValida)
           this.inventario.mochila.remove(item);
    }
}
