import java.util.*;
public class EstrategiaPriorizandoElfosVerdes implements Estrategia
{   
    private ArrayList<Elfo> collections(ArrayList<Elfo> elfos){
        ArrayList<Elfo> elfosVivos = new ArrayList<>();
        elfosVivos = this.buscarStatusDiferente(Status.MORTO, elfos);
        
        Collections.sort(elfosVivos,new Comparator <Elfo>(){
            public int compare(Elfo elfoAtual, Elfo proximoElfo){
                boolean mesmoTipo = elfoAtual.getClass() == proximoElfo.getClass();
            
                if(mesmoTipo){
                    return 0;
                }
                
                return elfoAtual instanceof ElfoVerde && proximoElfo instanceof ElfoNoturno ? -1 : 1;
            }
        });
        return elfosVivos;
    }
    
    public ArrayList<Elfo> getOrdemDeAtaque(ArrayList<Elfo> atacantes){
        return this.collections(atacantes);
    }
    
    public ArrayList<Elfo> buscarStatusDiferente(Status status, ArrayList<Elfo> elfo){
        ArrayList<Elfo> listaStatus = new ArrayList<>();
        int i = 0;
        for(Elfo elfoAtual : elfo){
            if(elfoAtual.getStatus() != status)
                listaStatus.add(elfoAtual);
        }
        return listaStatus;
    }
}
