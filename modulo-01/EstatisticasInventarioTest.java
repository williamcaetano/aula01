

import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class EstatisticasInventarioTest
{   
    @Test
    
    public void calcularMediaInventarioVazio(){
        Inventario inventario = new Inventario();
        EstatisticasInventario estatistica = new EstatisticasInventario(inventario);
        
        assertTrue(Double.isNaN(estatistica.calcularMedia()));
    }
    
    @Test
    
    public void exibirMediaDasQuantidadesDosItensDaLista(){
        Inventario inventario = new Inventario();
        EstatisticasInventario estatistica = new EstatisticasInventario(inventario);
        
        Item espada = new Item(4,"espada");
        Item escudo = new Item(5,"escudo");
        Item armadura = new Item(2,"armadura");
        Item flecha = new Item(6,"flecha");
        
        inventario.adicionaItem(espada);
        inventario.adicionaItem(escudo);
        inventario.adicionaItem(armadura);
        inventario.adicionaItem(flecha);
        
        assertEquals(4.25, estatistica.calcularMedia(), 1e-9);
    }
    
    
    @Test
    
    public void exibirMedianaDasQuantidadesDosItensDaListaComTamanhoImpar(){
        Inventario inventario = new Inventario();
        EstatisticasInventario estatistica = new EstatisticasInventario(inventario);
        
        Item espada = new Item(4,"espada");
        Item escudo = new Item(5,"escudo");
        Item armadura = new Item(2,"armadura");
        Item flecha = new Item(6,"flecha");
        Item bracelete = new Item(7,"bracelete");
        
        inventario.adicionaItem(espada);
        inventario.adicionaItem(escudo);
        inventario.adicionaItem(armadura);
        inventario.adicionaItem(flecha);
        inventario.adicionaItem(bracelete);
        
        assertEquals(5, estatistica.calcularMediana(), 1e-9);
    }
    
    @Test
    
    public void exibirMedianaDasQuantidadesDosItensDaListaComTamanhoPar(){
        Inventario inventario = new Inventario();
        EstatisticasInventario estatistica = new EstatisticasInventario(inventario);
        
        Item espada = new Item(5,"espada");
        Item escudo = new Item(4,"escudo");
        Item flecha = new Item(7,"flecha");
        Item bracelete = new Item(6,"bracelete");
        Item armadura = new Item(9,"armadura");
        Item bonus = new Item(1,"bonus xp");
        
        inventario.adicionaItem(espada);
        inventario.adicionaItem(escudo);
        inventario.adicionaItem(flecha);
        inventario.adicionaItem(bracelete);
        inventario.adicionaItem(armadura);
        inventario.adicionaItem(bonus);
        
        inventario.ordenarItens();
        
        assertEquals(5.5, estatistica.calcularMediana(), 1e-9);
    }
    
    @Test
    
    public void teste(){
        Inventario inventario = new Inventario();
        EstatisticasInventario estatistica = new EstatisticasInventario(inventario);
        
        Item espada = new Item(5,"espada");
        Item escudo = new Item(4,"escudo");
        Item flecha = new Item(7,"flecha");
        Item bracelete = new Item(6,"bracelete");
        Item armadura = new Item(9,"armadura");
        Item bonus = new Item(1,"bonus xp");
        
        inventario.adicionaItem(espada);
        inventario.adicionaItem(escudo);
        inventario.adicionaItem(flecha);
        inventario.adicionaItem(bracelete);
        inventario.adicionaItem(armadura);
        inventario.adicionaItem(bonus);
        
        inventario.ordenarItens();
        
        
        assertEquals(6, inventario.getItem().size());
    }
    
    @Test
    
    public void testeMetodoQtdItensAcimaDaMedia(){
        Inventario inventario = new Inventario();
        EstatisticasInventario estatistica = new EstatisticasInventario(inventario);
        
        Item bonus = new Item(1,"bonus xp");
        Item espada = new Item(4,"espada");
        Item escudo = new Item(5,"escudo");
        Item armadura = new Item(2,"armadura");
        Item flecha = new Item(6,"flecha");
        Item bracelete = new Item(7,"bracelete");
        
        inventario.adicionaItem(espada);
        inventario.adicionaItem(escudo);
        inventario.adicionaItem(armadura);
        inventario.adicionaItem(flecha);
        inventario.adicionaItem(bracelete);
        inventario.adicionaItem(bonus);
        
        assertEquals(3, estatistica.qtdItensAcimaDaMedia());
    }
}
