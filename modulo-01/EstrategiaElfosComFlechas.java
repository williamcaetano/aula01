import java.util.*;

public class EstrategiaElfosComFlechas extends EstrategiaPriorizandoElfosVerdes
{
    public ArrayList<Elfo> somenteElfosComFlechas(ArrayList<Elfo> atacantes){
        ArrayList<Elfo> elfosComFlechas = new ArrayList<>();
        
        for(Elfo elfoAtual : atacantes){
            if(elfoAtual.getQtdFlecha() != 0)
                elfosComFlechas.add(elfoAtual);
        }
        return elfosComFlechas;
    }
    
    
    public ArrayList<Elfo> EstrategiaElfosComMaisFlechasPrimeiro(ArrayList<Elfo> atacantes){
        ArrayList<Elfo> elfosVivos = new ArrayList<>();
        ArrayList<Elfo> elfosComFlechas = new ArrayList<>();
        ArrayList<Elfo> elfosComFlechasDesc = new ArrayList<>();
        ArrayList<Elfo> elfosComFlechasDescTritaPorCentoNoturno = new ArrayList<>();
        
        elfosVivos = buscarStatusDiferente(Status.MORTO, atacantes);
        elfosComFlechas = this.somenteElfosComFlechas(elfosVivos);
        elfosComFlechasDesc = this.ordenarDescendente(elfosComFlechas);
        int i = this.quantidadeElfosNoturnosNoExercito(elfosComFlechas);
        elfosComFlechasDescTritaPorCentoNoturno = this.removerElfosNoturnos(i,elfosComFlechasDesc);
        
        return elfosComFlechasDescTritaPorCentoNoturno;
    }
    
    public ArrayList<Elfo> removerElfosNoturnos(int i, ArrayList<Elfo> atacantes){
        ArrayList<Elfo> elfosRemovidos = new ArrayList<>();
        int j = 0; int fim = atacantes.size()-1;
        while(j<=i && fim>0){
            if(atacantes.get(fim) instanceof ElfoNoturno){
                atacantes.remove(fim);
                j++;
                fim--;
            }
            else
                fim--;
        }
        return atacantes;
    }
    
    public int quantidadeElfosNoturnosNoExercito(ArrayList<Elfo> atacantes){
        int quantidadeTotal = 0;
        int quantidadePermitida = 0;
        
        for(Elfo elfoAtual : atacantes){
            if(elfoAtual instanceof ElfoNoturno)
                quantidadeTotal++;
        }
        quantidadePermitida=(int)(quantidadeTotal*0.3);
        return (quantidadeTotal-quantidadePermitida);
    }
    
    public ArrayList<Elfo> ordenarDescendente(ArrayList<Elfo> atacantes){
        int i = atacantes.size()-1;
        
        while (i>0){
            int last = 0;
            for(int j = 0; j < i; j++){
                if(atacantes.get(j).getQtdFlecha() < atacantes.get(j+1).getQtdFlecha()){
                    Elfo aux = atacantes.get(j);
                    atacantes.set(j, atacantes.get(j+1));
                    atacantes.set((j+1), aux);
                    last = j;
                }
            }
            i = last;
        }
        return atacantes;
    }
}
