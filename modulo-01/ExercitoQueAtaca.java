
public class ExercitoQueAtaca extends Exercito
{
    private Estrategia estrategia;

    public ExercitoQueAtaca(Estrategia estrategia){
        this.estrategia = estrategia;
    }

    public void trocarEstrategia (Estrategia estrategia){
        this.estrategia = estrategia; 
    }
}
