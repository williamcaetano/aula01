
import java.util.*;
import java.util.Collections;
import java.lang.*;
public class Elfo extends Personagem /*implements Comparable<Elfo>*/{
    protected int qtdExpPorAtaque;
    protected double qtdDano;
    private static int qtdElfos;
    
    {
        this.qtdExpPorAtaque = 1;
        this.qtdDano = 0;
    }
    
    public Elfo(String nome){
        super(nome);
        
        this.inventario.adicionaItem(new Item(4,"Flecha"));
        this.inventario.adicionaItem(new Item(1,"Arco"));
        Elfo.qtdElfos++;
        //super.ganharItem(new Item(4,"Flecha"));
        //super.ganharItem(new Item(1,"Arco")); 
        
        this.vida = 100.0;
    }
    /*
    public int compareTo(Object elfo){
        return this.getClass()
    }
    */
    
    public Item getFlecha(){ 
        return inventario.obter(0);
    }
    
    public int getQtdFlecha(){
        return this.getFlecha().getQuantidade();
    }
    
    public void incrementarFlecha(int quantidade){
        getFlecha().acrescentarQuantidade(quantidade);  
    } 
    
    public boolean podeAtirarFlecha(){
        return inventario.obter(0).getQuantidade() > 0;
    }
    
    public double calcularDano(){
        return this.qtdDano;
    }
    
    public void atirarFlecha(Dwarf dwarf){
     int qntAtual = this.getFlecha().getQuantidade();
     
     if(podeAtirarFlecha() && podeSofrerDano()){
         inventario.obter(0).setQuantidade(qntAtual - 1);
         this.incrementarExp();
         dwarf.decrementarVida();
     }
    }
    
    public void incrementarExp(){
        this.exp = this.exp + this.qtdExpPorAtaque;
    }
    
    public String imprimirResultado(){
        return "";
    }
    
    protected void finalize() throws Throwable{
        Elfo.qtdElfos--;
    }
    
    public static int getQtdElfos(){
        return Elfo.qtdElfos;
    }
}
