package br.com.dbccompany.vemserSpring.Entity;

public enum TipoPersonagem {
    ELFO,
    ELFO_VERDE,
    ELFO_NOTURNO,
    ELFO_DA_LUZ,
    DWARF,
    DWARF_BARBA_LONGA
}
