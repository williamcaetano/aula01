package br.com.dbccompany.vemserSpring.Controller;

import br.com.dbccompany.vemserSpring.Entity.InventarioXItem;
import br.com.dbccompany.vemserSpring.Service.InventarioXItemService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@Controller
@RequestMapping("/api/inventario_x_item")
public class InventarioXItemController {

    @Autowired
    InventarioXItemService service;

    @GetMapping(value = "/")
    @ResponseBody
    public List<InventarioXItem> todosInventarioXItem(){
        return service.todosInventarioXItem();
    }

    @PostMapping(value = "/novo")
    @ResponseBody
    public InventarioXItem novoInventarioXItem(@RequestBody InventarioXItem inventarioXItem){
        return service.salvar(inventarioXItem);
    }

    @PutMapping(value = "/editar/{id}")
    @ResponseBody
    public InventarioXItem editarInventarioXItem(@PathVariable Integer id, @RequestBody InventarioXItem inventarioXItem){
        return service.editar(id, inventarioXItem);
    }
}
