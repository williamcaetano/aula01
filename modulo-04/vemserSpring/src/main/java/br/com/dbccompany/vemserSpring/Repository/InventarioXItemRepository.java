package br.com.dbccompany.vemserSpring.Repository;

import br.com.dbccompany.vemserSpring.Entity.InventarioXItem;
import org.springframework.data.repository.CrudRepository;

public interface InventarioXItemRepository extends CrudRepository<InventarioXItem, Integer> {

}
