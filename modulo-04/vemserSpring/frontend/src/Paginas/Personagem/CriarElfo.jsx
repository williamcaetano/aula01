import React, {Component} from 'react';
import * as axios from 'axios';
import './elfo.css'

export default class CriarElfo extends Component{
    constructor(props){
        super(props)
        this.state = {
            nome: ''
        }        
    }

    trocaValoresState(evt){
        const  {name, value} = evt.target;
        this.setState({
            [name] : value,      
        })
    }

    novoElfo(evt){
        evt.preventDefault();
        const { nome } = this.state
        const header = {
            headers: {Authorization : localStorage.getItem('Authorization')}
        }
        if(nome){
            //executa regra de login
            axios.post('http://localhost:8080/api/elfo/novo',{
                    nome
            }, header).then(
                resp=>{
                    console.log(resp)
                    this.props.history.push('/elfos') 
                }) 
        }}

    render() {
        return (
            <React.Fragment>
                <h1>Crie seu Elfo: </h1>
                <input className="input" type='text' name = 'nome' id='nome' placeholder='Digite o nome do Elfo...' onBlur={ this.trocaValoresState.bind(this) } />
                <button className="button" type='button' onClick={this.novoElfo.bind(this)}> Criar </button>
            </React.Fragment>
        )   
    }
}