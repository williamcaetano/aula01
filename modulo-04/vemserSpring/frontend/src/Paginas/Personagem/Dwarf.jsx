import React, {Component} from 'react';
import * as axios from 'axios';
import { Link } from 'react-router-dom'
import dwarf from '../../img/dwarf.png'
import './elfo.css'

export default class Dwarf extends Component {
    constructor(props){
        super(props)
        this.state = {
            ListaDwarfs : [],
            ListaAlterada: []
        }        
    }
    componentDidMount() {
        const header = {
            headers: {Authorization : localStorage.getItem('Authorization')}
        }
        axios.get('http://localhost:8080/api/dwarf/', header).then(resp=>
            
            {   console.log(resp)
                const item = resp.data;
                this.setState({
                    ListaDwarfs : item,
                    ListaAlterada : item
                })
            })
    }

    render() {
        const {ListaDwarfs} = this.state
        return (
            <React.Fragment>
                <h1 className="titulo">Lista Dwarfs</h1>
                <Link className="button" to="/elfos"> Lista Elfos </Link>
                { ListaDwarfs.map((item, index) => { 
                    return(
                        <React.Fragment key={index}>
                            <div className="container">
                                <div className="item">
                                        <span className="paragrafo">ID: {item.id}</span><br/>
                                        <span className="paragrafo">Nome: {item.nome}</span><br/>
                                        <span className="paragrafo">Experiencia: {item.experiencia} </span><br/>
                                        <span className="paragrafo">Vida: {item.vida} </span><br/>
                                        <span className="paragrafo">Dano: {item.dano} </span><br/>
                                        <img src={dwarf} alt=''></img>
                                </div>
                            </div>
                        </React.Fragment> 
                    )
                }) }
            </React.Fragment>
        )   
    }
}