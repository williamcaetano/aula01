import React, {Component} from 'react';
import * as axios from 'axios';
import './login.css'

export default class Login extends Component {

    constructor(props){
        super(props)
        this.state={
            username : '',
            password : ''
        }
        this.trocaValoresState = this.trocaValoresState.bind(this)
    }

    trocaValoresState(evt){
        const  {name, value} = evt.target;
        this.setState({
            [name] : value,      
        })
    }

    logar(evt){
        evt.preventDefault();
        const { username, password } = this.state
        console.log(username, password)
        if(username  && password)
        {
            //executa regra de login
            axios.post('http://localhost:8080/login',{
                    username:   this.state.username,
                    password : this.state.password
            }).then(
                resp=>{
                    console.log(resp)
                    localStorage.setItem( 'Authorization' , resp.headers.authorization ) 
                    this.props.history.push('/elfos')     
                }  ) 
        }}
        

    render() {
        return (
        <div>
            <React.Fragment>
                <div className="container2">
                    <div className="containerLogin">
                        <h1>Faça seu Login:</h1>
                        <input className="inputLogin" type='text' name = 'username' id='username' placeholder='Digite o seu email...' onChange = { this.trocaValoresState.bind(this) } />
                        <br />
                        <input className="inputLogin" type='password' name = 'password' id='password' placeholder='Digite sua senha...' onChange = { this.trocaValoresState.bind(this) } />
                        <br />
                        <button className="buttonLogin" type='button' onClick={this.logar.bind(this)}> Logar </button>
                    </div>
                </div>
            </React.Fragment>
        </div>
        
        )   
        
    }
}