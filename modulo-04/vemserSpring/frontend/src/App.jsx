import React, {Component} from 'react';
import './App.css';
import Login from './Paginas/Login/Login'
import Elfo from './Paginas/Personagem/Elfo'
import Dwarf from './Paginas/Personagem/Dwarf'
import CriarElfo from './Paginas/Personagem/CriarElfo'
import { BrowserRouter as Router, Route } from 'react-router-dom'
import { PrivateRoute } from './Componentes/PrivateRoute'

export default class App extends Component{
  render() {
    return (
      <div className="App">
        <Router>
          <React.Fragment>
            <Route path="/login" component={ Login }/>
            <PrivateRoute path="/" exact component={Elfo}></PrivateRoute>
            <PrivateRoute  path="/elfos" exact component={Elfo}></PrivateRoute>
            <PrivateRoute  path="/dwarfs" exact component={Dwarf}></PrivateRoute>
            <PrivateRoute  path="/criaElfo" exact component={CriarElfo}></PrivateRoute>
          </React.Fragment>
        </Router>
      </div>
    );
  }
}


