package br.com.dbccompany.projetofinalSpring;

import br.com.dbccompany.projetofinalSpring.Entity.Clientes;
import br.com.dbccompany.projetofinalSpring.Entity.Contato;
import br.com.dbccompany.projetofinalSpring.Entity.TipoContato;
import br.com.dbccompany.projetofinalSpring.Service.ClientesService;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit4.SpringRunner;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.ArrayList;
import java.util.List;

@RunWith(SpringRunner.class)
@DataJpaTest
public class ClientesTest extends ProjetofinalSpringApplicationTests{
    @Autowired
    private ClientesService clientesService;

    @Autowired
    private TestEntityManager entityManager;

    @Test
    public void realizarTestes() {
        Clientes cliente = new Clientes();
        cliente.setNome("William");
        cliente.setCpf("02273538030");
        Clientes cliente2 = new Clientes();
        cliente.setNome("João");
        cliente.setCpf("02273538030");
        TipoContato tipoContato = new TipoContato();
        tipoContato.setNome("Email");
        TipoContato tipoContato2 = new TipoContato();
        tipoContato2.setNome("Telefone");
        Contato contato = new Contato();
        contato.setTipoContato(tipoContato);
        contato.setValor("abc@email.com");
        contato.setCliente(cliente);
        Contato contato2 = new Contato();
        contato2.setTipoContato(tipoContato2);
        contato2.setValor("51 991234567");
        contato2.setCliente(cliente);
        List<Contato> contatos = new ArrayList<>();
        contatos.add(contato);
        contatos.add(contato2);
        cliente.setContatos(contatos);
        entityManager.persist(cliente);
        entityManager.persist(cliente2);
        entityManager.flush();

        try {
            clientesService.salvar(cliente);
            clientesService.salvar(cliente2);
        } catch (Exception e) {
            assertThat(e.getMessage()).isEqualTo("CPF ja cadastrado.");
        }
    }
}
