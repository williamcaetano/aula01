package br.com.dbccompany.projetofinalSpring.Repository;

import br.com.dbccompany.projetofinalSpring.Controller.UsuarioController;
import br.com.dbccompany.projetofinalSpring.ProjetofinalSpringApplicationTests;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

public class UsuarioIntegrationTest extends ProjetofinalSpringApplicationTests {

    private MockMvc mock;

    @Autowired
    private UsuarioController controller;

    @Before
    public void setUp(){
        this.mock = MockMvcBuilders.standaloneSetup(controller).build();
    }

    @Test
    public void testeGetApiStatusOK() throws Exception{
        this.mock.perform(MockMvcRequestBuilders.get("/api/usuario/")).andExpect(MockMvcResultMatchers.status().isOk());
    }
}
