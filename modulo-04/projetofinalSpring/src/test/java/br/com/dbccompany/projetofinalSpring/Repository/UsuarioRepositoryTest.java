package br.com.dbccompany.projetofinalSpring.Repository;

import br.com.dbccompany.projetofinalSpring.Entity.Usuario;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit4.SpringRunner;
import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
@DataJpaTest
public class UsuarioRepositoryTest {

    @Autowired
    private TestEntityManager entityManager;

    @Autowired
    private UsuarioRepository repository;

    @Test
    public void procurarUsuariosPorNome(){

        Usuario usuario = new Usuario();
        usuario.setNome("William");
        entityManager.persist(usuario);
        entityManager.flush();

        Usuario resposta = repository.findByNome(usuario.getNome());

        assertThat(resposta.getNome()).isEqualTo(usuario.getNome());

    }
}
