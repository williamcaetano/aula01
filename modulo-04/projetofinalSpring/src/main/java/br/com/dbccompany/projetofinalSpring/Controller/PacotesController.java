package br.com.dbccompany.projetofinalSpring.Controller;

import br.com.dbccompany.projetofinalSpring.Entity.Pacotes;
import br.com.dbccompany.projetofinalSpring.Service.PacotesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping("/api/pacotes")
public class PacotesController {

    @Autowired
    PacotesService service;

    @GetMapping(value = "/")
    @ResponseBody
    public List<Pacotes> todosPacotes(){
        return service.todosPacotes();
    }

    @PostMapping(value = "/novo")
    @ResponseBody
    public Pacotes novoPacote(@RequestBody Pacotes pacote) {
        return service.salvar(pacote);
    }

    @PutMapping(value = "/editar/{id}")
    @ResponseBody
    public Pacotes editarPacotes(@PathVariable Integer id, @RequestBody Pacotes pacote) {
        return service.editar(id, pacote);
    }
}
