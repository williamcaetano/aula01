package br.com.dbccompany.projetofinalSpring.Entity;

import org.hibernate.validator.constraints.Email;

import javax.persistence.*;
import javax.validation.constraints.Pattern;

@Entity
public class Usuario {
    @Id
    @Column(name = "ID_USUARIO")
    @SequenceGenerator( allocationSize = 1, name = "USUARIO_SEQ", sequenceName = "USUARIO_SEQ")
    @GeneratedValue(generator = "USUARIO_SEQ", strategy = GenerationType.SEQUENCE)
    private Integer id;
    @Column(name = "NOME", nullable = false)
    private String nome;
    @Email(message = "Digite um email valido.")
    @Column(name = "EMAIL", unique = true, nullable = false)
    private String email;
    @Column(name = "LOGIN", unique = true, nullable = false)
    private String login;
    @Pattern(regexp = "^[A-Za-z0-9]+$", message= "A senha deve conter somente letras e numeros")
    @Column(name = "SENHA", nullable = false)
    private String senha;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getSenha() {
        return senha;
    }

    public void setSenha(String senha) {
        this.senha = senha;
    }
}
