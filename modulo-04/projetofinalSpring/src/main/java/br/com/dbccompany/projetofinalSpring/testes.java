package br.com.dbccompany.projetofinalSpring;


import java.text.ParseException;

public class testes {
    public static void main(String[] args) {

        //TESTAR CRIPTOGRAFIA MD5
        String minhaSenha = "teste123";
        String resultado;

        CriptografiaMD5 teste = new CriptografiaMD5();
        resultado = teste.criptografar(minhaSenha);
        System.out.println(resultado);

        //TESTAR CONVERSAO DE VALOR
        String valorMoeda = "R$ 450,00";
        Double valor = 450.50;

        Double resultado1;
        String resultado2;

        ConversaoMoeda converter = new ConversaoMoeda();
        resultado2 = converter.valorParaMoeda(valor);
        System.out.println(resultado2);

        try{
            resultado1 = converter.moedaParaValor(valorMoeda);
            System.out.println(resultado1);
        }catch (ParseException ex){
            System.out.println("FALHOU");
        }

    }
}
