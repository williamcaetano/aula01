package br.com.dbccompany.projetofinalSpring.Controller;

import br.com.dbccompany.projetofinalSpring.Entity.Acessos;
import br.com.dbccompany.projetofinalSpring.Service.AcessosService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping("/api/acessos")
public class AcessosController {

    @Autowired
    AcessosService service;

    @GetMapping(value = "/")
    @ResponseBody
    public List<Acessos> todosAcessos(){
        return service.todosAcessos();
    }

    @PostMapping(value = "/novo")
    @ResponseBody
    public String novoAcesso(@RequestBody Acessos acesso) {
        return service.salvar(acesso);
    }

    @PutMapping(value = "/editar/{id}")
    @ResponseBody
    public Acessos editarAcesso(@PathVariable Integer id, @RequestBody Acessos acesso) {
        return service.editar(id, acesso);
    }
}
