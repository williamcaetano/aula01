package br.com.dbccompany.projetofinalSpring.Service;

import br.com.dbccompany.projetofinalSpring.Entity.*;
import br.com.dbccompany.projetofinalSpring.Repository.PagamentosRepository;
import br.com.dbccompany.projetofinalSpring.Repository.SaldoClienteRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Calendar;
import java.util.Date;
import java.util.List;


@Service
public class PagamentosService {

    @Autowired
    private PagamentosRepository pagamentosRepository;

    @Autowired
    private SaldoClienteService saldoClienteService;

    @Autowired
    private ContratacaoService contratacaoService;

    @Autowired
    private ClientesPacotesService clientesPacotesService;

    @Transactional( rollbackFor = Exception.class)
    public Pagamentos salvar(Pagamentos pagamento){
        return efetuarPagamento(pagamento);
    }

    @Transactional( rollbackFor = Exception.class)
    public Pagamentos editar( Integer id, Pagamentos pagamentos){
        pagamentos.setId(id);
        return pagamentosRepository.save(pagamentos);
    }

    public Pagamentos getPagamentos(Integer id){
        return pagamentosRepository.findById(id).get();
    }

    public List<Pagamentos> todosPagamentos(){
        return (List<Pagamentos>) pagamentosRepository.findAll();
    }

    public Pagamentos efetuarPagamento(Pagamentos pagamento){
        ClientesPacotes clientesPacotes = clientesPacotesService.getClientesPacotes(pagamento.getClientePacote().getId());
        Contratacao contratacao = contratacaoService.getContratacao(pagamento.getContratacao().getId());
        Date dataVencimento;

        if(clientesPacotes.getId() != null){

            Clientes cliente = clientesPacotes.getCliente();
            List<EspacosPacotes> espPacotes = clientesPacotes.getPacote().getEspacosPacotes();

            for(EspacosPacotes espacoPacote : espPacotes){
                SaldoClienteID id = new SaldoClienteID(cliente.getId(), espacoPacote.getEspaco().getId());
                SaldoCliente saldo = new SaldoCliente();

                saldo.setId(id);
                saldo.setQuantidade(espacoPacote.getQuantidade()*clientesPacotes.getQuantidade());
                saldo.setTipoContratacao(espacoPacote.getTipoContratacao());
                //DEFINIR VENCIMENTO
                if(saldo.getVencimento() == null){
                    dataVencimento = new Date(System.currentTimeMillis());
                }
                else {
                    dataVencimento = saldo.getVencimento();
                }
                saldo.setVencimento(calcularVencimento(dataVencimento, espacoPacote.getQuantidade(), espacoPacote.getPrazoDias() ,espacoPacote.getTipoContratacao()));
                saldoClienteService.salvar(saldo);
            }
        }
        else if (contratacao.getId() != null){
            Clientes cliente = contratacao.getCliente();
            Espacos espaco = contratacao.getEspaco();

            SaldoClienteID id = new SaldoClienteID(cliente.getId(), espaco.getId());
            SaldoCliente saldo = new SaldoCliente();

            saldo.setId(id);
            saldo.setQuantidade(contratacao.getQuantidade());
            saldo.setTipoContratacao(contratacao.getTipoContratacao());
            if(saldo.getVencimento() == null){
                dataVencimento = new Date(System.currentTimeMillis());
            }
            else {
                dataVencimento = saldo.getVencimento();
            }
            saldo.setVencimento(calcularVencimento(dataVencimento, contratacao.getQuantidade(),contratacao.getPrazoDias() ,contratacao.getTipoContratacao()));
            saldoClienteService.salvar(saldo);
        }
        return pagamentosRepository.save(pagamento);
    }

    public Date calcularVencimento(Date data, Integer quantidade ,Integer prazo, TipoContratacao tipoContrato){
        TipoContratacao tipo = tipoContrato;
        Calendar calendario = Calendar.getInstance();
        calendario.setTime(data);

        switch (tipo){
            case MES: calendario.add(Calendar.MONTH, quantidade+prazo);
                break;
            case SEMANA: calendario.add(Calendar.WEEK_OF_YEAR, quantidade+prazo);
                break;
            case DIARIA: calendario.add(Calendar.DAY_OF_YEAR, quantidade+prazo);
                break;
            case TURNO: calendario.add(Calendar.HOUR, (quantidade* 5)+prazo);
                break;
            case HORA: calendario.add(Calendar.HOUR, quantidade+prazo);
                break;
            case MINUTO: calendario.add(Calendar.MINUTE, quantidade+prazo);
                break;
        }
        return calendario.getTime();
    }
}
