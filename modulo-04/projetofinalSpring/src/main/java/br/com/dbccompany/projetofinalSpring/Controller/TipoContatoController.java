package br.com.dbccompany.projetofinalSpring.Controller;

import br.com.dbccompany.projetofinalSpring.Entity.TipoContato;
import br.com.dbccompany.projetofinalSpring.Service.TipoContatoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping("/api/tipo_contato")
public class TipoContatoController {

    @Autowired
    TipoContatoService service;

    @GetMapping(value = "/")
    @ResponseBody
    public List<TipoContato> todosTiposContatos(){
        return service.todosTipoContatos();
    }

    @PostMapping(value = "/novo")
    @ResponseBody
    public TipoContato novoTipoContato(@RequestBody TipoContato tipoContato) {
        return service.salvar(tipoContato);
    }

    @PutMapping(value = "/editar/{id}")
    @ResponseBody
    public TipoContato editarTipoContato(@PathVariable Integer id, @RequestBody TipoContato tipoContato) {
        return service.editar(id, tipoContato);
    }
}

