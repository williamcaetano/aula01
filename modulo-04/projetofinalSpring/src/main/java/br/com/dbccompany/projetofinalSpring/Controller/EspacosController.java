package br.com.dbccompany.projetofinalSpring.Controller;

import br.com.dbccompany.projetofinalSpring.Entity.Espacos;
import br.com.dbccompany.projetofinalSpring.Service.EspacosService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping( "/api/espacos" )
public class EspacosController {

    @Autowired
    EspacosService service;

    @GetMapping(value = "/")
    @ResponseBody
    public List<Espacos> todosEspacos(){
        return service.todosEspacos();
    }

    @PostMapping(value = "/novo")
    @ResponseBody
    public Espacos novoEspacos(@RequestBody Espacos espaco) {
        return service.salvar(espaco);
    }

    @PutMapping(value = "/editar/{id}")
    @ResponseBody
    public Espacos editarEspacos(@PathVariable Integer id, @RequestBody Espacos espaco) {
        return service.editar(id, espaco);
    }
}
