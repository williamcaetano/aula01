package br.com.dbccompany.projetofinalSpring;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ProjetofinalSpringApplication {

	public static void main(String[] args) {
		SpringApplication.run(ProjetofinalSpringApplication.class, args);
	}

}
