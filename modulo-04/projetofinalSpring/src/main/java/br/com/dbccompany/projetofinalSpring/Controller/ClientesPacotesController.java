package br.com.dbccompany.projetofinalSpring.Controller;

import br.com.dbccompany.projetofinalSpring.Entity.ClientesPacotes;
import br.com.dbccompany.projetofinalSpring.Service.ClientesPacotesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping("/api/clientesPacotes")
public class ClientesPacotesController {

    @Autowired
    ClientesPacotesService service;

    @GetMapping(value = "/")
    @ResponseBody
    public List<ClientesPacotes> todosClientesPacotes(){
        return service.todosClientesPacotes();
    }

    @PostMapping( value = "/novo")
    @ResponseBody
    public ClientesPacotes novoClientesPacotes(@RequestBody ClientesPacotes clientesPacotes) {
        return service.salvar(clientesPacotes);
    }

    @PutMapping(value = "/editar/{id}")
    @ResponseBody
    public ClientesPacotes editarClientesPacotes(@PathVariable Integer id, @RequestBody ClientesPacotes clientesPacotes) {
        return service.editar(id, clientesPacotes);
    }
}
