package br.com.dbccompany.projetofinalSpring.Repository;

import br.com.dbccompany.projetofinalSpring.Entity.Usuario;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UsuarioRepository extends CrudRepository<Usuario, Integer> {
    Usuario findByNome(String nome);
}
