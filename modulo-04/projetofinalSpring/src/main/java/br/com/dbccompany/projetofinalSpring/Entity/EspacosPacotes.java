package br.com.dbccompany.projetofinalSpring.Entity;

import javax.persistence.*;

@Entity
public class EspacosPacotes {
    @Id
    @Column(name = "ID_ESPACO_PACOTE")
    @SequenceGenerator(allocationSize = 1, name = "ESPACO_PACOTE_SEQ", sequenceName = "ESPACO_PACOTE_SEQ")
    @GeneratedValue(generator = "ESPACO_PACOTE_SEQ", strategy = GenerationType.SEQUENCE)
    private Integer id;
    @Column(name = "QUANTIDADE", nullable = false)
    private Integer quantidade;
    @Column(name = "PRAZO_DIAS", nullable = false)
    private Integer prazoDias;

    @ManyToOne
    @JoinColumn(name = "id_espaco")
    private Espacos espaco;

    public Espacos getEspaco() {
        return espaco;
    }

    public void setEspaco(Espacos espaco) {
        this.espaco = espaco;
    }

    public Pacotes getPacote() {
        return pacote;
    }

    public void setPacote(Pacotes pacote) {
        this.pacote = pacote;
    }

    @ManyToOne
    @JoinColumn(name = "id_pacote")
    private Pacotes pacote;

    @Enumerated(EnumType.STRING)
    private TipoContratacao tipoContratacao;

    public TipoContratacao getTipoContratacao() {
        return tipoContratacao;
    }

    public void setTipoContratacao(TipoContratacao tipoContratacao) {
        this.tipoContratacao = tipoContratacao;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getQuantidade() {
        return quantidade;
    }

    public void setQuantidade(Integer quantidade) {
        this.quantidade = quantidade;
    }

    public Integer getPrazoDias() {
        return prazoDias;
    }

    public void setPrazoDias(Integer prazoDias) {
        this.prazoDias = prazoDias;
    }
}
