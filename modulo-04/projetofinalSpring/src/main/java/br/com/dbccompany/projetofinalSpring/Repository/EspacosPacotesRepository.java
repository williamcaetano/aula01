package br.com.dbccompany.projetofinalSpring.Repository;

import br.com.dbccompany.projetofinalSpring.Entity.EspacosPacotes;
import br.com.dbccompany.projetofinalSpring.Entity.Pacotes;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface EspacosPacotesRepository extends CrudRepository<EspacosPacotes, Integer> {
    List<EspacosPacotes> buscarTodosPorPacote(Pacotes pacote);
}
