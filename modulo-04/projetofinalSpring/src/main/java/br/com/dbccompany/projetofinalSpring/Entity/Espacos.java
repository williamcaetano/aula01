package br.com.dbccompany.projetofinalSpring.Entity;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
public class Espacos {
    @Id
    @Column(name = "ID_ESPACO")
    @SequenceGenerator(allocationSize = 1, name = "ESPACO_SEQ", sequenceName = "ESPACO_SEQ")
    @GeneratedValue(generator = "ESPACO_SEQ", strategy = GenerationType.SEQUENCE)
    private Integer id;
    @Column(name = "NOME", nullable = false, unique = true)
    private String nome;
    @Column(name = "QTD_PESSOAS", unique = true, nullable = false)
    private Integer qtdPessoas;
    @Column(name = "VALOR", unique = true, nullable = false)
    private Double valor;

    @OneToMany(mappedBy = "espaco")
    private List<EspacosPacotes> espacosPacotes = new ArrayList<>();

    @OneToMany(mappedBy = "espaco")
    private List<SaldoCliente> saldosClientes = new ArrayList<>();

    @OneToMany(mappedBy = "espaco")
    private List<Contratacao> contratacoes = new ArrayList<>();

    public List<EspacosPacotes> getEspacosPacotes() {
        return espacosPacotes;
    }

    public void setEspacosPacotes(List<EspacosPacotes> espacosPacotes) {
        this.espacosPacotes = espacosPacotes;
    }

    public List<SaldoCliente> getSaldosClientes() {
        return saldosClientes;
    }

    public void setSaldosClientes(List<SaldoCliente> saldosClientes) {
        this.saldosClientes = saldosClientes;
    }

    public List<Contratacao> getContratacoes() {
        return contratacoes;
    }

    public void setContratacoes(List<Contratacao> contratacoes) {
        this.contratacoes = contratacoes;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public Integer getQtdPessoas() {
        return qtdPessoas;
    }

    public void setQtdPessoas(Integer qtdPessoas) {
        this.qtdPessoas = qtdPessoas;
    }

    public Double getValor() {
        return valor;
    }

    public void setValor(Double valor) {
        this.valor = valor;
    }
}
