package br.com.dbccompany.projetofinalSpring.Service;

import br.com.dbccompany.projetofinalSpring.Entity.ClientesPacotes;
import br.com.dbccompany.projetofinalSpring.Repository.ClientesPacotesRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;


@Service
public class ClientesPacotesService {

    @Autowired
    private ClientesPacotesRepository clientesPacotesRepository;

    @Transactional( rollbackFor = Exception.class)
    public ClientesPacotes salvar(ClientesPacotes clientesPacotes){
        return clientesPacotesRepository.save(clientesPacotes);
    }

    @Transactional( rollbackFor = Exception.class)
    public ClientesPacotes editar( Integer id, ClientesPacotes clientesPacotes){
        clientesPacotes.setId(id);
        return clientesPacotesRepository.save(clientesPacotes);

    }

    public ClientesPacotes getClientesPacotes(Integer id){
        return clientesPacotesRepository.findById(id).get();
    }

    public List<ClientesPacotes> todosClientesPacotes(){
        return (List<ClientesPacotes>) clientesPacotesRepository.findAll();
    }



}
