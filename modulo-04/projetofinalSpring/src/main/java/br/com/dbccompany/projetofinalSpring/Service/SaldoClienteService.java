package br.com.dbccompany.projetofinalSpring.Service;

import br.com.dbccompany.projetofinalSpring.Entity.SaldoCliente;
import br.com.dbccompany.projetofinalSpring.Entity.SaldoClienteID;
import br.com.dbccompany.projetofinalSpring.Repository.SaldoClienteRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;


@Service
public class SaldoClienteService {

    @Autowired
    private SaldoClienteRepository saldoClienteRepository;

    @Transactional( rollbackFor = Exception.class)
    public SaldoCliente salvar(SaldoCliente saldoCliente){
        return saldoClienteRepository.save(saldoCliente);
    }

    @Transactional( rollbackFor = Exception.class)
    public SaldoCliente editar(SaldoClienteID id, SaldoCliente saldoCliente){
        return saldoClienteRepository.save(saldoCliente);
    }

    public SaldoCliente getSaldoCliente(Integer id){
        return saldoClienteRepository.findById(id).get();
    }

    public List<SaldoCliente> todosSaldoClientes(){
        return (List<SaldoCliente>) saldoClienteRepository.findAll();
    }
}
