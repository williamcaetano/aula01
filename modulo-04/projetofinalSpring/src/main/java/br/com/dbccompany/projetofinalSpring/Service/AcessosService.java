package br.com.dbccompany.projetofinalSpring.Service;


import br.com.dbccompany.projetofinalSpring.Entity.Acessos;
import br.com.dbccompany.projetofinalSpring.Entity.Clientes;
import br.com.dbccompany.projetofinalSpring.Entity.Espacos;
import br.com.dbccompany.projetofinalSpring.Entity.SaldoCliente;
import br.com.dbccompany.projetofinalSpring.Repository.AcessosRepository;
import org.springframework.stereotype.Service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;
import java.time.ZoneId;
import java.time.temporal.ChronoUnit;
import java.util.Date;
import java.util.List;


@Service
public class AcessosService {

    @Autowired
    private AcessosRepository acessosRepository;

    @Autowired
    private SaldoClienteService saldoClienteService;

    @Autowired
    private ClientesService clientesService;

    @Autowired
    private EspacosService espacosService;

    @Transactional(rollbackFor = Exception.class)
    public String salvar(Acessos acessos) {
        return realizarAcesso(acessos);
    }

    @Transactional(rollbackFor = Exception.class)
    public Acessos editar(Integer id, Acessos acessos) {
        acessos.setId(id);
        return acessosRepository.save(acessos);

    }

    public Acessos getAcessos(Integer id) {
        return acessosRepository.findById(id).get();
    }

    public List<Acessos> todosAcessos() {
        return (List<Acessos>) acessosRepository.findAll();
    }


    public String realizarAcesso(Acessos acessos) {
        Integer clienteId = acessos.getSaldoCliente().getId().getIdCliente();
        Integer espacoId = acessos.getSaldoCliente().getId().getIdEspaco();
        Clientes cliente = clientesService.getClientes(clienteId);
        Espacos espaco = espacosService.getEspacos(espacoId);
        List<Acessos> listaAcessos = acessosRepository.findAllBySaldoCliente_idClienteAndSaldoCliente_idEspaco(cliente, espaco);
        Acessos ultimoAcesso = new Acessos();

        if(listaAcessos.size() > 0) {
            ultimoAcesso = listaAcessos.get(listaAcessos.size() - 1);
        }

        if (acessos.getEntrada()) {
            //REGRA DE ENTRADA
            if (acessos.getData() == null) {
                acessos.setData(new Date(System.currentTimeMillis()));
            }
            if (acessos.getSaldoCliente().getQuantidade() > 0 &&
                    acessos.getData().compareTo(acessos.getSaldoCliente().getVencimento()) != 1) {
                SaldoCliente saldoCliente = acessos.getSaldoCliente();
                saldoCliente.getAcessos().add(acessos);
                saldoClienteService.editar(saldoCliente.getId(), acessos.getSaldoCliente());
                return String.format("Realizou o acesso, bem vindo!");
            } else if (acessos.getSaldoCliente().getQuantidade() == 0) {
                return String.format("Saldo insuficiente");
            } else
                acessos.getSaldoCliente().setQuantidade(0);
            return String.format("Saldo Vencido");
        } else { //REGRA DE SAIDA
            LocalDate dataEntrada = ultimoAcesso.getData().toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
            LocalDate dataSaida = acessos.getData().toInstant().atZone(ZoneId.systemDefault()).toLocalDate();

            SaldoCliente saldoAtualizado = new SaldoCliente();
            saldoAtualizado.setId(acessos.getSaldoCliente().getId());
            saldoAtualizado.setTipoContratacao(acessos.getSaldoCliente().getTipoContratacao());
            saldoAtualizado.setVencimento(acessos.getSaldoCliente().getVencimento());

            long diferencaEmDias = ChronoUnit.DAYS.between(dataSaida, dataEntrada);
            saldoAtualizado.setQuantidade(this.descontarSaldo(diferencaEmDias, acessos));

            saldoClienteService.salvar(saldoAtualizado);
            acessosRepository.save(acessos);
        }
        return "O saldo foi atualizado, volte sempre.";
    }

    public Integer descontarSaldo(Long duracaoNoEspaco, Acessos acesso) {
        Integer novaQuantidade = 0;
        Integer duracao = duracaoNoEspaco.intValue(); //dias
        switch (acesso.getSaldoCliente().getTipoContratacao()) {
            case MINUTO:
                novaQuantidade = acesso.getSaldoCliente().getQuantidade() - (duracao*1440);
                break;
            case HORA:
                novaQuantidade = acesso.getSaldoCliente().getQuantidade() - (duracao*24);
                break;
            case TURNO:
                novaQuantidade = acesso.getSaldoCliente().getQuantidade() - (duracao*24*5);
                break;
            case DIARIA:
                novaQuantidade = acesso.getSaldoCliente().getQuantidade() - duracao;
                break;
            case SEMANA:
                novaQuantidade = acesso.getSaldoCliente().getQuantidade() - (duracao/7);
                break;
            case MES:
                novaQuantidade = acesso.getSaldoCliente().getQuantidade() - (duracao/30);
                break;
        }
        return novaQuantidade;
    }


}