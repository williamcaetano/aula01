package br.com.dbccompany.projetofinalSpring.Repository;

import br.com.dbccompany.projetofinalSpring.Entity.Acessos;
import br.com.dbccompany.projetofinalSpring.Entity.Clientes;
import br.com.dbccompany.projetofinalSpring.Entity.Espacos;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface AcessosRepository extends CrudRepository<Acessos, Integer> {
    List<Acessos> findAllBySaldoCliente_idClienteAndSaldoCliente_idEspaco(Clientes cliente, Espacos espaco);
}
