package br.com.dbccompany.projetofinalSpring.Entity;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;

@Embeddable
public class SaldoClienteID implements Serializable {

    @Column(name = "ID_SALDO")
    private Integer idEspaco;

    @Column(name = "ID_CLIENTE")
    private Integer idCliente;

    public SaldoClienteID() {
    }

    public SaldoClienteID(Integer idEspaco, Integer idCliente) {
        this.idEspaco = idEspaco;
        this.idCliente = idCliente;
    }

    public Integer getIdEspaco() {
        return idEspaco;
    }

    public void setIdEspaco(Integer idEspaco) {
        this.idEspaco = idEspaco;
    }

    public Integer getIdCliente() {
        return idCliente;
    }

    public void setIdCliente(Integer idCliente) {
        this.idCliente = idCliente;
    }
}