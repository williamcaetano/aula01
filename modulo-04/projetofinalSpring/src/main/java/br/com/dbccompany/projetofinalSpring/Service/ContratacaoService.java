package br.com.dbccompany.projetofinalSpring.Service;

import br.com.dbccompany.projetofinalSpring.ConversaoMoeda;
import br.com.dbccompany.projetofinalSpring.Entity.Contratacao;
import br.com.dbccompany.projetofinalSpring.Repository.ContratacaoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;


@Service
public class ContratacaoService {

    @Autowired
    private ContratacaoRepository contratacaoRepository;


    @Transactional( rollbackFor = Exception.class)
    public String salvar(Contratacao contratacao){
        contratacaoRepository.save(contratacao);
        String valor = this.calcularCustoContrato(contratacao);
        return valor;
    }

    @Transactional( rollbackFor = Exception.class)
    public Contratacao editar( Integer id, Contratacao contratacao){
        contratacao.setId(id);
        return contratacaoRepository.save(contratacao);

    }

    public Contratacao getContratacao(Integer id){
        return contratacaoRepository.findById(id).get();
    }

    public List<Contratacao> todasContratacoes(){
        return (List<Contratacao>) contratacaoRepository.findAll();
    }

    public String calcularCustoContrato(Contratacao contrato){
        ConversaoMoeda converter = new ConversaoMoeda();
        Double diariaEspaco = contrato.getEspaco().getValor();
        Double valorPorTipoContrato = 0.0;

        switch (contrato.getTipoContratacao()){
        case MES: valorPorTipoContrato = diariaEspaco * 30;
            break;
        case SEMANA: valorPorTipoContrato = diariaEspaco * 7;
            break;
        case DIARIA: valorPorTipoContrato = diariaEspaco;
            break;
        case TURNO: valorPorTipoContrato = diariaEspaco * 5 / 24;
            break;
        case HORA: valorPorTipoContrato = diariaEspaco / 24;
            break;
        case MINUTO: valorPorTipoContrato = diariaEspaco / 1440;
            break;
        }
        Double custoTotal = (valorPorTipoContrato * contrato.getQuantidade()) - contrato.getDesconto();

        return converter.valorParaMoeda(custoTotal);
    }
}


