package br.com.dbccompany.projetofinalSpring.Service;

import br.com.dbccompany.projetofinalSpring.Entity.Clientes;
import br.com.dbccompany.projetofinalSpring.Entity.Contato;
import br.com.dbccompany.projetofinalSpring.Repository.ClientesRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;


@Service
public class ClientesService {

    @Autowired
    private ClientesRepository clientesRepository;

    @Transactional(rollbackFor = Exception.class)
    public Clientes salvar(Clientes clientes) throws Exception{
        if(clientesRepository.findByCpf(clientes.getCpf()) != null){
            throw new Exception("CPF ja cadastrado.");
        }

        boolean encontrou = this.encontrarTipo(clientes.getContatos());
        if(encontrou){
            return clientesRepository.save(clientes);
        }
        throw new Exception("O cliente deve ter um contato do tipo Email e um do tipo Telefone");
    }

    @Transactional( rollbackFor = Exception.class)
    public Clientes editar( Integer id, Clientes clientes){
        clientes.setId(id);
        return clientesRepository.save(clientes);
    }

    public Clientes getClientes(Integer id){
        return clientesRepository.findById(id).get();
    }

    public List<Clientes> todosClientes(){
        return (List<Clientes>) clientesRepository.findAll();
    }

    public boolean encontrarTipo(List<Contato> lista){
        for (Contato c : lista) {
            if (c.getTipoContato().getNome().equalsIgnoreCase("Email")) {
                for (Contato c2 : lista){
                    if (c2.getTipoContato().getNome().equalsIgnoreCase("Telefone")){
                        return true;
                    }
                }
            }
        }
        return false;
    }

}


