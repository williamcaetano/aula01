package br.com.dbccompany.projetofinalSpring;

import java.math.BigInteger;
import java.security.MessageDigest;

public class CriptografiaMD5 {

    public String criptografar(String senha){
        String senhaCripografada;

        if(senha == null){
            return null;
        }
        try {
            MessageDigest m = MessageDigest.getInstance("MD5");
            m.update(senha.getBytes(), 0, senha.length());
            senhaCripografada = new BigInteger(1, m.digest()).toString(16);
        } catch (Exception e) {
            System.out.println("Erro na criptografia");
            return null;
        }
        return senhaCripografada;
    }

}
