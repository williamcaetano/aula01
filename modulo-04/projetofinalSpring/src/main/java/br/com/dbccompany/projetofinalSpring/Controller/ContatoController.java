package br.com.dbccompany.projetofinalSpring.Controller;

import br.com.dbccompany.projetofinalSpring.Entity.Contato;
import br.com.dbccompany.projetofinalSpring.Service.ContatoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping("/api/contatos")
public class ContatoController {

    @Autowired
    ContatoService service;

    @GetMapping(value = "/")
    @ResponseBody
    public List<Contato> todosContatos(){
        return service.todosContatos();
    }

    @PostMapping(value = "/novo")
    @ResponseBody

    public Contato novoContato(@RequestBody Contato contato) {
        return service.salvar(contato);
    }

    @PutMapping(value = "/editar/{id}")
    @ResponseBody
    public Contato editarContato(@PathVariable Integer id, @RequestBody Contato contato) {
        return service.editar(id, contato);
    }
}
