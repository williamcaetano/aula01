package br.com.dbccompany.projetofinalSpring.Repository;

import br.com.dbccompany.projetofinalSpring.Entity.Contratacao;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ContratacaoRepository extends CrudRepository<Contratacao, Integer> {
}
