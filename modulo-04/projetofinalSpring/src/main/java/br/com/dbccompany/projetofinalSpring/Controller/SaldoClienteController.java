package br.com.dbccompany.projetofinalSpring.Controller;

import br.com.dbccompany.projetofinalSpring.Entity.SaldoCliente;
import br.com.dbccompany.projetofinalSpring.Service.SaldoClienteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping("/api/saldo_cliente")
public class SaldoClienteController {

    @Autowired
    SaldoClienteService service;

    @GetMapping(value = "/")
    @ResponseBody
    public List<SaldoCliente> todosSaldosClientes(){
        return service.todosSaldoClientes();
    }

    @PostMapping(value = "/novo")
    @ResponseBody
    public SaldoCliente novoSaldoCliente(@RequestBody SaldoCliente saldoCliente) {
        return service.salvar(saldoCliente);
    }

    /*
    @PutMapping(value = "/editar/{id}")
    @ResponseBody
    public SaldoCliente editarSaldoCliente(@PathVariable Integer id, @RequestBody SaldoCliente saldoCliente) {
        return service.editar(id, saldoCliente);
    }
    */

}
