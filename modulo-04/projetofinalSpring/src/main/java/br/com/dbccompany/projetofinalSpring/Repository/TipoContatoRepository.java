package br.com.dbccompany.projetofinalSpring.Repository;

import br.com.dbccompany.projetofinalSpring.Entity.TipoContato;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TipoContatoRepository extends CrudRepository<TipoContato, Integer> {
}
