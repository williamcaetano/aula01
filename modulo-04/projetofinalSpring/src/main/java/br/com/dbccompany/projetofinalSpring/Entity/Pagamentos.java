package br.com.dbccompany.projetofinalSpring.Entity;

import javax.persistence.*;

@Entity
public class Pagamentos {
    @Id
    @Column(name = "ID_PAGAMENTO")
    @SequenceGenerator(allocationSize = 1, name = "PAGAMENTO_SEQ", sequenceName = "PAGAMENTO_SEQ")
    @GeneratedValue(generator = "PAGAMENTO_SEQ", strategy = GenerationType.SEQUENCE)
    private Integer id;

    @Enumerated(EnumType.STRING)
    private TipoPagamento tipoPagamento;

    @ManyToOne
    @JoinColumn(name = "id_cliente_pacote")
    private ClientesPacotes clientePacote;

    @ManyToOne
    @JoinColumn(name = "id_contratacao")
    private Contratacao contratacao;

    public ClientesPacotes getClientePacote() {
        return clientePacote;
    }

    public void setClientePacote(ClientesPacotes clientePacote) {
        this.clientePacote = clientePacote;
    }

    public Contratacao getContratacao() {
        return contratacao;
    }

    public void setContratacao(Contratacao contratacao) {
        this.contratacao = contratacao;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public TipoPagamento getTipoPagamento() {
        return tipoPagamento;
    }

    public void setTipoPagamento(TipoPagamento tipoPagamento) {
        this.tipoPagamento = tipoPagamento;
    }
}
