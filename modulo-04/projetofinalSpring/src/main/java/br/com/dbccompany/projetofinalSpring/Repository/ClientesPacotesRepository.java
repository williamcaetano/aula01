package br.com.dbccompany.projetofinalSpring.Repository;

import br.com.dbccompany.projetofinalSpring.Entity.ClientesPacotes;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ClientesPacotesRepository extends CrudRepository<ClientesPacotes, Integer> {
}
