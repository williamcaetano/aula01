package br.com.dbccompany.projetofinalSpring.Controller;

import br.com.dbccompany.projetofinalSpring.Entity.Clientes;
import br.com.dbccompany.projetofinalSpring.Service.ClientesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@Controller
@RequestMapping("/api/clientes")
public class ClientesController {

    @Autowired
    ClientesService service;

    @GetMapping(value = "/")
    @ResponseBody
    public List<Clientes> todosClientes(){
        return service.todosClientes();
    }

    @PostMapping(value = "/novo")
    @ResponseBody
    public Clientes novoClientes(@Valid @RequestBody Clientes cliente ) throws Exception{
        return service.salvar(cliente);
    }

    @PutMapping(value = "/editar/{id}")
    @ResponseBody
    public Clientes editarClientes(@Valid @PathVariable Integer id, @RequestBody Clientes cliente) {
        return service.editar(id, cliente);
    }
}
