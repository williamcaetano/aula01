package br.com.dbccompany.projetofinalSpring.Entity;

import javax.persistence.*;
import java.util.Date;

@Entity
public class Acessos {
    @Id
    @Column(name = "ID_ACESSO")
    @SequenceGenerator(allocationSize = 1, name = "ACESSO_SEQ", sequenceName = "ACESSO_SEQ")
    @GeneratedValue(generator = "ACESSO_SEQ", strategy = GenerationType.SEQUENCE)
    private Integer id;

    @ManyToOne(fetch=FetchType.LAZY)
    @JoinColumns({
            @JoinColumn(name = "ID_ESPACO", insertable = false, updatable = false),
            @JoinColumn(name = "ID_CLIENTE", insertable = false, updatable = false)
    })
    private SaldoCliente saldoCliente;

    @Column(name = "DATA")
    private Date data;

    @Column(name = "IS_ENTRADA")
    private Boolean isEntrada;

    @Column(name = "IS_EXCECAO")
    private Boolean isExcecao;

    public SaldoCliente getSaldoCliente() {
        return saldoCliente;
    }

    public void setSaldoCliente(SaldoCliente saldoCliente) {
        this.saldoCliente = saldoCliente;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Date getData() {
        return data;
    }

    public void setData(Date data) {
        this.data = data;
    }

    public Boolean getEntrada() {
        return isEntrada;
    }

    public void setEntrada(Boolean entrada) {
        isEntrada = entrada;
    }

    public Boolean getExcecao() {
        return isExcecao;
    }

    public void setExcecao(Boolean excecao) {
        isExcecao = excecao;
    }
}
