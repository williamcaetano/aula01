package br.com.dbccompany.projetofinalSpring.Controller;

import br.com.dbccompany.projetofinalSpring.Entity.Usuario;
import br.com.dbccompany.projetofinalSpring.Service.UsuarioService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@Controller
@RequestMapping("/api/usuario")
public class UsuarioController {

    @Autowired
    UsuarioService service;

    @GetMapping(value = "/")
    @ResponseBody
    public List<Usuario> todosUsuarios(){
        return service.todosUsuarios();
    }

    @PostMapping(value = "/novo")
    @ResponseBody
    public Usuario novoUsuario(@Valid @RequestBody Usuario usuario) {
        return service.salvar(usuario);
    }

    @PutMapping(value = "/editar/{id}")
    @ResponseBody
    public Usuario editarUsuarios(@Valid @PathVariable Integer id, @RequestBody Usuario usuario) {
        return service.editar(id, usuario);
    }
}
