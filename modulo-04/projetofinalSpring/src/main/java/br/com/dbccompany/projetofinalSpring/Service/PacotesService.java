package br.com.dbccompany.projetofinalSpring.Service;

import br.com.dbccompany.projetofinalSpring.Entity.Pacotes;
import br.com.dbccompany.projetofinalSpring.Repository.PacotesRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;


@Service
public class PacotesService {

    @Autowired
    private PacotesRepository pacotesRepository;

    @Transactional( rollbackFor = Exception.class)
    public Pacotes salvar(Pacotes pacotes){
        return pacotesRepository.save(pacotes);
    }

    @Transactional( rollbackFor = Exception.class)
    public Pacotes editar( Integer id, Pacotes pacotes){
        pacotes.setId(id);
        return pacotesRepository.save(pacotes);
    }

    public Pacotes getPacotes(Integer id){
        return pacotesRepository.findById(id).get();
    }

    public List<Pacotes> todosPacotes(){
        return (List<Pacotes>) pacotesRepository.findAll();
    }
}

