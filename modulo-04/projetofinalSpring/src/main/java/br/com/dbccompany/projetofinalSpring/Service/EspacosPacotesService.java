package br.com.dbccompany.projetofinalSpring.Service;

import br.com.dbccompany.projetofinalSpring.Entity.EspacosPacotes;
import br.com.dbccompany.projetofinalSpring.Entity.Pacotes;
import br.com.dbccompany.projetofinalSpring.Repository.EspacosPacotesRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;


@Service
public class EspacosPacotesService {

    @Autowired
    private EspacosPacotesRepository espacoPacotesRepository;

    @Transactional( rollbackFor = Exception.class)
    public EspacosPacotes salvar(EspacosPacotes espacoPacotes){
        return espacoPacotesRepository.save(espacoPacotes);
    }

    @Transactional( rollbackFor = Exception.class)
    public EspacosPacotes editar( Integer id, EspacosPacotes espacoPacotes){
        espacoPacotes.setId(id);
        return espacoPacotesRepository.save(espacoPacotes);
    }

    public EspacosPacotes getEspacoPacotes(Integer id){
        return espacoPacotesRepository.findById(id).get();
    }

    public List<EspacosPacotes> todosEspacoPacotes(){
        return (List<EspacosPacotes>) espacoPacotesRepository.findAll();
    }

    public List<EspacosPacotes> buscarPorPacote(Pacotes pacote){
        return espacoPacotesRepository.buscarTodosPorPacote(pacote);
    }
}

