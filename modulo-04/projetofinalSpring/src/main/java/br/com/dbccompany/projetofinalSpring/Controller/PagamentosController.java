package br.com.dbccompany.projetofinalSpring.Controller;

import br.com.dbccompany.projetofinalSpring.Entity.Pagamentos;
import br.com.dbccompany.projetofinalSpring.Service.PagamentosService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping("/api/pagamentos")
public class PagamentosController {

    @Autowired
    PagamentosService service;

    @GetMapping(value = "/")
    @ResponseBody
    public List<Pagamentos> todosPagamentos(){
        return service.todosPagamentos();
    }

    @PostMapping(value = "/novo")
    @ResponseBody
    public Pagamentos novoPagamento(@RequestBody Pagamentos pagamento) {
        return service.salvar(pagamento);
    }

    @PutMapping(value = "/editar/{id}")
    @ResponseBody
    public Pagamentos editarPagamentos(@PathVariable Integer id, @RequestBody Pagamentos pagamento) {
        return service.editar(id, pagamento);
    }
}
