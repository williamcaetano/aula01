package br.com.dbccompany.projetofinalSpring.Entity;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
public class Contratacao {
    @Id
    @Column(name = "ID_CONTRATACAO")
    @SequenceGenerator(allocationSize = 1, name = "CONTRATACAO_SEQ", sequenceName = "CONTRATACAO_SEQ")
    @GeneratedValue(generator = "CONTRATACAO_SEQ", strategy = GenerationType.SEQUENCE)
    private Integer id;
    @Column(name = "QUANTIDADE",nullable = false)
    private Integer quantidade;
    @Column(name = "PRAZO_DIAS",nullable = false)
    private Integer prazoDias;
    @Column(name = "DESCONTO")
    private Double desconto;

    @ManyToOne
    @JoinColumn(name = "id_espaco")
    private Espacos espaco;

    @ManyToOne
    @JoinColumn(name = "id_cliente")
    private Clientes cliente;

    @OneToMany(mappedBy = "contratacao", cascade = CascadeType.ALL)
    private List<Pagamentos> pagamentos = new ArrayList<>();

    @Enumerated(EnumType.STRING)
    private TipoContratacao tipoContratacao;

    public Espacos getEspaco() {
        return espaco;
    }

    public void setEspaco(Espacos espaco) {
        this.espaco = espaco;
    }

    public List<Pagamentos> getPagamentos() {
        return pagamentos;
    }

    public void setPagamentos(List<Pagamentos> pagamentos) {
        this.pagamentos = pagamentos;
    }

    public Clientes getCliente() {
        return cliente;
    }

    public void setCliente(Clientes cliente) {
        this.cliente = cliente;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getQuantidade() {
        return quantidade;
    }

    public void setQuantidade(Integer quantidade) {
        this.quantidade = quantidade;
    }

    public Integer getPrazoDias() {
        return prazoDias;
    }

    public void setPrazoDias(Integer prazoDias) {
        this.prazoDias = prazoDias;
    }

    public Double getDesconto() {
        return desconto;
    }

    public void setDesconto(Double desconto) {
        this.desconto = desconto;
    }

    public TipoContratacao getTipoContratacao() {
        return tipoContratacao;
    }

    public void setTipoContratacao(TipoContratacao tipoContratacao) {
        this.tipoContratacao = tipoContratacao;
    }
}
