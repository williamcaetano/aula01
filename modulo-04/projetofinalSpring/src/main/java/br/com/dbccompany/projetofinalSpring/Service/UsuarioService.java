package br.com.dbccompany.projetofinalSpring.Service;

import br.com.dbccompany.projetofinalSpring.CriptografiaMD5;
import br.com.dbccompany.projetofinalSpring.Entity.Usuario;
import br.com.dbccompany.projetofinalSpring.Repository.UsuarioRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


import java.util.List;


@Service
public class UsuarioService {

    @Autowired
    private UsuarioRepository usuariosRepository;

    @Transactional( rollbackFor = Exception.class)
    public Usuario salvar(Usuario usuario) {
        if (usuario.getSenha().length() >= 6) {
            CriptografiaMD5 md5 = new CriptografiaMD5();
            String senhaCriptografada = md5.criptografar(usuario.getSenha());
            usuario.setSenha(senhaCriptografada);

            return usuariosRepository.save(usuario);
        }
        System.out.println("A senha deve conter no minimo 6 digitos");
        return null;
    }

    @Transactional( rollbackFor = Exception.class)
    public Usuario editar( Integer id, Usuario usuario){
        usuario.setId(id);
        return usuariosRepository.save(usuario);
    }

    @Transactional(rollbackFor = Exception.class)
    public void deletar(Usuario usuario) {
        usuariosRepository.delete(usuario);
    }

    public Usuario getUsuario(Integer id){
        return usuariosRepository.findById(id).get();
    }

    public List<Usuario> todosUsuarios(){
        return (List<Usuario>) usuariosRepository.findAll();
    }
}

