package br.com.dbccompany.projetofinalSpring.Entity;

import javax.persistence.*;

@Entity
public class Contato {
    @Id
    @Column(name = "ID_CONTATO")
    @SequenceGenerator( allocationSize = 1, name = "CONTATO_SEQ", sequenceName = "CONTATO_SEQ")
    @GeneratedValue(generator = "CONTATO_SEQ", strategy = GenerationType.SEQUENCE)
    private Integer id;
    @Column(name = "VALOR", nullable = false)
    private String valor;

    @ManyToOne
    @JoinColumn(name = "id_Tipo_contato")
    private TipoContato tipoContato;

    @ManyToOne
    @JoinColumn(name = "id_cliente")
    private Clientes cliente;

    public Clientes getCliente() {
        return cliente;
    }

    public void setCliente(Clientes cliente) {
        this.cliente = cliente;
    }

    public TipoContato getTipoContato() {
        return tipoContato;
    }

    public void setTipoContato(TipoContato tipoContato) {
        this.tipoContato = tipoContato;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getValor() {
        return valor;
    }

    public void setValor(String valor) {
        this.valor = valor;
    }
}
