package br.com.dbccompany.projetofinalSpring.Service;

import br.com.dbccompany.projetofinalSpring.Entity.Espacos;
import br.com.dbccompany.projetofinalSpring.Repository.EspacosRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;


@Service
public class EspacosService {

    @Autowired
    private EspacosRepository espacosRepository;

    @Transactional( rollbackFor = Exception.class)
    public Espacos salvar(Espacos espacos){
        return espacosRepository.save(espacos);
    }

    @Transactional( rollbackFor = Exception.class)
    public Espacos editar( Integer id, Espacos espacos){
        espacos.setId(id);
        return espacosRepository.save(espacos);
    }

    public Espacos getEspacos(Integer id){
        return espacosRepository.findById(id).get();
    }

    public List<Espacos> todosEspacos(){
        return (List<Espacos>) espacosRepository.findAll();
    }
}

