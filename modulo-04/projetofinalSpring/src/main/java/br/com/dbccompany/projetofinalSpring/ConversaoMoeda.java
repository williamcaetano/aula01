package br.com.dbccompany.projetofinalSpring;

import java.text.NumberFormat;
import java.text.ParseException;
import java.util.Locale;

public class ConversaoMoeda {

    public Double moedaParaValor(String valorMoeda) throws ParseException {
        NumberFormat formato = NumberFormat.getNumberInstance(new Locale("pt", "BR"));
        return formato.parse(valorMoeda.replaceAll("[^\\d.,]", "")).doubleValue();
    }

    public String valorParaMoeda(Double valor) {
        return NumberFormat.getCurrencyInstance(new Locale("pt", "BR")).format(valor);
    }
}
