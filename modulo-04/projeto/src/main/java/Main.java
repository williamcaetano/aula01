import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Main {
    public static void main(String[] args){
        Connection conn = Connector.connect();
        try{
            /*
            ResultSet rs = conn.prepareStatement("select tname from tab where tname = 'PAISES'").executeQuery();
            if (!rs.next()) {
                conn.prepareStatement("CREATE TABLE PAISES(\n"
                +"ID_PAIS INTEGER NOT NULL PRIMARY KEY,\n"
                +"NOME VARCHAR(100) NOT NULL\n"
                +")").execute();
            }
            PreparedStatement pst = conn.prepareStatement("insert into paises(id_pais, nome) "
                                    + "values (paises_seq.nextval, ?)");
            pst.setString(1, "BRASIL");
            pst.executeUpdate();

            rs = conn.prepareStatement("select * from paises").executeQuery();
            while(rs.next()){
                System.out.println(String.format("Nome do pais: %s", rs.getString("nome")));
            }

            ResultSet rs1 = conn.prepareStatement("select tname from tab where tname = 'ESTADOS'").executeQuery();
            if (!rs1.next()) {
                conn.prepareStatement("CREATE TABLE ESTADOS(\n"
                        +"ID_ESTADO INTEGER NOT NULL PRIMARY KEY,\n"
                        +"NOME VARCHAR(100) NOT NULL"
                        +")").execute();
            }
            PreparedStatement pst1 = conn.prepareStatement("insert into estados(id_estado, nome) "
                    + "values (estados_seq.nextval, ?)");
            pst1.setString(1, "RIO GRANDE DO SUL");
            pst1.executeUpdate();

            rs1 = conn.prepareStatement("select * from estados").executeQuery();
            while(rs1.next()){
                System.out.println(String.format("Nome do estado: %s", rs1.getString("nome")));
            }

            ResultSet rs1 = conn.prepareStatement("select tname from tab where tname = 'CIDADES'").executeQuery();
            if (!rs1.next()) {
                conn.prepareStatement("CREATE TABLE CIDADES(\n"
                        +"ID_CIDADE INTEGER NOT NULL PRIMARY KEY,\n"
                        +"NOME VARCHAR(100) NOT NULL"
                        +")").execute();
            }
            PreparedStatement pst1 = conn.prepareStatement("insert into cidades(id_cidade, nome) "
                    + "values (estados_seq.nextval, ?)");
            pst1.setString(1, "NOVO HAMBURGO");
            pst1.executeUpdate();

            rs1 = conn.prepareStatement("select * from cidades").executeQuery();
            while(rs1.next()){
                System.out.println(String.format("Nome da cidade: %s", rs1.getString("nome")));
            }

            ResultSet rs1 = conn.prepareStatement("select tname from tab where tname = 'BAIRROS'").executeQuery();
            if (!rs1.next()) {
                conn.prepareStatement("CREATE TABLE BAIRROS(\n"
                        +"ID_BAIRRO INTEGER NOT NULL PRIMARY KEY,\n"
                        +"NOME VARCHAR(100) NOT NULL"
                        +")").execute();
            }

            PreparedStatement pst1 = conn.prepareStatement("insert into bairros(id_bairro, nome) "
                    + "values (bairros_seq.nextval, ?)");
            pst1.setString(1, "Rondonia");
            pst1.executeUpdate();

            rs1 = conn.prepareStatement("select * from bairros").executeQuery();
            while(rs1.next()) {
                System.out.println(String.format("Nome do bairro: %s", rs1.getString("nome")));
            }

            PreparedStatement pst = conn.prepareStatement("insert into paises(id_pais, nome) "
                    + "values (paises_seq.nextval, ?)");
            pst.setString(1, "ESTADOS UNIDOS");
            pst.executeUpdate();
            */

            ResultSet rs1 = conn.prepareStatement("select * from paises").executeQuery();
            while(rs1.next()) {
                System.out.println(String.format("Nome do pais: %s", rs1.getString("nome")));
            }
            System.out.println(rs1.isLast());

        }catch (SQLException ex){
            Logger.getLogger(Connector.class.getName()).log(Level.SEVERE, "ERRO DE CONEXÃO", ex);
        }
    }
}
