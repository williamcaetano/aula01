import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/*
* ARQUIVO DE CONEXÃO COM O BANCO DE DADOS ORACLE, VERIFICA SE A CONEXÃO É VÁLIDA, CASO CONTRARIO
* CRIA UMA CLASSE COM O DRIVE DE CONEXÃO, SETA OS DADOS DE CONEXÃO E EXECUTA O GET
*
* ERRO: CASO CAIA NO CATCH CRIA UM LOG DE ERRO DE CONEXÃO
* */

public class Connector {

    private static Connection conn;

    public static Connection connect(){
        try{
            if (conn != null && conn.isValid(10)){
                return conn;
            }

            Class.forName("oracle.jdbc.driver.OracleDriver");
            conn = DriverManager.getConnection("jdbc:oracle:thin:@localhost:49161:XE",
                    "system",
                    "william");
        }catch (ClassNotFoundException | SQLException ex){
            Logger.getLogger(Connector.class.getName()).log(Level.SEVERE, "ERRO DE CONEXÃO", ex);
        }
        return conn;
    }
}
