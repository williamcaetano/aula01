package br.com.dbccompany.bancodigital.Service;

import java.util.logging.Level;
import java.util.logging.Logger;

import org.hibernate.Transaction;

import br.com.dbccompany.bancodigital.Dao.EstadosDAO;
import br.com.dbccompany.bancodigital.Dto.EstadosDTO;
import br.com.dbccompany.bancodigital.Entity.HibernateUtil;
import br.com.dbccompany.bancodigital.Entity.Estados;

public class EstadosService {
	private static final EstadosDAO ESTADOS_DAO = new EstadosDAO();
	private static final Logger LOG = Logger.getLogger(EstadosService.class.getName());
	
	public void salvarEstadosDTO(EstadosDTO estadosDTO) {
		boolean started = HibernateUtil.beginTransaction();
		Transaction transaction = HibernateUtil.getSession().getTransaction();
		
		Estados estados = ESTADOS_DAO.parseFrom(estadosDTO);
		
		try {
			Estados estadosRes = ESTADOS_DAO.buscar(1);
			if(estadosRes == null) {
				ESTADOS_DAO.criar(estados);
			}
			else {
				estados.setId(estadosRes.getId());
				ESTADOS_DAO.atualizar(estados);
			}
			
			if (started) {
				transaction.commit();
			}
			
			estadosDTO.setIdEstados(estados.getId());
			
		} catch ( Exception ex ) {
			transaction.rollback();
			LOG.log(Level.SEVERE, ex.getMessage(), ex);
		}
	}
	
	public void salvarEstados(Estados estado) {
		boolean started = HibernateUtil.beginTransaction();
		Transaction transaction = HibernateUtil.getSession().getTransaction();
		
		
		try {
			Estados estadosRes = ESTADOS_DAO.buscar(estado.getId());
			if(estadosRes == null) {
				ESTADOS_DAO.criar(estado);
			}
			else {
				estado.setId(estadosRes.getId());
				ESTADOS_DAO.atualizar(estado);
			}
			
			if (started) {
				transaction.commit();
			}
			
		} catch ( Exception ex ) {
			transaction.rollback();
			LOG.log(Level.SEVERE, ex.getMessage(), ex);
		}
	}
}
