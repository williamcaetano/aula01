package br.com.dbccompany.bancodigital.Dao;

import br.com.dbccompany.bancodigital.Dto.CidadesDTO;
import br.com.dbccompany.bancodigital.Entity.Cidades;

public class CidadesDAO extends AbstractDAO<Cidades>{
	
	public Cidades parseFrom(CidadesDTO dto) {
		Cidades cidades = null;
		
		if(dto.getIdCidades() != null) {
			cidades = buscar(dto.getIdCidades());
		}
		else {
			cidades = new Cidades();
			cidades.setNome(dto.getNome());
			
			EstadosDAO estadoDAO = new EstadosDAO();
			cidades.setEstado(estadoDAO.parseFrom(dto.getEstados()));
		}
		
		return cidades;
	}
	
	
	@Override
	protected  Class<Cidades> getEntityClass(){
		return Cidades.class;
	}
}
