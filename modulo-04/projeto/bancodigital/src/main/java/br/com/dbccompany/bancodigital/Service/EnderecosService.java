package br.com.dbccompany.bancodigital.Service;

import java.util.logging.Level;
import java.util.logging.Logger;

import org.hibernate.Transaction;

import br.com.dbccompany.bancodigital.Dao.EnderecosDAO;
import br.com.dbccompany.bancodigital.Dto.EnderecosDTO;
import br.com.dbccompany.bancodigital.Entity.HibernateUtil;
import br.com.dbccompany.bancodigital.Entity.Enderecos;

public class EnderecosService {
	private static final EnderecosDAO ENDERECOS_DAO = new EnderecosDAO();
	private static final Logger LOG = Logger.getLogger(EnderecosService.class.getName());
	
	public void salvarEnderecosDTO(EnderecosDTO enderecoDTO) {
		boolean started = HibernateUtil.beginTransaction();
		Transaction transaction = HibernateUtil.getSession().getTransaction();
		
		Enderecos endereco = ENDERECOS_DAO.parseFrom(enderecoDTO);
		
		try {
			Enderecos enderecosRes = ENDERECOS_DAO.buscar(1);
			if(enderecosRes == null) {
				ENDERECOS_DAO.criar(endereco);
			}
			else {
				endereco.setId(enderecosRes.getId());
				ENDERECOS_DAO.atualizar(endereco);
			}
			
			if (started) {
				transaction.commit();
			}
			
			enderecoDTO.setIdEendereco(endereco.getId());
			
		} catch ( Exception ex ) {
			transaction.rollback();
			LOG.log(Level.SEVERE, ex.getMessage(), ex);
		}
		
	}
	
	public void salvarEnderecos(Enderecos endereco) {
		boolean started = HibernateUtil.beginTransaction();
		Transaction transaction = HibernateUtil.getSession().getTransaction();
		
		try {
			Enderecos enderecosRes = ENDERECOS_DAO.buscar(endereco.getId());
			if(enderecosRes == null) {
				ENDERECOS_DAO.criar(endereco);
			}
			else {
				endereco.setId(enderecosRes.getId());
				ENDERECOS_DAO.atualizar(endereco);
			}
			
			if (started) {
				transaction.commit();
			}
			
			
		} catch ( Exception ex ) {
			transaction.rollback();
			LOG.log(Level.SEVERE, ex.getMessage(), ex);
		}
		
	}
}
