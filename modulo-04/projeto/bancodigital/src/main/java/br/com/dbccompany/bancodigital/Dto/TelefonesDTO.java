package br.com.dbccompany.bancodigital.Dto;

public class TelefonesDTO {
	
	private Integer idTelefones;
	private String numero;
	
	public Integer getIdTelefones() {
		return idTelefones;
	}
	public void setIdTelefones(Integer idTelefone) {
		this.idTelefones = idTelefone;
	}
	public String getNumero() {
		return numero;
	}
	public void setNumero(String numero) {
		this.numero = numero;
	}
	
	
}
