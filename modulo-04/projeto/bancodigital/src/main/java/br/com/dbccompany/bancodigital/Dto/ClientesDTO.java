package br.com.dbccompany.bancodigital.Dto;

import br.com.dbccompany.bancodigital.Entity.EstadoCivil;

public class ClientesDTO {
	
	private Integer idClientes;
	private String nome, cpf, rg, dataNasc;
	private EstadoCivil estadoCivil;
	private EnderecosDTO endereco;
	
	public Integer getIdClientes() {
		return idClientes;
	}
	public void setIdClientes(Integer idClientes) {
		this.idClientes = idClientes;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getCpf() {
		return cpf;
	}
	public void setCpf(String cpf) {
		this.cpf = cpf;
	}
	public String getRg() {
		return rg;
	}
	public void setRg(String rg) {
		this.rg = rg;
	}
	public String getDataNasc() {
		return dataNasc;
	}
	public void setDataNasc(String dataNasc) {
		this.dataNasc = dataNasc;
	}
	public EstadoCivil getEstadoCivil() {
		return estadoCivil;
	}
	public void setEstadoCivil(EstadoCivil estadoCivil) {
		this.estadoCivil = estadoCivil;
	}
	public EnderecosDTO getEndereco() {
		return endereco;
	}
	public void setEndereco(EnderecosDTO endereco) {
		this.endereco = endereco;
	}
	
}
