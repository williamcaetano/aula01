package br.com.dbccompany.bancodigital.Dao;

import br.com.dbccompany.bancodigital.Dto.EmailsDTO;
import br.com.dbccompany.bancodigital.Entity.Emails;

public class EmailsDAO extends AbstractDAO<Emails>{
	
	public Emails parseFrom(EmailsDTO emailDTO) {
		Emails emails = null;
		
		if(emailDTO.getIdEmails() != null) {
			emails = buscar(emailDTO.getIdEmails());
		}
		emails = new Emails();
		emails.setEmail(emailDTO.getEmail());
		
		return emails;
	}
	
	@Override
	protected  Class<Emails> getEntityClass(){
		return Emails.class;
	}

}
