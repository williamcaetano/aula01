package br.com.dbccompany.bancodigital.Service;

import java.util.logging.Level;
import java.util.logging.Logger;

import org.hibernate.Transaction;

import br.com.dbccompany.bancodigital.Dao.EmailsDAO;
import br.com.dbccompany.bancodigital.Dto.EmailsDTO;
import br.com.dbccompany.bancodigital.Entity.HibernateUtil;
import br.com.dbccompany.bancodigital.Entity.Emails;

public class EmailsService {
	private static final EmailsDAO EMAILS_DAO = new EmailsDAO();
	private static final Logger LOG = Logger.getLogger(EmailsService.class.getName());
	
	public void salvarEmailsDTO(EmailsDTO emailDTO) {
		boolean started = HibernateUtil.beginTransaction();
		Transaction transaction = HibernateUtil.getSession().getTransaction();
		
		Emails email = EMAILS_DAO.parseFrom(emailDTO);
		
		try {
			Emails emailRes = EMAILS_DAO.buscar(1);
			if(emailRes == null) {
				EMAILS_DAO.criar(email);
			}
			else {
				email.setId(emailRes.getId());
				EMAILS_DAO.atualizar(email);
			}
			
			if (started) {
				transaction.commit();
			}
			
			emailDTO.setIdEmails(email.getId());
		} catch ( Exception ex ) {
			transaction.rollback();
			LOG.log(Level.SEVERE, ex.getMessage(), ex);
		}
		
	}
	
	public void salvarEmails(Emails email) {
		boolean started = HibernateUtil.beginTransaction();
		Transaction transaction = HibernateUtil.getSession().getTransaction();
		try {
			Emails emailRes = EMAILS_DAO.buscar(email.getId());
			if(emailRes == null) {
				EMAILS_DAO.criar(email);
			}
			else {
				email.setId(emailRes.getId());
				EMAILS_DAO.atualizar(email);
			}
			
			if (started) {
				transaction.commit();
			}
			
		} catch ( Exception ex ) {
			transaction.rollback();
			LOG.log(Level.SEVERE, ex.getMessage(), ex);
		}
		
	}
}
