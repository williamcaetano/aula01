package br.com.dbccompany.bancodigital.Service;

import java.util.logging.Level;
import java.util.logging.Logger;

import org.hibernate.Transaction;

import br.com.dbccompany.bancodigital.Dao.BancosDAO;
import br.com.dbccompany.bancodigital.Dto.BancosDTO;
import br.com.dbccompany.bancodigital.Entity.HibernateUtil;
import br.com.dbccompany.bancodigital.Entity.Bancos;

public class BancosService {
	private static final BancosDAO BANCOS_DAO = new BancosDAO();
	private static final Logger LOG = Logger.getLogger(BancosService.class.getName());
	
	public void salvarBancosDTO(BancosDTO bancoDTO) {
		boolean started = HibernateUtil.beginTransaction();
		Transaction transaction = HibernateUtil.getSession().getTransaction();
		
		Bancos banco = BANCOS_DAO.parseFrom(bancoDTO);
		
		try {
			Bancos bancoRes = BANCOS_DAO.buscar(1);
			if(bancoRes == null) {
				BANCOS_DAO.criar(banco);
			}else {
				banco.setId(bancoRes.getId());
				BANCOS_DAO.atualizar(banco);
			}
			
			if (started) {
				transaction.commit();
			}
			
			bancoDTO.setIdBancos(banco.getId());
		} catch ( Exception ex ) {
			transaction.rollback();
			LOG.log(Level.SEVERE, ex.getMessage(), ex);
		}
		
	}
	
	public void salvarBancos(Bancos banco) {
		boolean started = HibernateUtil.beginTransaction();
		Transaction transaction = HibernateUtil.getSession().getTransaction();
		
		try {
			Bancos bancoRes = BANCOS_DAO.buscar(banco.getId());
			if(bancoRes == null) {
				BANCOS_DAO.criar(banco);
			}else {
				banco.setId(bancoRes.getId());
				BANCOS_DAO.atualizar(banco);
			}
			
			if (started) {
				transaction.commit();
			}
			
		} catch ( Exception ex ) {
			transaction.rollback();
			LOG.log(Level.SEVERE, ex.getMessage(), ex);
		}
		
	}
}

