package br.com.dbccompany.bancodigital.Service;

import java.util.logging.Level;
import java.util.logging.Logger;

import org.hibernate.Transaction;

import br.com.dbccompany.bancodigital.Dao.TelefonesDAO;
import br.com.dbccompany.bancodigital.Dto.TelefonesDTO;
import br.com.dbccompany.bancodigital.Entity.HibernateUtil;
import br.com.dbccompany.bancodigital.Entity.Telefones;

public class TelefonesService {
	private static final TelefonesDAO TELEFONES_DAO = new TelefonesDAO();
	private static final Logger LOG = Logger.getLogger(TelefonesService.class.getName());
	
	public void salvarTelefonesDTO(TelefonesDTO telefoneDTO) {
		boolean started = HibernateUtil.beginTransaction();
		Transaction transaction = HibernateUtil.getSession().getTransaction();
		
		Telefones telefone = TELEFONES_DAO.parseFrom(telefoneDTO);
		
		try {
			Telefones telefoneRes = TELEFONES_DAO.buscar(telefoneDTO.getIdTelefones());
			if(telefoneRes == null) {
				TELEFONES_DAO.criar(telefone);
			}else {
				telefone.setId(telefoneRes.getId());
				TELEFONES_DAO.atualizar(telefone);
			}
			
			if (started) {
				transaction.commit();
			}
			
			telefoneDTO.setIdTelefones(telefone.getId());
		} catch ( Exception ex ) {
			transaction.rollback();
			LOG.log(Level.SEVERE, ex.getMessage(), ex);
		}
		
	}
	
	public void salvarTelefones(Telefones telefone) {
		boolean started = HibernateUtil.beginTransaction();
		Transaction transaction = HibernateUtil.getSession().getTransaction();
		
		try {
			Telefones telefoneRes = TELEFONES_DAO.buscar(telefone.getId());
			if(telefoneRes == null) {
				TELEFONES_DAO.criar(telefone);
			}else {
				telefone.setId(telefoneRes.getId());
				TELEFONES_DAO.atualizar(telefone);
			}
			
			if (started) {
				transaction.commit();
			}
			
		} catch ( Exception ex ) {
			transaction.rollback();
			LOG.log(Level.SEVERE, ex.getMessage(), ex);
		}
		
	}
}
