package br.com.dbccompany.bancodigital;


import java.util.ArrayList;
import java.util.List;

//import java.util.ArrayList;
//import java.util.List;
//import java.util.logging.Level;

//import org.hibernate.Criteria;
//import org.hibernate.Session;
//import org.hibernate.Transaction;
//import org.hibernate.criterion.Restrictions;

import java.util.logging.Logger;

import br.com.dbccompany.bancodigital.Entity.Agencias;
import br.com.dbccompany.bancodigital.Entity.Bairros;
import br.com.dbccompany.bancodigital.Entity.Bancos;
import br.com.dbccompany.bancodigital.Entity.Cidades;
import br.com.dbccompany.bancodigital.Entity.Clientes;
import br.com.dbccompany.bancodigital.Entity.ContasType;
import br.com.dbccompany.bancodigital.Entity.Correntistas;
import br.com.dbccompany.bancodigital.Entity.Emails;
import br.com.dbccompany.bancodigital.Entity.Enderecos;
import br.com.dbccompany.bancodigital.Entity.EstadoCivil;
import br.com.dbccompany.bancodigital.Entity.Estados;
import br.com.dbccompany.bancodigital.Entity.Paises;
import br.com.dbccompany.bancodigital.Entity.Telefones;
import br.com.dbccompany.bancodigital.Entity.TelefonesType;
import br.com.dbccompany.bancodigital.Service.AgenciasService;
import br.com.dbccompany.bancodigital.Service.BairrosService;
import br.com.dbccompany.bancodigital.Service.BancosService;
import br.com.dbccompany.bancodigital.Service.CidadesService;
import br.com.dbccompany.bancodigital.Service.ClientesService;
import br.com.dbccompany.bancodigital.Service.CorrentistasService;
import br.com.dbccompany.bancodigital.Service.EmailsService;
import br.com.dbccompany.bancodigital.Service.EnderecosService;
import br.com.dbccompany.bancodigital.Service.EstadosService;
import br.com.dbccompany.bancodigital.Service.PaisesService;
import br.com.dbccompany.bancodigital.Service.TelefonesService;

public class Main {
	
	private static final Logger LOG = Logger.getLogger(Main.class.getName());
	private static final TelefonesType CELULAR = null;
	
	public static void main(String[] args) {
		
		//PAISES
		PaisesService servicePais = new PaisesService();
		Paises pais = new Paises();
		Paises pais2 = new Paises();
		pais.setNome("Argentina");
		pais2.setNome("Brasil");
		servicePais.salvarPaises(pais);
		servicePais.salvarPaises(pais2);
		
		//ESTADOS
		EstadosService serviceEstado =  new EstadosService();
		Estados estado = new Estados();
		Estados estado2 = new Estados();
		estado.setNome("Cordova");
		estado2.setNome("Rio Grande do Sul");
		estado.setPais(pais);
		estado2.setPais(pais2);
		serviceEstado.salvarEstados(estado);
		serviceEstado.salvarEstados(estado2);
		
		//CIDADES
		CidadesService serviceCidade = new CidadesService();
		Cidades cidade = new Cidades();
		Cidades cidade2 = new Cidades();
		cidade.setNome("Cordova");
		cidade2.setNome("Porto Alegre");
		cidade.setEstado(estado);
		cidade2.setEstado(estado2);
		serviceCidade.salvarCidades(cidade);
		serviceCidade.salvarCidades(cidade2);
		
		//BAIRROS
		BairrosService serviceBairro = new BairrosService();
		Bairros bairro = new Bairros();
		Bairros bairro2 = new Bairros();
		bairro.setNome("Guemes");
		bairro2.setNome("Passo D'areia");
		bairro.setCidade(cidade);
		bairro2.setCidade(cidade2);
		serviceBairro.salvarBairros(bairro);
		serviceBairro.salvarBairros(bairro2);
		
		//ENDERECOS
		EnderecosService serviceEndereco = new EnderecosService();
		Enderecos endereco = new Enderecos();
		Enderecos endereco2 = new Enderecos();
		Enderecos endereco3 = new Enderecos();
		Enderecos endereco4 = new Enderecos();
		Enderecos endereco5 = new Enderecos();
		Enderecos endereco6 = new Enderecos();
		endereco.setLogradouro("Rua Palermo");
		endereco.setNumero(33);
		endereco.setBairro(bairro);
		endereco2.setLogradouro("Av. Andara�");
		endereco2.setNumero(531);
		endereco2.setBairro(bairro2);
		endereco3.setLogradouro("Av. Pedro Adams Filho");
		endereco3.setNumero(51);
		endereco3.setBairro(bairro);
		endereco4.setLogradouro("Av. Siete de Setembro");
		endereco4.setNumero(152);
		endereco4.setBairro(bairro2);
		endereco5.setLogradouro("Rua Guia L�pez");
		endereco5.setNumero(22);
		endereco5.setBairro(bairro);
		endereco6.setLogradouro("Rua Bartolomeu de Gusm�o");
		endereco6.setNumero(437);
		endereco6.setBairro(bairro2);
		serviceEndereco.salvarEnderecos(endereco);
		serviceEndereco.salvarEnderecos(endereco2);
		serviceEndereco.salvarEnderecos(endereco3);
		serviceEndereco.salvarEnderecos(endereco4);
		serviceEndereco.salvarEnderecos(endereco5);
		serviceEndereco.salvarEnderecos(endereco6);
		
		//BANCOS X AGENCIAS X CORRENTISTAS
		AgenciasService serviceAgencia = new AgenciasService();
		Agencias agencia = new Agencias();
		Agencias agencia2 = new Agencias();
		//SETAR DADOS AGENCIAS
		agencia.setNome("Agencia argentina");
		agencia.setCodigo(171);
		agencia2.setNome("Agencia brasileira");
		agencia2.setCodigo(71);
		BancosService serviceBanco = new BancosService();
		//CRIAR LISTA DE AGENCIAS NOS BANCOS
		List<Agencias> agenciasBanco = new ArrayList<>();
		List<Agencias> agenciasBanco2 = new ArrayList<>();
		Bancos banco = new Bancos();
		Bancos banco2 = new Bancos();
		//SETAR DADOS DOS BANCOS (LIST AGENCIAS NO BANCO E BANCO NAS AGENCIAS)
		banco.setNome("de l� Nacion");
		banco.setCodigo(1010);
		agenciasBanco.add(agencia);
		agencia.setBanco(banco);
		
		banco.setAgencias(agenciasBanco);
		banco2.setNome("BancoDBC");
		banco2.setCodigo(2130);
		agenciasBanco2.add(agencia2);
		agencia2.setBanco(banco2);
		banco2.setAgencias(agenciasBanco2);
		
		//TELEFONES
		TelefonesService serviceTelefone = new TelefonesService();
		Telefones telefone = new Telefones();
		Telefones telefone2 = new Telefones();
		Telefones telefone3 = new Telefones();
		Telefones telefone4 = new Telefones();
		telefone.setNumero("98765-4321");
		telefone.setTiposTelefone(TelefonesType.CELULAR);
		telefone2.setNumero("91234-8765");
		telefone2.setTiposTelefone(TelefonesType.CELULAR);
		telefone3.setNumero("94567-4321");
		telefone3.setTiposTelefone(TelefonesType.CELULAR);
		telefone4.setNumero("95678-1234");
		telefone4.setTiposTelefone(TelefonesType.CELULAR);
		
		
		//CORRENTISTAS
		CorrentistasService serviceCorrentista = new CorrentistasService();
		Correntistas correntista = new Correntistas();
		Correntistas correntista2 = new Correntistas();
		Correntistas correntista3 = new Correntistas();
		Correntistas correntista4 = new Correntistas();
		correntista.setTipoConta(ContasType.PF);
		correntista.setSaldo(100.0);
		correntista2.setTipoConta(ContasType.PF);
		correntista2.setSaldo(100.0);
		correntista3.setTipoConta(ContasType.PF);
		correntista3.setSaldo(100.0);
		correntista4.setTipoConta(ContasType.PF);
		correntista4.setSaldo(100.0);
		
		//CLIENTES
		ClientesService serviceCliente = new ClientesService();
		Clientes cliente = new Clientes();
		Clientes cliente2 = new Clientes();
		Clientes cliente3 = new Clientes();
		Clientes cliente4 = new Clientes();
		cliente.setNome("Jo�o");
		cliente.setDataNascimento("25/03/97");
		cliente.setRg("26.514.122-9");
		cliente.setCpf("944.637.040-06");
		cliente.setEstadoCivil(EstadoCivil.SOLTEIRO);
		cliente.setEndereco(endereco3);
		cliente2.setNome("William");
		cliente2.setDataNascimento("15/09/95");
		cliente2.setRg("33.782.287-6");
		cliente2.setCpf("435.141.050-73");
		cliente2.setEstadoCivil(EstadoCivil.SOLTEIRO);
		cliente2.setEndereco(endereco4);
		cliente3.setNome("Maria");
		cliente3.setDataNascimento("10/12/94");
		cliente3.setRg("33.792.113-1");
		cliente3.setCpf("055.217.270-78");
		cliente3.setEstadoCivil(EstadoCivil.SOLTEIRO);
		cliente3.setEndereco(endereco5);
		cliente4.setNome("Mario");
		cliente4.setDataNascimento("04/07/98");
		cliente4.setRg("22.414.817-5");
		cliente4.setCpf("920.019.630-68");
		cliente4.setEstadoCivil(EstadoCivil.SOLTEIRO);
		cliente4.setEndereco(endereco6);
		
		
		//EMAILS x CLIENTES
		EmailsService serviceEmails = new EmailsService();
		Emails email = new Emails();
		Emails email2 = new Emails();
		Emails email3 = new Emails();
		Emails email4 = new Emails();
		email.setEmail("abc@email.com");
		email.setCliente(cliente);
		email2.setEmail("def@email.com");
		email2.setCliente(cliente2);
		email3.setEmail("ghi@email.com");
		email3.setCliente(cliente3);
		email4.setEmail("jkl@email.com");
		email4.setCliente(cliente4);
		
		//LISTAS RESTANTES
		List<Clientes> listaClientes = new ArrayList<>();
		List<Clientes> listaClientes2 = new ArrayList<>();
		List<Clientes> listaClientes3 = new ArrayList<>();
		List<Clientes> listaClientes4 = new ArrayList<>();
		List<Telefones> listaTelefones = new ArrayList<>();
		List<Telefones> listaTelefones2 = new ArrayList<>();
		List<Telefones> listaTelefones3 = new ArrayList<>();
		List<Telefones> listaTelefones4 = new ArrayList<>();
		List<Emails> listaEmails = new ArrayList<>();
		List<Emails> listaEmails2 = new ArrayList<>();
		List<Emails> listaEmails3 = new ArrayList<>();
		List<Emails> listaEmails4 = new ArrayList<>();
		List<Correntistas> listaCorrentistas = new ArrayList<>();
		List<Correntistas> listaCorrentistas2 = new ArrayList<>();
		List<Correntistas> listaCorrentistas3 = new ArrayList<>();
		List<Correntistas> listaCorrentistas4 = new ArrayList<>();
		List<Correntistas> correntistaPorAgencia = new ArrayList<>();
		List<Correntistas> correntistaPorAgencia2 = new ArrayList<>();
		List<Agencias> listaAgencias = new ArrayList<>();
		
		//SETAR DADOS LISTAS
		//Setar clientes nos telefones
		listaClientes.add(cliente);
		telefone.setClientes(listaClientes);
		listaClientes2.add(cliente2);
		telefone2.setClientes(listaClientes2);
		listaClientes3.add(cliente3);
		telefone3.setClientes(listaClientes3);
		listaClientes4.add(cliente4);
		telefone4.setClientes(listaClientes4);
		
		//Setar telefones nos clientes
		listaTelefones.add(telefone);
		cliente.setTelefones(listaTelefones);
		listaTelefones2.add(telefone2);
		cliente2.setTelefones(listaTelefones2);
		listaTelefones3.add(telefone3);
		cliente3.setTelefones(listaTelefones3);
		listaTelefones4.add(telefone4);
		cliente4.setTelefones(listaTelefones4);
		
		//setar emails nos clientes
		listaEmails.add(email);
		cliente.setEmails(listaEmails);
		listaEmails2.add(email2);
		cliente2.setEmails(listaEmails2);
		listaEmails3.add(email3);
		cliente3.setEmails(listaEmails3);
		listaEmails4.add(email4);
		cliente4.setEmails(listaEmails4);
		
		//Setar correntistas nos clientes
		listaCorrentistas.add(correntista);
		cliente.setCorrentistas(listaCorrentistas);
		listaCorrentistas2.add(correntista2);
		cliente2.setCorrentistas(listaCorrentistas2);
		listaCorrentistas3.add(correntista3);
		cliente3.setCorrentistas(listaCorrentistas3);
		listaCorrentistas4.add(correntista4);
		cliente4.setCorrentistas(listaCorrentistas4);
		
		//Setar clientes nos correntistas
		correntista.setClientes(listaClientes);
		correntista2.setClientes(listaClientes2);
		correntista3.setClientes(listaClientes3);
		correntista3.setClientes(listaClientes3);
		
		//Setar agencias nos correntistas
		correntista.setAgencias(agenciasBanco);
		correntista2.setAgencias(agenciasBanco);
		correntista3.setAgencias(agenciasBanco2);
		correntista4.setAgencias(agenciasBanco2);
		
		//Setar correntistas nas agencias
		correntistaPorAgencia.add(correntista);
		correntistaPorAgencia.add(correntista2);
		correntistaPorAgencia2.add(correntista3);
		correntistaPorAgencia2.add(correntista4);
		agencia.setCorrentistas(correntistaPorAgencia);
		agencia2.setCorrentistas(correntistaPorAgencia2);
		
		//EXERCICIO 1
		correntista.transferencia(correntista2, 100.0);
		System.out.println("");
		System.out.println("");
		System.out.println("Saldo correntista prejudicado: "+correntista.getSaldo()
										+", Saldo correntista beneficiado: "+correntista2.getSaldo());
		System.out.println("");
		System.out.println("");
		//SALVAR DADOS  NO BD
		serviceTelefone.salvarTelefones(telefone);
		serviceTelefone.salvarTelefones(telefone2);
		serviceTelefone.salvarTelefones(telefone3);
		serviceTelefone.salvarTelefones(telefone4);
		serviceBanco.salvarBancos(banco);
		serviceAgencia.salvarAgencias(agencia);
		serviceBanco.salvarBancos(banco2);
		serviceAgencia.salvarAgencias(agencia2);
		serviceCorrentista.salvarCorrentistas(correntista);
		serviceCorrentista.salvarCorrentistas(correntista2);
		serviceCorrentista.salvarCorrentistas(correntista3);
		serviceCorrentista.salvarCorrentistas(correntista4);
		serviceEmails.salvarEmails(email);
		serviceEmails.salvarEmails(email2);
		serviceEmails.salvarEmails(email3);
		serviceEmails.salvarEmails(email4);
		serviceTelefone.salvarTelefones(telefone);
		serviceTelefone.salvarTelefones(telefone2);
		serviceTelefone.salvarTelefones(telefone3);
		serviceTelefone.salvarTelefones(telefone4);
		serviceCliente.salvarClientes(cliente);
		serviceCliente.salvarClientes(cliente2);
		serviceCliente.salvarClientes(cliente3);
		serviceCliente.salvarClientes(cliente4);
		
		System.exit(0);
	}
	
	//public static void main(String[] args) {
		//Session session = null;
		//Transaction transaction = null;
		//try {
			//session = HibernateUtil.getSession();
			//transaction = session.beginTransaction();
			
			//Paises pais1 = new Paises();
			//pais1.setNome("Brasil");
			
			//session.save(pais1);
			
			//session.createQuery("select * from paises;").executeUpdate();
			
			//Criteria criteria = session.createCriteria(Paises.class);
			//criteria.createAlias("nomeDoParametro", "nome_paises");
			//criteria.add(
						//Restrictions.isNotNull("nome")   );
			
			//List<Paises> listPaises = criteria.list();
			
			//Estados estado1 = new Estados();
			//estado1.setNome("Rio Grande do Sul");
			
			//estado1.setPais(pais1);
			//session.save(estado1);
			
			//transaction.commit();
		//} catch( Exception e )  {
			//if (transaction != null) {
				//transaction.rollback();
			//}
			//LOG.log(Level.SEVERE, e.getMessage(), e);
			//System.exit(1);
		//} finally {
			//if (session != null) {
				//session.close();
			//}
		//}
		//System.exit(0);
	//}
}
