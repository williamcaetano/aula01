package br.com.dbccompany.bancodigital.Dto;

public class AgenciasDTO {
	
	private Integer idAgencias, codigo;
	private String nome;
	private EnderecosDTO enderecos;
	private BancosDTO bancos;
	
	public Integer getIdAgencias() {
		return idAgencias;
	}
	public void setIdAgencias(Integer idAgencias) {
		this.idAgencias = idAgencias;
	}
	public Integer getCodigo() {
		return codigo;
	}
	public void setCodigo(Integer codigo) {
		this.codigo = codigo;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public EnderecosDTO getEnderecos() {
		return enderecos;
	}
	public void setEnderecos(EnderecosDTO enderecos) {
		this.enderecos = enderecos;
	}
	public BancosDTO getBancos() {
		return bancos;
	}
	public void setBancos(BancosDTO bancos) {
		this.bancos = bancos;
	}
	
	
}
