package br.com.dbccompany.bancodigital.Service;

import java.util.logging.Level;
import java.util.logging.Logger;

import org.hibernate.Transaction;

import br.com.dbccompany.bancodigital.Dao.BairrosDAO;
import br.com.dbccompany.bancodigital.Dto.BairrosDTO;
import br.com.dbccompany.bancodigital.Entity.HibernateUtil;
import br.com.dbccompany.bancodigital.Entity.Bairros;

public class BairrosService {
	private static final BairrosDAO BAIRROS_DAO = new BairrosDAO();
	private static final Logger LOG = Logger.getLogger(BairrosService.class.getName());
	
	public void salvarBairrosDTO(BairrosDTO bairroDTO) {
		boolean started = HibernateUtil.beginTransaction();
		Transaction transaction = HibernateUtil.getSession().getTransaction();
		
		Bairros bairro = BAIRROS_DAO.parseFrom(bairroDTO);
		
		try {
			Bairros bairrosRes = BAIRROS_DAO.buscar(1);
			if(bairrosRes == null) {
				BAIRROS_DAO.criar(bairro);
			}else {
				bairro.setId(bairrosRes.getId());
				BAIRROS_DAO.atualizar(bairro);
			}
			
			if (started) {
				transaction.commit();
			}
			
			bairroDTO.setIdBairros(bairro.getId());
			
		} catch ( Exception ex ) {
			transaction.rollback();
			LOG.log(Level.SEVERE, ex.getMessage(), ex);
		}
		
	}
	
	public void salvarBairros(Bairros bairro) {
		boolean started = HibernateUtil.beginTransaction();
		Transaction transaction = HibernateUtil.getSession().getTransaction();
		
		try {
			Bairros bairrosRes = BAIRROS_DAO.buscar(bairro.getId());
			if(bairrosRes == null) {
				BAIRROS_DAO.criar(bairro);
			}else {
				bairro.setId(bairrosRes.getId());
				BAIRROS_DAO.atualizar(bairro);
			}
			
			if (started) {
				transaction.commit();
			}
			
		} catch ( Exception ex ) {
			transaction.rollback();
			LOG.log(Level.SEVERE, ex.getMessage(), ex);
		}
		
	}
}

