package br.com.dbccompany.bancodigital.Dao;

import br.com.dbccompany.bancodigital.Dto.ClientesDTO;
import br.com.dbccompany.bancodigital.Entity.Clientes;


public class ClientesDAO extends AbstractDAO<Clientes>{
	
	public Clientes parseFrom(ClientesDTO dto) {
		Clientes clientes = null;
		
		if(dto.getIdClientes() != null) {
			clientes = buscar(dto.getIdClientes());
		}
		else {
			clientes = new Clientes();
			clientes.setNome(dto.getNome());
			clientes.setCpf(dto.getCpf());
			clientes.setRg(dto.getRg());
			
			EnderecosDAO enderecoDAO = new EnderecosDAO();
			
			clientes.setEndereco(enderecoDAO.parseFrom(dto.getEndereco()));
		}
		return clientes;
	}
	
	@Override
	protected  Class<Clientes> getEntityClass(){
		return Clientes.class;
	}
}
