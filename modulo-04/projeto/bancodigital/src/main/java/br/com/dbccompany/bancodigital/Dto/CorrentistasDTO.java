package br.com.dbccompany.bancodigital.Dto;

import br.com.dbccompany.bancodigital.Entity.ContasType;

public class CorrentistasDTO {
	private Integer idCorrentistas;
	private ContasType tipoConta;
	
	public Integer getIdCorrentistas() {
		return idCorrentistas;
	}
	public void setIdCorrentistas(Integer idCorrentistas) {
		this.idCorrentistas = idCorrentistas;
	}
	public ContasType getTipoConta() {
		return tipoConta;
	}
	public void setTipoConta(ContasType tipoConta) {
		this.tipoConta = tipoConta;
	}
	
}
