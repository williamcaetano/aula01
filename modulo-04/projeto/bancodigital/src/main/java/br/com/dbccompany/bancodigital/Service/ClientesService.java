package br.com.dbccompany.bancodigital.Service;

import java.util.logging.Level;
import java.util.logging.Logger;

import org.hibernate.Transaction;

import br.com.dbccompany.bancodigital.Dao.ClientesDAO;
import br.com.dbccompany.bancodigital.Dto.ClientesDTO;
import br.com.dbccompany.bancodigital.Entity.HibernateUtil;
import br.com.dbccompany.bancodigital.Entity.Clientes;

public class ClientesService {
	private static final ClientesDAO CLIENTES_DAO = new ClientesDAO();
	private static final Logger LOG = Logger.getLogger(ClientesService.class.getName());
	
	public void salvarClientesDTO(ClientesDTO clienteDTO) {
		boolean started = HibernateUtil.beginTransaction();
		Transaction transaction = HibernateUtil.getSession().getTransaction();
		
		Clientes cliente = CLIENTES_DAO.parseFrom(clienteDTO);
		
		try {
			Clientes clienteRes = CLIENTES_DAO.buscar(1);
			if(clienteRes == null) {
				CLIENTES_DAO.criar(cliente);
			}else {
				cliente.setId(clienteRes.getId());
				CLIENTES_DAO.atualizar(cliente);
			}
			
			if (started) {
				transaction.commit();
			}
			
			clienteDTO.setIdClientes(cliente.getId());
			
		} catch ( Exception ex ) {
			transaction.rollback();
			LOG.log(Level.SEVERE, ex.getMessage(), ex);
		}
		
	}
	
	public void salvarClientes(Clientes cliente) {
		boolean started = HibernateUtil.beginTransaction();
		Transaction transaction = HibernateUtil.getSession().getTransaction();
		
		try {
			Clientes clienteRes = CLIENTES_DAO.buscar(cliente.getId());
			if(clienteRes == null) {
				CLIENTES_DAO.criar(cliente);
			}else {
				cliente.setId(clienteRes.getId());
				CLIENTES_DAO.atualizar(cliente);
			}
			
			if (started) {
				transaction.commit();
			}
			
		} catch ( Exception ex ) {
			transaction.rollback();
			LOG.log(Level.SEVERE, ex.getMessage(), ex);
		}
		
	}
}

