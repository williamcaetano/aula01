package br.com.dbccompany.bancodigital.Dto;

public class EmailsDTO {
	
	private Integer idEmails;
	private String email;
	
	public Integer getIdEmails() {
		return idEmails;
	}
	public void setIdEmails(Integer idEmails) {
		this.idEmails = idEmails;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
}
