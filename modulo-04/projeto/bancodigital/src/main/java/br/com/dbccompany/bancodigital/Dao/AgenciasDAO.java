package br.com.dbccompany.bancodigital.Dao;

import br.com.dbccompany.bancodigital.Dto.AgenciasDTO;
import br.com.dbccompany.bancodigital.Entity.Agencias;

public class AgenciasDAO extends AbstractDAO<Agencias>{
	
	public Agencias parseFrom(AgenciasDTO dto) {
		Agencias agencias = null;
		
		if(dto.getIdAgencias() != null) {
			agencias = buscar(dto.getIdAgencias());
		}
		else {
			agencias = new Agencias();
			agencias.setNome(dto.getNome());
			
			EnderecosDAO enderecoDAO = new EnderecosDAO();
			BancosDAO bancoDAO = new BancosDAO();
			
			agencias.setEndereco(enderecoDAO.parseFrom(dto.getEnderecos()));
			agencias.setBanco(bancoDAO.parseFrom(dto.getBancos()));
		}
		return agencias;
	}
	
	@Override
	protected  Class<Agencias> getEntityClass(){
		return Agencias.class;
	}
}
