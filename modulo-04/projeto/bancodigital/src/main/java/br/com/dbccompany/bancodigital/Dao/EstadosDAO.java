package br.com.dbccompany.bancodigital.Dao;

import br.com.dbccompany.bancodigital.Dto.EstadosDTO;
import br.com.dbccompany.bancodigital.Entity.Estados;

public class EstadosDAO extends AbstractDAO<Estados>{
	
	public Estados parseFrom(EstadosDTO dto) {
		Estados estados = null;
		
		if(dto.getIdEstados() != null) {
			estados = buscar(dto.getIdEstados());
		}
		else {
			estados = new Estados();
			estados.setNome(dto.getNome());
			
			PaisesDAO paisDAO = new PaisesDAO();
			estados.setPais(paisDAO.parseFrom(dto.getPaises()));
		}
		
		return estados;
	}
	
	@Override
	protected  Class<Estados> getEntityClass(){
		return Estados.class;
	}

}
