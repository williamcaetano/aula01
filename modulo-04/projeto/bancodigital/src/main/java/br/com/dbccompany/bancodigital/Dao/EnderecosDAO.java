package br.com.dbccompany.bancodigital.Dao;

import br.com.dbccompany.bancodigital.Dto.BairrosDTO;
import br.com.dbccompany.bancodigital.Dto.EnderecosDTO;
import br.com.dbccompany.bancodigital.Entity.Enderecos;

public class EnderecosDAO extends AbstractDAO<Enderecos>{
	
	public Enderecos parseFrom(EnderecosDTO dto) {
		Enderecos enderecos = null;
		
		if(dto.getIdEndereco() != null) {
			enderecos = buscar(dto.getIdEndereco());
		}
		else {
			enderecos = new Enderecos();
			enderecos.setLogradouro(dto.getLogradouro());
			enderecos.setNumero(dto.getNumero());
			
			BairrosDAO bairroDAO = new BairrosDAO();
			enderecos.setBairro(bairroDAO.parseFrom(dto.getBairros()));
		}
		return enderecos;
	}
	
	@Override
	protected  Class<Enderecos> getEntityClass(){
		return Enderecos.class;
	}

}
