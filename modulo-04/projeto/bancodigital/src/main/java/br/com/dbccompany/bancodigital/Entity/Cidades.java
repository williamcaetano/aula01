package br.com.dbccompany.bancodigital.Entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;


@Entity
@SequenceGenerator (allocationSize = 1, name = "CIDADES_SEQ", sequenceName = "CIDADES_SEQ")
public class Cidades extends AbstractEntity{
	
	private static final long serialVersionUID = 1L;
	
	@Id
    @GeneratedValue ( generator = "CIDADES_SEQ", strategy = GenerationType.SEQUENCE)
    private Integer idCidade;
	
	@Column(name= "nome", length = 200, nullable = false)
    private String nome;
    
    @ManyToOne
    @JoinColumn(name = "fk_id_estado")
    private Estados estado;


	public Integer getId() {
		return idCidade;
	}
	
	public void setId(Integer id) {
        this.idCidade = id;
    }

	public String getNome() {
		return nome;
	}


	public void setNome(String nome) {
		this.nome = nome;
	}


	public Estados getEstado() {
		return estado;
	}


	public void setEstado(Estados estado) {
		this.estado = estado;
	}
    
}
