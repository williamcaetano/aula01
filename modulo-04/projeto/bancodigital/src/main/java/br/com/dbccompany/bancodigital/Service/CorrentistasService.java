package br.com.dbccompany.bancodigital.Service;

import java.util.logging.Level;
import java.util.logging.Logger;

import org.hibernate.Transaction;

import br.com.dbccompany.bancodigital.Dao.CorrentistaDAO;
import br.com.dbccompany.bancodigital.Dto.CorrentistasDTO;
import br.com.dbccompany.bancodigital.Entity.HibernateUtil;
import br.com.dbccompany.bancodigital.Entity.Correntistas;

public class CorrentistasService {
	private static final CorrentistaDAO CORRENTISTAS_DAO = new CorrentistaDAO();
	private static final Logger LOG = Logger.getLogger(CorrentistasService.class.getName());
	
	public void salvarCorrentistasDTO(CorrentistasDTO correntistaDTO) {
		boolean started = HibernateUtil.beginTransaction();
		Transaction transaction = HibernateUtil.getSession().getTransaction();
		
		Correntistas correntista = CORRENTISTAS_DAO.parseFrom(correntistaDTO);
		
		try {
			Correntistas correntistaRes = CORRENTISTAS_DAO.buscar(1);
			if(correntistaRes == null) {
				CORRENTISTAS_DAO.criar(correntista);
			}else {
				correntista.setId(correntistaRes.getId());
				CORRENTISTAS_DAO.atualizar(correntista);
			}
			if (started) {
				transaction.commit();
			}
			
			correntistaDTO.setIdCorrentistas(correntista.getId());
			
		} catch ( Exception ex ) {
			transaction.rollback();
			LOG.log(Level.SEVERE, ex.getMessage(), ex);
		}
		
	}
	
	public void salvarCorrentistas(Correntistas correntista) {
		boolean started = HibernateUtil.beginTransaction();
		Transaction transaction = HibernateUtil.getSession().getTransaction();
		
		try {
			Correntistas correntistaRes = CORRENTISTAS_DAO.buscar(correntista.getId());
			if(correntistaRes == null) {
				CORRENTISTAS_DAO.criar(correntista);
			}else {
				correntista.setId(correntistaRes.getId());
				CORRENTISTAS_DAO.atualizar(correntista);
			}
			if (started) {
				transaction.commit();
			}
			
		} catch ( Exception ex ) {
			transaction.rollback();
			LOG.log(Level.SEVERE, ex.getMessage(), ex);
		}
		
	}
}

