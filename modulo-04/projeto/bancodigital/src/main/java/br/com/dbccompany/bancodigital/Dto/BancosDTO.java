package br.com.dbccompany.bancodigital.Dto;

public class BancosDTO {

	private Integer idBancos, codigo;
	private String nome;
	
	public Integer getIdBancos() {
		return idBancos;
	}
	public void setIdBancos(Integer idBancos) {
		this.idBancos = idBancos;
	}
	public Integer getCodigo() {
		return codigo;
	}
	public void setCodigo(Integer codigo) {
		this.codigo = codigo;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	
	
}
