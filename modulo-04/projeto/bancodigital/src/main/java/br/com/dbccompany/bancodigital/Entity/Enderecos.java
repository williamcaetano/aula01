package br.com.dbccompany.bancodigital.Entity;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;

@Entity
@SequenceGenerator (allocationSize = 1, name = "ENDERECOS_SEQ", sequenceName = "ENDERECOS_SEQ")

public class Enderecos  extends AbstractEntity{
	
	private static final long serialVersionUID = 1L;
	
	@Id
    @GeneratedValue ( generator = "ENDERECOS_SEQ", strategy = GenerationType.SEQUENCE)
    private Integer idEndereco;
	
	@Column(name= "logradouro", length = 200, nullable = false)
	private String logradouro;
	
	@Column(name= "numero", length = 200, nullable = false)
	private Integer numero;
	
	@Column(name= "complemento", length = 200, nullable = true)
	private String complemento;
	
	@ManyToOne
    @JoinColumn(name = "fk_id_bairro")
    private Bairros bairro;
	
	@OneToMany(mappedBy="endereco", cascade = CascadeType.ALL)
	private List<Agencias> agencias = new ArrayList<>();
	
	@OneToMany(mappedBy="endereco", cascade = CascadeType.ALL)
	private List<Clientes> clientes = new ArrayList<>();
	
	public Integer getId() {
		return idEndereco;
	}

	public Integer getIdEndereco() {
		return idEndereco;
	}

	public void setIdEndereco(Integer idEndereco) {
		this.idEndereco = idEndereco;
	}

	public List<Agencias> getAgencias() {
		return agencias;
	}

	public void setAgencias(List<Agencias> agencias) {
		this.agencias = agencias;
	}

	public List<Clientes> getClientes() {
		return clientes;
	}

	public void setClientes(List<Clientes> clientes) {
		this.clientes = clientes;
	}

	public void setId(Integer id) {
        this.idEndereco = id;
    }
	
	public String getLogradouro() {
		return logradouro;
	}

	public void setLogradouro(String logradouro) {
		this.logradouro = logradouro;
	}

	public Integer getNumero() {
		return numero;
	}

	public void setNumero(Integer numero) {
		this.numero = numero;
	}

	public String getComplemento() {
		return complemento;
	}

	public void setComplemento(String complemento) {
		this.complemento = complemento;
	}

	public Bairros getBairro() {
		return bairro;
	}

	public void setBairro(Bairros bairro) {
		this.bairro = bairro;
	}
	
}
