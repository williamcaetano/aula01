package br.com.dbccompany.bancodigital.Service;

import java.util.logging.Level;
import java.util.logging.Logger;

import org.hibernate.Transaction;

import br.com.dbccompany.bancodigital.Dao.AgenciasDAO;
import br.com.dbccompany.bancodigital.Dto.AgenciasDTO;
import br.com.dbccompany.bancodigital.Entity.HibernateUtil;
import br.com.dbccompany.bancodigital.Entity.Agencias;

public class AgenciasService {
	private static final AgenciasDAO AGENCIAS_DAO = new AgenciasDAO();
	private static final Logger LOG = Logger.getLogger(AgenciasService.class.getName());
	
	public void salvarAgenciasDTO(AgenciasDTO agenciaDTO) {
		boolean started = HibernateUtil.beginTransaction();
		Transaction transaction = HibernateUtil.getSession().getTransaction();
		
		Agencias agencia = AGENCIAS_DAO.parseFrom(agenciaDTO);
		
		try {
			Agencias agenciaRes = AGENCIAS_DAO.buscar(1);
			if(agenciaRes == null) {
				AGENCIAS_DAO.criar(agencia);
			}else {
				agencia.setId(agenciaRes.getId());
				AGENCIAS_DAO.atualizar(agencia);
			}
			if (started) {
				transaction.commit();
			}
			
			agenciaDTO.setIdAgencias(agencia.getId());
			
		} catch ( Exception ex ) {
			transaction.rollback();
			LOG.log(Level.SEVERE, ex.getMessage(), ex);
		}
		
	}
	
	public void salvarAgencias(Agencias agencia) {
		boolean started = HibernateUtil.beginTransaction();
		Transaction transaction = HibernateUtil.getSession().getTransaction();
		
		try {
			Agencias agenciaRes = AGENCIAS_DAO.buscar(agencia.getId());
			if(agenciaRes == null) {
				AGENCIAS_DAO.criar(agencia);
			}else {
				agencia.setId(agenciaRes.getId());
				AGENCIAS_DAO.atualizar(agencia);
			}
			if (started) {
				transaction.commit();
			}
			
		} catch ( Exception ex ) {
			transaction.rollback();
			LOG.log(Level.SEVERE, ex.getMessage(), ex);
		}
		
	}
}
