package br.com.dbccompany.bancodigital.Dto;

public class EnderecosDTO {
	
	private Integer idEndereco, numero;
	private String logradouro;
	
	//DTO Bairro
	private BairrosDTO bairros;

	public Integer getIdEndereco() {
		return idEndereco;
	}

	public void setIdEendereco(Integer idEndereco) {
		this.idEndereco = idEndereco;
	}

	public Integer getNumero() {
		return numero;
	}

	public void setNumero(Integer numero) {
		this.numero = numero;
	}

	public String getLogradouro() {
		return logradouro;
	}

	public void setLogradouro(String logradouro) {
		this.logradouro = logradouro;
	}

	public BairrosDTO getBairros() {
		return bairros;
	}

	public void setBairros(BairrosDTO bairros) {
		this.bairros = bairros;
	}
	
}
