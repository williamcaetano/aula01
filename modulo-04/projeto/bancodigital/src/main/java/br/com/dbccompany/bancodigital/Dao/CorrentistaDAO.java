package br.com.dbccompany.bancodigital.Dao;

import br.com.dbccompany.bancodigital.Dto.CorrentistasDTO;
import br.com.dbccompany.bancodigital.Entity.Correntistas;

public class CorrentistaDAO extends AbstractDAO<Correntistas>{
	
	public Correntistas parseFrom(CorrentistasDTO dto) {
		Correntistas correntistas = null;
		
		if(dto.getIdCorrentistas() != null) {
			correntistas = buscar(dto.getIdCorrentistas());
		}
		else {
			correntistas = new Correntistas();
			correntistas.setTipoConta(dto.getTipoConta());
		}
		return correntistas;
	}
	
	@Override
	protected  Class<Correntistas> getEntityClass(){
		return Correntistas.class;
	}

}
