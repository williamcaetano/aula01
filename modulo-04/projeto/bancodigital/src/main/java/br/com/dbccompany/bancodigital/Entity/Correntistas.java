package br.com.dbccompany.bancodigital.Entity;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.SequenceGenerator;

@Entity
@SequenceGenerator( allocationSize = 1, name = "CORRENTISTAS_SEQ", sequenceName = "CORRENTISTAS_SEQ")
public class Correntistas extends AbstractEntity{
	
	private static final long serialVersionUID = 1L;
	
	@Id
	@Column(name="ID_CORRENTISTA")
	@GeneratedValue( generator = "CORRENTISTAS_SEQ" , strategy = GenerationType.SEQUENCE)
	private Integer idCorrentista;
	
	@Column(name= "razaoSocial", length = 100, nullable = true)
	private String razaoSocial;
	
	@Column(name= "cnpj", length = 20, nullable = true)
	private String cnpj;
	
	@Column(name = "saldo", length = 20, nullable = true)
	private Double saldo;
	
	@Enumerated(EnumType.STRING)
	private ContasType tipoConta;
	
	@ManyToMany( mappedBy = "correntistas" )
	private List<Agencias> agencias = new ArrayList<>();
	
	@ManyToMany( cascade = CascadeType.ALL )
	@JoinTable(name= "correntistas_x_clientes" , joinColumns = { @JoinColumn(name="id_correntistas")} ,  
																inverseJoinColumns = {@JoinColumn(name="id_clientes")}						
						)
	private List<Clientes> clientes = new ArrayList<>();
	
	public Integer getId() {
		return idCorrentista;
	}
	
	public String getRazaoSocial() {
		return razaoSocial;
	}
	public void setRazaoSocial(String razaoSocial) {
		this.razaoSocial = razaoSocial;
	}
	public String getCnpj() {
		return cnpj;
	}
	public void setCnpj(String cnpj) {
		this.cnpj = cnpj;
	}
	public ContasType getTipoConta() {
		return tipoConta;
	}
	public void setTipoConta(ContasType tipoConta) {
		this.tipoConta = tipoConta;
	}
	
	public void setId(Integer id) {
		this.idCorrentista = id;
	}

	public Integer getIdCorrentista() {
		return idCorrentista;
	}

	public void setIdCorrentista(Integer idCorrentista) {
		this.idCorrentista = idCorrentista;
	}

	public Double getSaldo() {
		return saldo;
	}

	public void setSaldo(Double saldo) {
		this.saldo = saldo;
	}

	public List<Agencias> getAgencias() {
		return agencias;
	}

	public void setAgencias(List<Agencias> agencias) {
		this.agencias = agencias;
	}

	public List<Clientes> getClientes() {
		return clientes;
	}

	public void setClientes(List<Clientes> clientes) {
		this.clientes = clientes;
	}
	
	public boolean fazerDeposito(Double valor) {
		if (valor > 0) {
			this.saldo+=valor;
			return true;
		}
		else
			return false;
	}
	
	public boolean efetuarSaque(Double valor) {
		if(this.saldo >= valor) {
			this.saldo-=valor;
			return true;
		}
		return false;
	}
	
	public boolean transferencia(Correntistas c, Double valor) {
		boolean efetuouSaque = efetuarSaque(valor);
		if(efetuouSaque) {
			c.setSaldo(c.getSaldo()+valor);
			return true;
		}
		return false;
	}
	
}
