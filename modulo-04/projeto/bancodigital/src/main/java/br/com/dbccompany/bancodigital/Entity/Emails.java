package br.com.dbccompany.bancodigital.Entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

@Entity
@SequenceGenerator( allocationSize = 1, name = "EMAILS_SEQ", sequenceName = "EMAILS_SEQ")
public class Emails extends AbstractEntity{
	
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue( generator = "EMAILS_SEQ" , strategy = GenerationType.SEQUENCE)
	private Integer idEmail;
	
	@Column(name= "email", length = 200, nullable = false)
	private String email;
	
	@ManyToOne
    @JoinColumn(name = "id_cliente_clientes")
    private Clientes cliente;

	public Integer getId() {
		return idEmail;
	}
	
	public void setId(Integer id) {
        this.idEmail = id;
    }

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Clientes getCliente() {
		return cliente;
	}

	public void setCliente(Clientes cliente) {
		this.cliente = cliente;
	}
	
}
