package br.com.dbccompany.bancodigital.Service;

import java.util.logging.Level;
import java.util.logging.Logger;

import org.hibernate.Transaction;

import br.com.dbccompany.bancodigital.Dao.CidadesDAO;
import br.com.dbccompany.bancodigital.Dto.CidadesDTO;
import br.com.dbccompany.bancodigital.Entity.HibernateUtil;
import br.com.dbccompany.bancodigital.Entity.Cidades;

public class CidadesService {
	private static final CidadesDAO CIDADES_DAO = new CidadesDAO();
	private static final Logger LOG = Logger.getLogger(CidadesService.class.getName());
	
	public void salvarCidadesDTO(CidadesDTO cidadeDTO) {
		boolean started = HibernateUtil.beginTransaction();
		Transaction transaction = HibernateUtil.getSession().getTransaction();
		
		Cidades cidade = CIDADES_DAO.parseFrom(cidadeDTO);
		
		try {
			Cidades cidadesRes = CIDADES_DAO.buscar(1);
			if(cidadesRes == null) {
				CIDADES_DAO.criar(cidade);
			}else {
				cidade.setId(cidadesRes.getId());
				CIDADES_DAO.atualizar(cidade);
			}
			
			if (started) {
				transaction.commit();
			}
			
			cidadeDTO.setIdCidades(cidade.getId());
			
		} catch ( Exception ex ) {
			transaction.rollback();
			LOG.log(Level.SEVERE, ex.getMessage(), ex);
		}
		
	}
	
	public void salvarCidades(Cidades cidade) {
		boolean started = HibernateUtil.beginTransaction();
		Transaction transaction = HibernateUtil.getSession().getTransaction();
		
		try {
			Cidades cidadesRes = CIDADES_DAO.buscar(cidade.getId());
			if(cidadesRes == null) {
				CIDADES_DAO.criar(cidade);
			}else {
				cidade.setId(cidadesRes.getId());
				CIDADES_DAO.atualizar(cidade);
			}
			
			if (started) {
				transaction.commit();
			}
			
		} catch ( Exception ex ) {
			transaction.rollback();
			LOG.log(Level.SEVERE, ex.getMessage(), ex);
		}
		
	}
}
