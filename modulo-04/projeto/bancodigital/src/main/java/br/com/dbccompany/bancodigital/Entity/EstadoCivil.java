package br.com.dbccompany.bancodigital.Entity;

public enum EstadoCivil {
	CASADO, SOLTEIRO, DIVORCIADO, VIUVO
}
