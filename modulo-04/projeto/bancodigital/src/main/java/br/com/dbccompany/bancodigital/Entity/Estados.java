package br.com.dbccompany.bancodigital.Entity;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

@Entity
@SequenceGenerator (allocationSize = 1, name = "ESTADOS_SEQ", sequenceName = "ESTADOS_SEQ")
public class Estados extends AbstractEntity{
	
	private static final long serialVersionUID = 1L;
	
    @Id
    @GeneratedValue ( generator = "ESTADOS_SEQ", strategy = GenerationType.SEQUENCE)
    private Integer idEstado;
    
    @Column(name= "nome", length = 200, nullable = false)
    private String nome;    
    
    
    @ManyToOne
    @JoinColumn(name = "fk_id_pais")
    private Paises pais;
    
    public Integer getId() {
        return idEstado;
    }
    
    public void setId(Integer id) {
        this.idEstado = id;
    }
    
    public String getNome() {
        return nome;
    }
    
    public void setNome(String nome) {
        this.nome = nome;
    }
    
    public Paises getPais() {
        return pais;
    }
    
    public void setPais(Paises pais) {
        this.pais = pais;
    }
}