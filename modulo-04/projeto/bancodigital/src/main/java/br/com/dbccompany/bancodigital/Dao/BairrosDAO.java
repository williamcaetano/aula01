package br.com.dbccompany.bancodigital.Dao;

import br.com.dbccompany.bancodigital.Dto.BairrosDTO;
import br.com.dbccompany.bancodigital.Entity.Bairros;

public class BairrosDAO extends AbstractDAO<Bairros>{
	
	public Bairros parseFrom(BairrosDTO dto) {
		Bairros bairros = null;
		
		if(dto.getIdBairros() != null) {
			bairros = buscar(dto.getIdBairros());
		}
		else {
			bairros = new Bairros();
			bairros.setNome(dto.getNome());
			
			CidadesDAO cidadeDAO = new CidadesDAO();
			bairros.setCidade(cidadeDAO.parseFrom(dto.getCidades()));
		}
		
		return bairros;
	}
	
	@Override
	protected  Class<Bairros> getEntityClass(){
		return Bairros.class;
	}

}
