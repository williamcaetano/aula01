package br.com.dbccompany.bancodigital.Entity;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name="TELEFONES_BANCO")
@SequenceGenerator( allocationSize = 1, name = "TELEFONES_SEQ", sequenceName = "TELEFONES_SEQ")
public class Telefones extends AbstractEntity{
	
	private static final long serialVersionUID = 1L;
	
	@Id
	@Column(name = "ID_TELEFONE")
	@GeneratedValue( generator = "TELEFONES_SEQ" , strategy = GenerationType.SEQUENCE)
	private Integer idTelefone;
	
	@Column(name= "numero", length = 100, nullable = false)
	private String numero;
	
	@Enumerated(EnumType.STRING)
	private TelefonesType tiposTelefone;
	
	@ManyToMany( cascade = CascadeType.ALL )
	@JoinTable(name= "telefones_x_clientes" , joinColumns = { @JoinColumn(name="id_telefones")} ,  
																inverseJoinColumns = {@JoinColumn(name="id_clientes")}						
						)
	private List<Clientes> clientes = new ArrayList<>();
	
	public Integer getId() {
		return idTelefone;
	}
	
	public Integer getIdTelefone() {
		return idTelefone;
	}

	public void setIdTelefone(Integer idTelefone) {
		this.idTelefone = idTelefone;
	}

	public List<Clientes> getClientes() {
		return clientes;
	}

	public void setClientes(List<Clientes> clientes) {
		this.clientes = clientes;
	}

	public void setId(Integer id) {
        this.idTelefone = id;
    }
	
	public String getNumero() {
		return numero;
	}
	public void setNumero(String numero) {
		this.numero = numero;
	}
	public TelefonesType getTiposTelefone() {
		return tiposTelefone;
	}
	public void setTiposTelefone(TelefonesType tiposTelefone) {
		this.tiposTelefone = tiposTelefone;
	}
}
