package br.com.dbccompany.bancodigital.Dao;

import br.com.dbccompany.bancodigital.Dto.BancosDTO;
import br.com.dbccompany.bancodigital.Entity.Bancos;

public class BancosDAO extends AbstractDAO<Bancos>{
	
	public Bancos parseFrom(BancosDTO bancoDTO) {
		Bancos bancos = null;
		
		if(bancoDTO.getIdBancos() != null) {
			bancos = buscar(bancoDTO.getIdBancos());
		}
		bancos = new Bancos();
		bancos.setNome(bancoDTO.getNome());
		
		return bancos;
	}
	
	@Override
	protected  Class<Bancos> getEntityClass(){
		return Bancos.class;
	}

}
